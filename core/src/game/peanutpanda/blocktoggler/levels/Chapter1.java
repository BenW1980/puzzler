package game.peanutpanda.blocktoggler.levels;

import com.badlogic.gdx.utils.Array;

import game.peanutpanda.blocktoggler.gameobjects.ArrowShape;
import game.peanutpanda.blocktoggler.gameobjects.DoubleArrow;
import game.peanutpanda.blocktoggler.gameobjects.SingleArrow;
import game.peanutpanda.blocktoggler.gameobjects.Tile;

public class Chapter1 extends Chapter {

    public Chapter1() {

        setChapterNumber(1);

        levelMap.put(1, new Level("1-1", 1, 1));
        levelMap.put(2, new Level("1-2", 2, 1));
        levelMap.put(3, new Level("1-3", 3, 1));
        levelMap.put(4, new Level("1-4", 4, 2));
        levelMap.put(5, new Level("1-5", 5, 4));
        levelMap.put(6, new Level("1-6", 6, 2));
        levelMap.put(7, new Level("1-7", 7, 3));
        levelMap.put(8, new Level("1-8", 8, 3));
        levelMap.put(9, new Level("1-9", 9, 1));
        levelMap.put(10, new Level("1-10", 10, 2));
        levelMap.put(11, new Level("1-11", 11, 1));
        levelMap.put(12, new Level("1-12", 12, 2));
        levelMap.put(13, new Level("1-13", 13, 4));
        levelMap.put(14, new Level("1-14", 14, 4));
        levelMap.put(15, new Level("1-15", 15, 4));
        levelMap.put(16, new Level("1-16", 16, 6));
        levelMap.put(17, new Level("1-17", 17, 5));
        levelMap.put(18, new Level("1-18", 18, 2));
        levelMap.put(19, new Level("1-19", 19, 6));
        levelMap.put(20, new Level("1-20", 20, 6));

        levelMap.get(1).setUnlocked(true);
        unlocked = true;
    }

    @Override
    public void start(Level level) {


        tiles = new Array<Tile>();
        arrows = new Array<ArrowShape>();
        level.setActiveCount(0);

        switch (level.getCurrentLevel()) {
            case 1:
                level1();
                break;
            case 2:
                level2();
                break;
            case 3:
                level3();
                break;
            case 4:
                level4();
                break;
            case 5:
                level5();
                break;
            case 6:
                level6();
                break;
            case 7:
                level7();
                break;
            case 8:
                level8();
                break;
            case 9:
                level9();
                level.setActiveCount(1);
                break;
            case 10:
                level10();
                level.setActiveCount(2);
                break;
            case 11:
                level11();
                level.setActiveCount(2);
                break;
            case 12:
                level12();
                level.setActiveCount(2);
                break;
            case 13:
                level13();
                level.setActiveCount(4);
                break;
            case 14:
                level14();
                level.setActiveCount(4);
                break;
            case 15:
                level15();
                level.setActiveCount(4);
                break;
            case 16:
                level16();
                level.setActiveCount(7);
                break;
            case 17:
                level17();
                level.setActiveCount(4);
                break;
            case 18:
                level18();
                level.setActiveCount(3);
                break;
            case 19:
                level19();
                level.setActiveCount(8);
                break;
            case 20:
                level20();
                level.setActiveCount(4);
                break;
        }
    }

    public void level1() {

        for (int i = 0; i < 3; i++) {
            tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (i * singleTileSize), scrHeight / 2 - singleTileSize));
        }
    }

    @Override
    public void level2() {

        for (int i = 0; i < 2; i++) {
            tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + (i * singleTileSize), scrHeight / 2 - singleTileSize * 2));
        }

        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize, scrHeight / 2 - singleTileSize));
    }

    public void level3() {

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 0.5f, scrHeight / 2 - singleTileSize * 2));

        for (int i = 0; i < 3; i++) {
            tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (i * singleTileSize), scrHeight / 2 - singleTileSize));
        }

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 0.5f, scrHeight / 2));
    }

    @Override
    public void level4() {

        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2 - (4 * singleTileSize)));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2 - (3 * singleTileSize)));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2 - (2 * singleTileSize)));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2 - (singleTileSize)));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2));

    }

    @Override
    public void level5() {

        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2 - (4 * singleTileSize)));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2 - (3 * singleTileSize)));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2 - (2 * singleTileSize)));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2 - (singleTileSize)));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2));

        for (int i = 0; i < 2; i++) {
            tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize + (i * singleTileSize), scrHeight / 2 - singleTileSize * 2));
        }

        for (int i = 0; i < 2; i++) {
            tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize + (i * singleTileSize) + singleTileSize, scrHeight / 2 - singleTileSize * 2));
        }
    }

    @Override
    public void level6() {


        for (int i = 0; i < 3; i++) {
            tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (i * singleTileSize), scrHeight / 2 - singleTileSize * 2));
        }

        for (int i = 0; i < 3; i++) {
            tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (i * singleTileSize), scrHeight / 2 - singleTileSize));
        }
    }

    @Override
    public void level7() {


        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));

        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2));

    }

    @Override
    public void level8() {


        for (int i = 0; i < 2; i++) {
            tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (i * singleTileSize), scrHeight - singleTileSize * 9));
        }
        for (int i = 0; i < 2; i++) {
            tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize + (i * singleTileSize), scrHeight - singleTileSize * 8));
        }
        for (int i = 0; i < 2; i++) {
            tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize + (i * singleTileSize), scrHeight - singleTileSize * 7));
        }

        for (int i = 0; i < 2; i++) {
            tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize + (i * singleTileSize), scrHeight - singleTileSize * 6));
        }
        for (int i = 0; i < 2; i++) {
            tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (i * singleTileSize), scrHeight - singleTileSize * 5));
        }

    }

    @Override
    public void level9() {


        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 2));

        arrows.add(new SingleArrow(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize, 90));
    }

    @Override
    public void level10() {

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 2));

        arrows.add(new SingleArrow(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize, 90));

        ///

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + singleTileSize, scrHeight / 2));

        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2, true));


    }

    @Override
    public void level11() { //OK

        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize, true));


        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2));


        arrows.add(new SingleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, 90));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize / 2, scrHeight / 2 - singleTileSize, true));


    }


    @Override
    public void level12() { //OK

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - 2 * singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (singleTileSize), scrHeight / 2 - 2 * singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (2 * singleTileSize), scrHeight / 2 - 2 * singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        arrows.add(new SingleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, 0));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize / 2, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2));

    }

    @Override
    public void level13() { //OK


        arrows.add(new SingleArrow(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2, 90));
        arrows.add(new SingleArrow(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 2, 90));

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 3, true));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 2, true));


        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2, true));


        ///

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + singleTileSize, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 + singleTileSize, true));


    }

    @Override
    public void level14() { //OK

        arrows.add(new SingleArrow(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize, 90));

        arrows.add(new SingleArrow(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, 90));

        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));

        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2, true));

        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 + singleTileSize));
    }

    @Override
    public void level15() { //OK

        arrows.add(new DoubleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, 0));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize / 2, scrHeight / 2 - singleTileSize, true));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - 2 * singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (singleTileSize), scrHeight / 2 - 2 * singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (2 * singleTileSize), scrHeight / 2 - 2 * singleTileSize));


        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2));
    }

    @Override
    public void level16() { //OK

        arrows.add(new DoubleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2, 0));
        arrows.add(new DoubleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, 0));

        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 + singleTileSize, true)); //up
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, true)); // mid
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (singleTileSize), scrHeight / 2 - singleTileSize * 3, true)); //low
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2, true)); //left1
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true)); //left2
        tiles.add(new Tile(scrWidth / 2 + singleTileSize / 2, scrHeight / 2, true)); //right1
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true)); //right2

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize));


        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (2 * singleTileSize), scrHeight / 2 - singleTileSize * 3));


        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));


        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 + singleTileSize));

        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 + singleTileSize));

    }

    @Override
    public void level17() {

        arrows.add(new DoubleArrow(scrWidth / 2 - singleTileSize / 2, scrHeight / 2 - (2 * singleTileSize), 0));


        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2 - (4 * singleTileSize)));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2 - (3 * singleTileSize), true));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2 - (singleTileSize), true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize / 2, scrHeight / 2));

        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize + (singleTileSize), scrHeight / 2 - singleTileSize * 2, true));

        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize + (singleTileSize) + singleTileSize, scrHeight / 2 - singleTileSize * 2));

    }

    @Override
    public void level18() { //OK

        arrows.add(new DoubleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, 0));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - 2 * singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (singleTileSize), scrHeight / 2 - 2 * singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (2 * singleTileSize), scrHeight / 2 - 2 * singleTileSize));


        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize / 2, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2, true));

    }

    @Override
    public void level19() {

        boolean bool;

        for (int i = 0; i < 5; i++) {
            bool = i % 2 == 0;
            tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize + (i * singleTileSize), scrHeight / 2 - singleTileSize * 3, bool));
        }

        for (int i = 0; i < 5; i++) {
            bool = i % 2 != 0;
            tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize + (i * singleTileSize), scrHeight / 2 - singleTileSize, bool));
        }

        for (int i = 0; i < 5; i++) {
            bool = i % 2 == 0;
            tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize + (i * singleTileSize), scrHeight / 2 + singleTileSize, bool));
        }

        arrows.add(new SingleArrow(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2, 90));
        arrows.add(new SingleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2, 90));
        arrows.add(new SingleArrow(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2, 90));

        arrows.add(new SingleArrow(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, 90));
        arrows.add(new SingleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, 90));
        arrows.add(new SingleArrow(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, 90));
    }

    @Override
    public void level20() {


        arrows.add(new SingleArrow(scrWidth / 2, scrHeight / 2 - singleTileSize, 0));
        arrows.add(new SingleArrow(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize, 0));

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + (singleTileSize), scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + (2 * singleTileSize), scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + (3 * singleTileSize), scrHeight / 2, true));

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + (singleTileSize), scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + (2 * singleTileSize), scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + (3 * singleTileSize), scrHeight / 2 - singleTileSize * 2));


        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize));

    }

}
