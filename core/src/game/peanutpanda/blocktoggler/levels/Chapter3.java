package game.peanutpanda.blocktoggler.levels;

import com.badlogic.gdx.utils.Array;

import game.peanutpanda.blocktoggler.gameobjects.ArrowShape;
import game.peanutpanda.blocktoggler.gameobjects.Deflector;
import game.peanutpanda.blocktoggler.gameobjects.Direction;
import game.peanutpanda.blocktoggler.gameobjects.DoubleArrow;
import game.peanutpanda.blocktoggler.gameobjects.Pusher;
import game.peanutpanda.blocktoggler.gameobjects.Shooter;
import game.peanutpanda.blocktoggler.gameobjects.SingleArrow;
import game.peanutpanda.blocktoggler.gameobjects.Tile;


public class Chapter3 extends Chapter {

    public Chapter3() {

        setChapterNumber(3);

        levelMap.put(1, new Level("3-1", 1, 3));
        levelMap.put(2, new Level("3-2", 2, 3));
        levelMap.put(3, new Level("3-3", 3, 4));
        levelMap.put(4, new Level("3-4", 4, 6));
        levelMap.put(5, new Level("3-5", 5, 5));
        levelMap.put(6, new Level("3-6", 6, 10));
        levelMap.put(7, new Level("3-7", 7, 6));
        levelMap.put(8, new Level("3-8", 8, 5));
        levelMap.put(9, new Level("3-9", 9, 8));
        levelMap.put(10, new Level("3-10", 10, 12));
        levelMap.put(11, new Level("3-11", 11, 7));
        levelMap.put(12, new Level("3-12", 12, 7));
        levelMap.put(13, new Level("3-13", 13, 5));
        levelMap.put(14, new Level("3-14", 14, 6));
        levelMap.put(15, new Level("3-15", 15, 5));
        levelMap.put(16, new Level("3-16", 16, 6));
        levelMap.put(17, new Level("3-17", 17, 8));
        levelMap.put(18, new Level("3-18", 18, 13));
        levelMap.put(19, new Level("3-19", 19, 8));
        levelMap.put(20, new Level("3-20", 20, 7));

//        levelMap.get(1).setUnlocked(true);

    }

    @Override
    public void start(Level level) {

        tiles = new Array<Tile>();
        arrows = new Array<ArrowShape>();
        shooters = new Array<Shooter>();
        deflectors = new Array<Deflector>();
        pushers = new Array<Pusher>();
        level.setActiveCount(0);

        switch (level.getCurrentLevel()) {
            case 1:
                level1();
                level.setActiveCount(1);
                break;
            case 2:
                level2();
                level.setActiveCount(1);
                break;
            case 3:
                level3();
                level.setActiveCount(2);
                break;
            case 4:
                level4();
                level.setActiveCount(3);
                break;
            case 5:
                level.setActiveCount(5);
                level5();
                break;
            case 6:
                level6();
                level.setActiveCount(4);
                break;
            case 7:
                level7();
                level.setActiveCount(6);
                break;
            case 8:
                level8();
                level.setActiveCount(4);
                break;
            case 9:
                level9();
                level.setActiveCount(4);
                break;
            case 10:
                level10();
                level.setActiveCount(8);
                break;
            case 11:
                level11();
                level.setActiveCount(6);
                break;
            case 12:
                level12();
                level.setActiveCount(8);
                break;
            case 13:
                level13();
                level.setActiveCount(4);
                break;
            case 14:
                level14();
                level.setActiveCount(2);
                break;
            case 15:
                level15();
                level.setActiveCount(6);
                break;
            case 16:
                level16();
                level.setActiveCount(3);
                break;
            case 17:
                level17();
                level.setActiveCount(8);
                break;
            case 18:
                level18();
                level.setActiveCount(4);
                break;
            case 19:
                level19();
                level.setActiveCount(6);
                break;
            case 20:
                level20();
                level.setActiveCount(6);
                break;
        }

    }

    @Override
    public void level1() {

        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize, Direction.LEFT));

        Tile tile1 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize);
        tile1.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize, true));
        tiles.add(tile1);

    }

    @Override
    public void level2() {
        Tile tile1 = new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4);
        tile1.setDirUpStop(scrHeight / 2 - singleTileSize * 2);
        tiles.add(tile1);

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, true));

        pushers.add(new Pusher(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2));
    }

    @Override
    public void level3() {

        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 3, Direction.LEFT));

        Tile tile1 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 3);
        tile1.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);
        tile1.setDirRightStop(scrWidth / 2 + singleTileSize);

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize, true));
        tiles.add(tile1);

        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize));

        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 3, Direction.RIGHT));
    }

    @Override
    public void level4() {

        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 0.5f, scrHeight / 2 - singleTileSize * 4, Direction.UP));
        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 0.5f, scrHeight / 2 + singleTileSize, Direction.DOWN));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 1.5f, scrHeight / 2 - singleTileSize * 4));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 1.5f, scrHeight / 2 - singleTileSize * 3));
        Tile tile2 = new Tile(scrWidth / 2 - singleTileSize * 0.5f, scrHeight / 2 - singleTileSize * 3, true);
        tile2.setDirUpStop(scrHeight / 2);
        tile2.setDirDownStop(scrHeight / 2 - singleTileSize * 3);

        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 1.5f, scrHeight / 2 - singleTileSize * 4, Direction.UP));
        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 1.5f, scrHeight / 2 + singleTileSize, Direction.DOWN));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 0.5f, scrHeight / 2 - singleTileSize * 4));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 0.5f, scrHeight / 2 - singleTileSize * 3));
        Tile tile1 = new Tile(scrWidth / 2 + singleTileSize * 1.5f, scrHeight / 2 - singleTileSize * 3);
        tile1.setDirUpStop(scrHeight / 2);
        tile1.setDirDownStop(scrHeight / 2 - singleTileSize * 3);

        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 2.5f, scrHeight / 2 - singleTileSize * 4, Direction.UP));
        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 2.5f, scrHeight / 2 + singleTileSize, Direction.DOWN));
        Tile tile3 = new Tile(scrWidth / 2 - singleTileSize * 2.5f, scrHeight / 2 - singleTileSize * 3);
        tile3.setDirUpStop(scrHeight / 2);
        tile3.setDirDownStop(scrHeight / 2 - singleTileSize * 3);

        tiles.add(tile2);
        tiles.add(tile1);

        tiles.add(tile3);

        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 0.5f, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 0.5f, scrHeight / 2 + singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 1.5f, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 1.5f, scrHeight / 2 + singleTileSize));


    }

    @Override
    public void level5() {

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 4));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 3, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2));

        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2, Direction.LEFT));
        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 2, Direction.LEFT));
        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 4, Direction.LEFT));

        Tile tile3 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 4);
        tile3.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);
        tiles.add(tile3);

        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 3, true));

        Tile tile2 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 2);
        tile2.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);
        tiles.add(tile2);

        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize, true));

        Tile tile1 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2);
        tile1.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);
        tiles.add(tile1);

        shooters.add(new Shooter(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        shooters.add(new Shooter(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        deflectors.add(new Deflector(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize, 45));
        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 + singleTileSize, 45));

    }

    @Override
    public void level6() {

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 3, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 + singleTileSize, true));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 4));

        Tile tile1 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 3);
        tile1.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);
        tiles.add(tile1);


        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize, Direction.LEFT));
        Tile tile2 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize);
        tile2.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);
        tiles.add(tile2);
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize * 2));

        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 3, Direction.LEFT));

    }

    @Override
    public void level7() { //OK

        Tile tile1 = new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2, true);
        tile1.setDirDownStop(scrHeight / 2 - singleTileSize * 2);
        tile1.setDirUpStop(scrHeight / 2);

        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2, true));

        Tile tile2 = new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2, true);
        tile2.setDirDownStop(scrHeight / 2 - singleTileSize * 2);
        tile2.setDirUpStop(scrHeight / 2);

        arrows.add(new DoubleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, 0));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize, true));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize / 2, scrHeight / 2 - singleTileSize, true));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));

        pushers.add(new Pusher(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 + singleTileSize, Direction.DOWN));


        pushers.add(new Pusher(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 + singleTileSize, Direction.DOWN));


        pushers.add(new Pusher(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3, Direction.UP));
        pushers.add(new Pusher(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3, Direction.UP));

        tiles.add(tile1);
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2));

        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2));
        tiles.add(tile2);

    }

    @Override
    public void level8() {

        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4));
        Tile tile3 = new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4);
        tile3.setDirUpStop(scrHeight / 2 + singleTileSize);
        tiles.add(tile3);
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4));
        tiles.add(new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4, true));


        pushers.add(new Pusher(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 + singleTileSize * 2, Direction.DOWN));
        pushers.add(new Pusher(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 + singleTileSize * 2, Direction.DOWN));

        Tile tile1 = new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 + singleTileSize, true);
        tile1.setDirDownStop(scrHeight / 2 - singleTileSize * 3);
        tiles.add(tile1);

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 + singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 + singleTileSize));

        Tile tile2 = new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 + singleTileSize, true);
        tile2.setDirDownStop(scrHeight / 2 - singleTileSize * 3);
        tiles.add(tile2);

        pushers.add(new Pusher(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));


    }

    @Override
    public void level9() {


        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize, Direction.LEFT));
        Tile tile1 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize);
        tile1.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);


        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize, Direction.LEFT));
        Tile tile2 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize);
        tile2.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);


        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 3, Direction.LEFT));
        Tile tile3 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 3);
        tile3.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 4));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 + singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 + singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 + singleTileSize * 2, true));


        tiles.add(tile3);
        tiles.add(tile2);
        tiles.add(tile1);

    }


    @Override
    public void level10() {

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 4));
        Tile tile3 = new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 3);
        tile3.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);
        tile3.setDirRightStop(scrWidth / 2 + singleTileSize);
        tiles.add(tile3);
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 2));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 + singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 + singleTileSize * 2));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 4));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 2));


        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2));


        Tile tile1 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize);
        tile1.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);
        tile1.setDirRightStop((scrWidth / 2 + singleTileSize));
        tiles.add(tile1);


        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize * 2));


        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 3, Direction.LEFT));
        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize, Direction.LEFT));
        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 + singleTileSize, Direction.RIGHT));
        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 3, Direction.RIGHT));


    }

    @Override
    public void level11() { //OK

        pushers.add(new Pusher(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 + singleTileSize, Direction.DOWN));
        pushers.add(new Pusher(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 + singleTileSize, Direction.DOWN));
        pushers.add(new Pusher(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4, Direction.UP));
        pushers.add(new Pusher(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4, Direction.UP));

        Tile tile2 = new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3);
        tile2.setDirUpStop(scrHeight / 2);
        tile2.setDirDownStop(scrHeight / 2 - singleTileSize * 3);
        tiles.add(tile2);

        Tile tile1 = new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3);
        tile1.setDirUpStop(scrHeight / 2);
        tile1.setDirDownStop(scrHeight / 2 - singleTileSize * 3);
        tiles.add(tile1);

        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3, true));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3, true));

        arrows.add(new DoubleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2, 0));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2, true));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize / 2, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 + singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 + singleTileSize));

    }

    @Override
    public void level12() {

        pushers.add(new Pusher(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        pushers.add(new Pusher(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        pushers.add(new Pusher(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 + singleTileSize * 2, Direction.DOWN));
        pushers.add(new Pusher(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 + singleTileSize * 2, Direction.DOWN));

        Tile tile1 = new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4);
        tile1.setDirUpStop(scrHeight / 2);
        tiles.add(tile1);
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4, true));
        Tile tile2 = new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4);
        tile2.setDirUpStop(scrHeight / 2);
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(tile2);

        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 + singleTileSize, true));
        Tile tile3 = new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 + singleTileSize);
        tile3.setDirDownStop(scrHeight / 2 - singleTileSize * 3);
        tiles.add(tile3);
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 + singleTileSize, true));
        Tile tile4 = new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 + singleTileSize);
        tile4.setDirDownStop(scrHeight / 2 - singleTileSize * 3);
        tiles.add(tile4);
        tiles.add(new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 + singleTileSize, true));

        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 + singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 + singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 + singleTileSize * 2, true));

    }

    @Override
    public void level13() {

        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize, Direction.LEFT));
        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 5, Direction.LEFT));
        shooters.add(new Shooter(scrWidth / 2 + singleTileSize * 2f, scrHeight / 2 - singleTileSize * 2, Direction.LEFT));
        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 2, 45));
        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 4, 45));
        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize * 3, scrHeight / 2, 135));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 5, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 4));

        Tile tile2 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 5);
        tile2.setDirLeftStop(scrWidth / 2);
        tiles.add(tile2);

        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 4, true));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 2));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2, true));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 + singleTileSize, true));

        Tile tile1 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize);
        tile1.setDirLeftStop(scrWidth / 2);
        tiles.add(tile1);

    }

    @Override
    public void level14() { //OK

        arrows.add(new DoubleArrow(scrWidth / 2 - 1f * singleTileSize, scrHeight / 2, 0));
        arrows.add(new DoubleArrow(scrWidth / 2, scrHeight / 2 - singleTileSize, 0));
        pushers.add(new Pusher(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        pushers.add(new Pusher(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));

        Tile tile1 = new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 - singleTileSize * 4);
        tile1.setDirUpStop(scrHeight / 2 - singleTileSize * 2);
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(tile1);
        Tile tile2 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 4);
        tile2.setDirUpStop(scrHeight / 2 - singleTileSize * 2);
        tiles.add(tile2);
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 4, true));

        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 2));

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize));


        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 + singleTileSize));

    }

    @Override
    public void level15() { //OK

        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize, true));
        arrows.add(new SingleArrow(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize, 90));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, true));
        arrows.add(new SingleArrow(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, 90));
        tiles.add(new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize, true));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3, true));
        arrows.add(new DoubleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3, 0));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3, true));

        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));

        shooters.add(new Shooter(scrWidth / 2 + 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, Direction.LEFT));


    }

    @Override
    public void level16() {

        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        pushers.add(new Pusher(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        arrows.add(new SingleArrow(scrWidth / 2, scrHeight / 2, 90));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 4, true));
        Tile tile1 = new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 4);
        tile1.setDirUpStop(scrHeight / 2 - singleTileSize);
        tiles.add(tile1);
        Tile tile2 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 4);
        tile2.setDirUpStop(scrHeight / 2 - singleTileSize * 2);
        tiles.add(tile2);
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize));


        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2, true));


        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 + singleTileSize + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize + singleTileSize));
    }

    @Override
    public void level17() {

        pushers.add(new Pusher(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        shooters.add(new Shooter(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize, Direction.RIGHT));
        shooters.add(new Shooter(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize, Direction.LEFT));
        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize, 45));
        deflectors.add(new Deflector(scrWidth / 2, scrHeight / 2 - singleTileSize, 135));
        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 4, 45));
        deflectors.add(new Deflector(scrWidth / 2, scrHeight / 2 - singleTileSize * 4, 135));
        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 + singleTileSize * 2, Direction.DOWN));
        pushers.add(new Pusher(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize * 2, Direction.DOWN));

        Tile tile1 = new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 4);
        tile1.setDirUpStop(scrHeight / 2 + singleTileSize);
        tile1.setDirDownStop(scrHeight / 2 - singleTileSize * 4);

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 4, true));

        tiles.add(tile1);
        Tile tile2 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 4);
        tile2.setDirUpStop(scrHeight / 2 + singleTileSize);
        tile2.setDirDownStop(scrHeight / 2 - singleTileSize * 4);
        tiles.add(tile2);
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 4, true));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 3, true));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 3, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 2));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 + singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 + singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 + singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize * 2));

    }

    @Override
    public void level18() {

        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize, Direction.LEFT));
        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 3, Direction.LEFT));
        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 + singleTileSize, Direction.RIGHT));
        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 3, Direction.RIGHT));

        Tile tile1 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 3);
        tile1.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);
        tile1.setDirRightStop(scrWidth / 2 + singleTileSize);

        Tile tile2 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize);
        tile2.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);
        tile2.setDirRightStop(scrWidth / 2 + singleTileSize);

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 5));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 4));
        tiles.add(tile1);
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2));
        tiles.add(tile2);
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize * 2));

    }

    @Override
    public void level19() {

        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 + singleTileSize * 2, Direction.DOWN));
        pushers.add(new Pusher(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize * 2, Direction.DOWN));
        pushers.add(new Pusher(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        pushers.add(new Pusher(scrWidth / 2, scrHeight / 2 - singleTileSize * 5, Direction.UP));

        Tile tile3 = new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 + singleTileSize, true);
        tile3.setDirDownStop(scrHeight / 2 - singleTileSize * 4);

        Tile tile4 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize, true);
        tile4.setDirDownStop(scrHeight / 2 - singleTileSize * 4);

        Tile tile1 = new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 4);
        tile1.setDirUpStop(scrHeight / 2 - singleTileSize * 2);

        Tile tile2 = new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 4, true);
        tile2.setDirUpStop(scrHeight / 2 - singleTileSize * 2);

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 5, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 5));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 5));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 5, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 4));
        tiles.add(tile1);
        tiles.add(tile2);
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 4));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize));


        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 + singleTileSize * 2));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize * 2));


        tiles.add(tile3);
        tiles.add(tile4);

    }

    @Override
    public void level20() {

        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize, Direction.LEFT));
        pushers.add(new Pusher(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 3, Direction.LEFT));
        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 + singleTileSize, Direction.RIGHT));
        pushers.add(new Pusher(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 3, Direction.RIGHT));
        pushers.add(new Pusher(scrWidth / 2, scrHeight / 2 - singleTileSize * 5, Direction.UP));

        arrows.add(new SingleArrow(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize, 90));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize, true));

        arrows.add(new SingleArrow(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize, 90));

        Tile tile2 = new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 4);
        tile2.setDirUpStop(scrHeight / 2 - singleTileSize * 2);
        Tile tile3 = new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 3);
        tile3.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);
        tile3.setDirRightStop(scrWidth / 2 + singleTileSize);


        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(tile2);
        tiles.add(tile3);

        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 2, true));


        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2, true));

        Tile tile1 = new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize);
        tile1.setDirLeftStop(scrWidth / 2 - singleTileSize * 2);
        tile1.setDirRightStop((scrWidth / 2 + singleTileSize));


        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2));


        tiles.add(tile1);


    }
}
