package game.peanutpanda.blocktoggler.levels;

import com.badlogic.gdx.utils.Array;

import game.peanutpanda.blocktoggler.gameobjects.ArrowShape;
import game.peanutpanda.blocktoggler.gameobjects.Deflector;
import game.peanutpanda.blocktoggler.gameobjects.Direction;
import game.peanutpanda.blocktoggler.gameobjects.DoubleArrow;
import game.peanutpanda.blocktoggler.gameobjects.Shooter;
import game.peanutpanda.blocktoggler.gameobjects.SingleArrow;
import game.peanutpanda.blocktoggler.gameobjects.Tile;

public class Chapter2 extends Chapter {

    public Chapter2() {

        setChapterNumber(2);

        levelMap.put(1, new Level("2-1", 1, 1));
        levelMap.put(2, new Level("2-2", 2, 2));
        levelMap.put(3, new Level("2-3", 3, 3));
        levelMap.put(4, new Level("2-4", 4, 3));
        levelMap.put(5, new Level("2-5", 5, 2));
        levelMap.put(6, new Level("2-6", 6, 2));
        levelMap.put(7, new Level("2-7", 7, 7));
        levelMap.put(8, new Level("2-8", 8, 4));
        levelMap.put(9, new Level("2-9", 9, 3));
        levelMap.put(10, new Level("2-10", 10, 2));
        levelMap.put(11, new Level("2-11", 11, 3));
        levelMap.put(12, new Level("2-12", 12, 4));
        levelMap.put(13, new Level("2-13", 13, 4));
        levelMap.put(14, new Level("2-14", 14, 6));
        levelMap.put(15, new Level("2-15", 15, 3));
        levelMap.put(16, new Level("2-16", 16, 9));
        levelMap.put(17, new Level("2-17", 17, 6));
        levelMap.put(18, new Level("2-18", 18, 5));
        levelMap.put(19, new Level("2-19", 19, 4));
        levelMap.put(20, new Level("2-20", 20, 5));

    }

    @Override
    public void start(Level level) {

        tiles = new Array<Tile>();
        arrows = new Array<ArrowShape>();
        shooters = new Array<Shooter>();
        deflectors = new Array<Deflector>();
        level.setActiveCount(0);

        switch (level.getCurrentLevel()) {
            case 1:
                level1();
                break;
            case 2:
                level2();
                level.setActiveCount(1);
                break;
            case 3:
                level3();
                level.setActiveCount(5);
                break;
            case 4:
                level4();
                level.setActiveCount(1);
                break;
            case 5:
                level5();
                level.setActiveCount(1);
                break;
            case 6:
                level6();
                level.setActiveCount(1);
                break;
            case 7:
                level7();
                level.setActiveCount(8);
                break;
            case 8:
                level8();
                level.setActiveCount(2);
                break;
            case 9:
                level9();
                level.setActiveCount(4);
                break;
            case 10:
                level10();
                level.setActiveCount(3);
                break;
            case 11:
                level11();
                break;
            case 12:
                level12();
                break;
            case 13:
                level13();
                level.setActiveCount(2);
                break;
            case 14:
                level14();
                level.setActiveCount(3);
                break;
            case 15:
                level15();
                break;
            case 16:
                level16();
                level.setActiveCount(5);
                break;
            case 17:
                level17();
                level.setActiveCount(5);
                break;
            case 18:
                level18();
                level.setActiveCount(2);
                break;
            case 19:
                level19();
                level.setActiveCount(2);
                break;
            case 20:
                level20();
                level.setActiveCount(5);
                break;
        }
    }

    @Override
    public void level1() {

        for (int i = 0; i < 4; i++) {
            tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + (i * singleTileSize), scrHeight / 2 - singleTileSize * 2));
        }
        shooters.add(new Shooter(scrWidth - singleTileSize, scrHeight / 2 - singleTileSize * 2, Direction.LEFT));

    }

    @Override
    public void level2() {

        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));

        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 + singleTileSize));

        shooters.add(new Shooter(scrWidth / 2 + 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, Direction.LEFT));
        shooters.add(new Shooter(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3, Direction.UP));

    }

    @Override
    public void level3() {

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2, true));

        shooters.add(new Shooter(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2, Direction.LEFT));
        shooters.add(new Shooter(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3.5f, Direction.UP));

    }

    @Override
    public void level4() {

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2));

        shooters.add(new Shooter(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2, Direction.LEFT));
        shooters.add(new Shooter(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3.5f, Direction.UP));
    }

    @Override
    public void level5() {

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 2));

        shooters.add(new Shooter(scrWidth / 2 + singleTileSize * 2, scrHeight / 2, Direction.LEFT));

        deflectors.add(new Deflector(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2, 45));

    }

    @Override
    public void level6() {

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 2, true));

        shooters.add(new Shooter(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 2, Direction.LEFT));

        deflectors.add(new Deflector(scrWidth - singleTileSize * 6, scrHeight / 2 - singleTileSize * 2, 45));
        deflectors.add(new Deflector(scrWidth - singleTileSize * 6, scrHeight / 2, 45));
        deflectors.add(new Deflector(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2, 45));

    }

    @Override
    public void level7() { //OK

        shooters.add(new Shooter(scrWidth / 2 + 2.5f * singleTileSize, scrHeight / 2, Direction.LEFT));

        arrows.add(new DoubleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, 0));


        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize, true));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize / 2, scrHeight / 2 - singleTileSize, true));

        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2, true));

        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));

        shooters.add(new Shooter(scrWidth / 2 + 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, Direction.LEFT));

    }

    @Override
    public void level8() {

        shooters.add(new Shooter(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 + singleTileSize, Direction.RIGHT));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize / 2, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2));

        shooters.add(new Shooter(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2, Direction.LEFT));

        deflectors.add(new Deflector(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 + singleTileSize, 45));

        shooters.add(new Shooter(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, Direction.RIGHT));

        shooters.add(new Shooter(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2 - singleTileSize * 3, Direction.LEFT));

        deflectors.add(new Deflector(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3, 45));

    }

    @Override
    public void level9() {

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 5));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 5, true));

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 3, true));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize * 3));

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 - singleTileSize, true));

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize + singleTileSize, scrHeight / 2 + singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 + singleTileSize));

        shooters.add(new Shooter(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 - singleTileSize * 5, Direction.LEFT));

        deflectors.add(new Deflector(scrWidth - singleTileSize * 5.5f, scrHeight / 2 - singleTileSize * 5, 45));
        deflectors.add(new Deflector(scrWidth - singleTileSize * 5.5f, scrHeight / 2 + singleTileSize * 2, 45));
        deflectors.add(new Deflector(scrWidth / 2 - 1f * singleTileSize + singleTileSize, scrHeight / 2 + singleTileSize * 2, 45));

    }

    @Override
    public void level10() {

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));

        shooters.add(new Shooter(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2 - singleTileSize, Direction.LEFT));
        deflectors.add(new Deflector(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 - singleTileSize, 135));
        deflectors.add(new Deflector(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 - singleTileSize * 2, 225));
    }

    @Override
    public void level11() {

        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 + singleTileSize));
        deflectors.add(new Deflector(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 + singleTileSize * 2, 135));
        shooters.add(new Shooter(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        deflectors.add(new Deflector(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4, 135));


        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 + singleTileSize));
        shooters.add(new Shooter(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));

        tiles.add(new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 + singleTileSize));
        deflectors.add(new Deflector(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 + singleTileSize * 2, 135));
        shooters.add(new Shooter(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        deflectors.add(new Deflector(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4, 135));

    }

    @Override
    public void level12() {

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 3));

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 2));

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 1f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2));

        shooters.add(new Shooter(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2, Direction.LEFT));
        shooters.add(new Shooter(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2 - singleTileSize * 3, Direction.LEFT));

    }

    @Override
    public void level13() {

        shooters.add(new Shooter(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 + singleTileSize, Direction.RIGHT));
        deflectors.add(new Deflector(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 + singleTileSize, 45));
        arrows.add(new SingleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2, 90));
        arrows.add(new SingleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, 90));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 + singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 + singleTileSize));

        deflectors.add(new Deflector(scrWidth / 2 + 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3, 135));
    }

    @Override
    public void level14() {

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4, true));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3));

        shooters.add(new Shooter(scrWidth / 2 + 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3, Direction.LEFT));
        deflectors.add(new Deflector(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 - singleTileSize * 3, 135));
        deflectors.add(new Deflector(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 - singleTileSize * 4, 225));
        deflectors.add(new Deflector(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2 - singleTileSize * 4, 225));

        tiles.add(new Tile(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 + singleTileSize * 2));

        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 + singleTileSize * 2));

        tiles.add(new Tile(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2 + singleTileSize * 2));

        shooters.add(new Shooter(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 5.5f, Direction.UP));

    }

    @Override
    public void level15() {

        shooters.add(new Shooter(scrWidth / 2 - singleTileSize * 0.5f, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize * 0.5f, scrHeight / 2 - singleTileSize * 3, 45));

        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize * 2.5f, scrHeight / 2 - singleTileSize * 3, 45));
        deflectors.add(new Deflector(scrWidth / 2 + singleTileSize * 1.5f, scrHeight / 2 - singleTileSize * 3, 45));

        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize * 2.5f, scrHeight / 2 + singleTileSize * 2, 45));
        deflectors.add(new Deflector(scrWidth / 2 + singleTileSize * 1.5f, scrHeight / 2 + singleTileSize * 2, 45));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2.5f, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2.5f, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2.5f, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2.5f, scrHeight / 2 + singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 0.5f, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 0.5f, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 0.5f, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 0.5f, scrHeight / 2 + singleTileSize));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 1.5f, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 1.5f, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 1.5f, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 1.5f, scrHeight / 2 + singleTileSize));

    }

    @Override
    public void level16() {

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2f, scrHeight / 2 - singleTileSize * 4));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2f, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2f, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2f, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2f, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2f, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2f, scrHeight / 2 + singleTileSize * 2));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 4));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 4));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 4));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2f, scrHeight / 2 - singleTileSize * 4, true));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 1f, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 2, true));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 1f, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 1f, scrHeight / 2 + singleTileSize * 2));

        shooters.add(new Shooter(scrWidth / 2 + singleTileSize * 2f, scrHeight / 2 - singleTileSize * 5f, Direction.UP));
        shooters.add(new Shooter(scrWidth / 2 - singleTileSize * 3f, scrHeight / 2 - singleTileSize * 5f, Direction.UP));
        deflectors.add(new Deflector(scrWidth / 2 + singleTileSize * 2f, scrHeight / 2 - singleTileSize * 2, 45));
        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize * 3f, scrHeight / 2 - singleTileSize * 2, 45));
        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize * 3f, scrHeight / 2, 45));
        deflectors.add(new Deflector(scrWidth / 2 + singleTileSize * 2f, scrHeight / 2, 45));
        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize * 3f, scrHeight / 2 + singleTileSize * 2, 45));
        deflectors.add(new Deflector(scrWidth / 2 + singleTileSize * 2f, scrHeight / 2 + singleTileSize * 2, 45));

    }


    @Override
    public void level17() { //OK

        for (int i = 0; i < 3; i++) {
            tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (i * singleTileSize), scrHeight / 2 - singleTileSize * 3));
        }

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        arrows.add(new SingleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, 90));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2, true));
        arrows.add(new SingleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2, 90));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize / 2, scrHeight / 2, true));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 + singleTileSize));

        shooters.add(new Shooter(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        shooters.add(new Shooter(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));

    }

    @Override
    public void level18() {


        arrows.add(new SingleArrow(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 2, 90));
        arrows.add(new SingleArrow(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize, 0));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 4));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 4));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 3, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 3));

        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 2, true));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize * 2, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2, true));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize * 2, scrHeight / 2 + singleTileSize));

        shooters.add(new Shooter(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 5, Direction.UP));
        shooters.add(new Shooter(scrWidth / 2, scrHeight / 2 + singleTileSize * 2, Direction.DOWN));


    }

    @Override
    public void level19() {

        deflectors.add(new Deflector(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 - singleTileSize * 4.5f, 45));
        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 - singleTileSize * 4.5f));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 4.5f));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 4.5f));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 4.5f));
        shooters.add(new Shooter(scrWidth / 2 + 2 * singleTileSize, scrHeight / 2 - singleTileSize * 4.5f, Direction.LEFT));

        tiles.add(new Tile(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 - singleTileSize * 3.5f));
        tiles.add(new Tile(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 - singleTileSize * 2.5f, true));
        tiles.add(new Tile(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 - singleTileSize * 1.5f));
        tiles.add(new Tile(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 - singleTileSize * 0.5f, true));
        tiles.add(new Tile(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 + singleTileSize * 0.5f));
        deflectors.add(new Deflector(scrWidth / 2 - 3f * singleTileSize, scrHeight / 2 + singleTileSize * 1.5f, 135));

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 + singleTileSize * 1.5f));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 + singleTileSize * 1.5f));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 + singleTileSize * 1.5f));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize * 1.5f));
        deflectors.add(new Deflector(scrWidth / 2 + 2 * singleTileSize, scrHeight / 2 + singleTileSize * 1.5f, 45));

        tiles.add(new Tile(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2 - singleTileSize * 2.5f));
        tiles.add(new Tile(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2 - singleTileSize * 1.5f));
        tiles.add(new Tile(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2 - singleTileSize * 0.5f));
        tiles.add(new Tile(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2 + singleTileSize * 0.5f));
        deflectors.add(new Deflector(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2 - singleTileSize * 3.5f, 135));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 3.5f));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 3.5f));
        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 3.5f));
        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize * 2, scrHeight / 2 - singleTileSize * 3.5f, 45));

        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 - singleTileSize * 2.5f));
        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 - singleTileSize * 1.5f));
        tiles.add(new Tile(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 - singleTileSize * 0.5f));
        deflectors.add(new Deflector(scrWidth / 2 - 2f * singleTileSize, scrHeight / 2 + singleTileSize * 0.5f, 135));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 + singleTileSize * 0.5f));
        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 + singleTileSize * 0.5f));
        deflectors.add(new Deflector(scrWidth / 2 + singleTileSize, scrHeight / 2 + singleTileSize * 0.5f, 45));

        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 0.5f));
        tiles.add(new Tile(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 1.5f));
        deflectors.add(new Deflector(scrWidth / 2 + singleTileSize, scrHeight / 2 - singleTileSize * 2.5f, 135));

        tiles.add(new Tile(scrWidth / 2, scrHeight / 2 - singleTileSize * 2.5f));
        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 2.5f, 45));

        tiles.add(new Tile(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 1.5f));
        deflectors.add(new Deflector(scrWidth / 2 - singleTileSize, scrHeight / 2 - singleTileSize * 0.5f, 135));

        deflectors.add(new Deflector(scrWidth / 2, scrHeight / 2 - singleTileSize * 0.5f, 45));
        deflectors.add(new Deflector(scrWidth / 2, scrHeight / 2 - singleTileSize * 1.5f, 135));


    }

    @Override
    public void level20() {

        shooters.add(new Shooter(scrWidth / 2 + 2f * singleTileSize, scrHeight / 2 - singleTileSize, Direction.LEFT));
        shooters.add(new Shooter(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 4.5f, Direction.UP));
        deflectors.add(new Deflector(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize, 135));
        deflectors.add(new Deflector(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 3, 45));
        deflectors.add(new Deflector(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2, 135));
        deflectors.add(new Deflector(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2, 45));

        arrows.add(new DoubleArrow(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, 0));

        tiles.add(new Tile(scrWidth / 2 - 2.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize * 2, true));

        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize, scrHeight / 2 - singleTileSize));
        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2 - singleTileSize, true));
        tiles.add(new Tile(scrWidth / 2 + 0.5f * singleTileSize, scrHeight / 2 - singleTileSize));

        tiles.add(new Tile(scrWidth / 2 - 0.5f * singleTileSize, scrHeight / 2));


        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (0), scrHeight / 2 - singleTileSize * 3, true));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (singleTileSize), scrHeight / 2 - singleTileSize * 3));
        tiles.add(new Tile(scrWidth / 2 - 1.5f * singleTileSize + (2 * singleTileSize), scrHeight / 2 - singleTileSize * 3, true));

    }
}
