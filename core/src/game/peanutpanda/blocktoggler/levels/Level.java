package game.peanutpanda.blocktoggler.levels;

public class Level {

    private int currentLevel;
    private int minimumMoves;
    private int activeCount;
    private boolean unlocked = false;
    private boolean completed = false;
    private int stars;
    private String levelName;

    public Level(String levelName, int currentLevel, int minimumMoves) {
        this.levelName = levelName;
        this.currentLevel = currentLevel;
        this.minimumMoves = minimumMoves;
    }

    public void addStars(int stars) {

        if (this.stars < stars) {
            int result = stars - this.stars;
            this.stars += result;
        }
    }

    public String levelAsString() {

        String number;

        if (currentLevel < 10) {
            number = 0 + "" + currentLevel;
        } else {
            number = "" + currentLevel;
        }
        return number;
    }

    public void changeActiveTiles(int i) {

        switch (i) {
            case -1:
                activeCount--;
                break;
            case +1:
                activeCount++;
                break;
        }
    }


    public int getCurrentLevel() {
        return currentLevel;
    }

    public int getMinimumMoves() {
        return minimumMoves;
    }

    public int getActiveCount() {
        return activeCount;
    }

    public void setActiveCount(int activeCount) {
        this.activeCount = activeCount;
    }

    public boolean isUnlocked() {
        return unlocked;
    }

    public void setUnlocked(boolean unlocked) {
        this.unlocked = unlocked;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public String getLevelName() {
        return levelName;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
