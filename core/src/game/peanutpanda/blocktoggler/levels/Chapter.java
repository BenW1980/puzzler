package game.peanutpanda.blocktoggler.levels;

import com.badlogic.gdx.utils.Array;

import java.util.HashMap;

import game.peanutpanda.blocktoggler.gamedata.ScreenSize;
import game.peanutpanda.blocktoggler.gameobjects.ArrowShape;
import game.peanutpanda.blocktoggler.gameobjects.Deflector;
import game.peanutpanda.blocktoggler.gameobjects.Pusher;
import game.peanutpanda.blocktoggler.gameobjects.Shape;
import game.peanutpanda.blocktoggler.gameobjects.Shooter;
import game.peanutpanda.blocktoggler.gameobjects.Tile;

@SuppressWarnings("UseSparseArrays")
public abstract class Chapter {

    protected int singleTileSize = Shape.SINGLE_TILE_SIZE;
    protected int scrWidth = ScreenSize.WIDTH.getSize();
    protected int scrHeight = ScreenSize.HEIGHT.getSize();
    protected HashMap<Integer, Level> levelMap;
    protected Array<Tile> tiles;
    protected Array<ArrowShape> arrows;
    protected Array<Shooter> shooters;
    protected Array<Deflector> deflectors;
    protected Array<Pusher> pushers;
    protected int chapterNumber;
    protected boolean unlocked = false;

    public Chapter() {
        tiles = new Array<Tile>();
        arrows = new Array<ArrowShape>();
        shooters = new Array<Shooter>();
        deflectors = new Array<Deflector>();
        pushers = new Array<Pusher>();
        levelMap = new HashMap<Integer, Level>();

    }

    public String starsAsString() {

        int result = 0;
        String number;

        for (Level level : levelMap.values()) {
            result += level.getStars();
        }

        if (result < 10) {
            number = 0 + "" + result;
        } else {
            number = "" + result;
        }

        return number;
    }

    public int stars() {

        int result = 0;

        for (Level level : levelMap.values()) {
            result += level.getStars();
        }


        return result;
    }


    public abstract void start(Level level);

    public abstract void level1();

    public abstract void level2();

    public abstract void level3();

    public abstract void level4();

    public abstract void level5();

    public abstract void level6();

    public abstract void level7();

    public abstract void level8();

    public abstract void level9();

    public abstract void level10();

    public abstract void level11();

    public abstract void level12();

    public abstract void level13();

    public abstract void level14();

    public abstract void level15();

    public abstract void level16();

    public abstract void level17();

    public abstract void level18();

    public abstract void level19();

    public abstract void level20();


    public HashMap<Integer, Level> getLevelMap() {
        return levelMap;
    }

    public Array<Tile> getTiles() {
        return tiles;
    }

    public Array<ArrowShape> getArrows() {
        return arrows;
    }

    public Array<Shooter> getShooters() {
        return shooters;
    }

    public Array<Deflector> getDeflectors() {
        return deflectors;
    }

    public Array<Pusher> getPushers() {
        return pushers;
    }

    public int getChapterNumber() {
        return chapterNumber;
    }

    public void setChapterNumber(int chapterNumber) {
        this.chapterNumber = chapterNumber;
    }

    public boolean isUnlocked() {
        return unlocked;
    }

    public void setUnlocked(boolean unlocked) {
        this.unlocked = unlocked;
    }
}
