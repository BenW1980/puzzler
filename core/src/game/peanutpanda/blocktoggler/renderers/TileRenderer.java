package game.peanutpanda.blocktoggler.renderers;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import game.peanutpanda.blocktoggler.gamedata.AssetLoader;
import game.peanutpanda.blocktoggler.gameobjects.ArrowShape;
import game.peanutpanda.blocktoggler.gameobjects.Deflector;
import game.peanutpanda.blocktoggler.gameobjects.Direction;
import game.peanutpanda.blocktoggler.gameobjects.DoubleArrow;
import game.peanutpanda.blocktoggler.gameobjects.Pusher;
import game.peanutpanda.blocktoggler.gameobjects.Shooter;
import game.peanutpanda.blocktoggler.gameobjects.SingleArrow;
import game.peanutpanda.blocktoggler.gameobjects.Tile;

public class TileRenderer {

    private SpriteBatch batch;
    private AssetLoader assetLoader;

    public TileRenderer(SpriteBatch batch, AssetLoader assetLoader) {
        this.batch = batch;
        this.assetLoader = assetLoader;
    }

    public void render(Array<Tile> tiles, Array<ArrowShape> arrows, Array<Shooter> shooters, Array<Deflector> deflectors, Array<Pusher> pushers) {
        setTexture(tiles, arrows, shooters, deflectors, pushers);
        draw(tiles, arrows, shooters, deflectors, pushers);
    }


    private void draw(Array<Tile> tiles, Array<ArrowShape> arrows, Array<Shooter> shooters, Array<Deflector> deflectors, Array<Pusher> pushers) {

        for (Pusher pusher : pushers) {
            pusher.getSprite().setBounds(pusher.getRectangle().x, pusher.getRectangle().y, pusher.getRectangle().width, pusher.getRectangle().height);
            pusher.getBackground().setBounds(pusher.getBackgroundRect().x, pusher.getBackgroundRect().y, pusher.getBackgroundRect().width, pusher.getBackgroundRect().height);
            pusher.getBackground().draw(batch);
            pusher.getSprite().draw(batch);
        }

        for (Deflector deflector : deflectors) {

            if (deflector.isHitByProjectile() || deflector.isTappedByPlayer()) {
                deflector.getSprite().setBounds(deflector.getRectangle().x, deflector.getRectangle().y, deflector.getRectangle().width + 3, deflector.getRectangle().height + 3);
            } else {
                deflector.getSprite().setBounds(deflector.getRectangle().x, deflector.getRectangle().y, deflector.getRectangle().width, deflector.getRectangle().height);
            }

            deflector.getSprite().setOriginCenter();
            deflector.getSprite().setRotation(deflector.getAngle());


            deflector.getSprite().draw(batch);
        }

        for (Shooter shooter : shooters) {
            if (!shooter.isTappedByPlayer()) {
                shooter.getSprite().setBounds(shooter.getRectangle().x, shooter.getRectangle().y, shooter.getRectangle().width, shooter.getRectangle().height);
                shooter.getProjectile().getSprite().setBounds(shooter.getProjectile().getRectangle().x, shooter.getProjectile().getRectangle().y, shooter.getProjectile().getRectangle().width, shooter.getProjectile().getRectangle().height);
            } else {
                shooter.getSprite().setBounds(shooter.getRectangle().x, shooter.getRectangle().y, shooter.getRectangle().width + 3, shooter.getRectangle().height + 3);
                shooter.getProjectile().getSprite().setBounds(shooter.getProjectile().getRectangle().x, shooter.getProjectile().getRectangle().y, shooter.getProjectile().getRectangle().width + 3, shooter.getProjectile().getRectangle().height + 3);
            }
            shooter.getSprite().draw(batch);
            shooter.getProjectile().getSprite().draw(batch);
        }

        for (ArrowShape arrow : arrows) {
            if (!arrow.isTappedByPlayer()) {
                arrow.getSprite().setBounds(arrow.getRectangle().x, arrow.getRectangle().y, arrow.getRectangle().width, arrow.getRectangle().height);
            } else {
                arrow.getSprite().setBounds(arrow.getRectangle().x, arrow.getRectangle().y, arrow.getRectangle().width + 2, arrow.getRectangle().height + 2);
            }

            arrow.getSprite().setOriginCenter();
            arrow.getSprite().setRotation(arrow.getAngle());
            arrow.getSprite().draw(batch);
        }


        for (Tile tile : tiles) {

            tile.getSprite().setBounds(tile.getRectangle().x, tile.getRectangle().y, tile.getRectangle().width, tile.getRectangle().height);

            if (!tile.isPressedDown()) {
                tile.getSprite().draw(batch);
            } else {
                batch.draw(tile.getSprite(), tile.getRectangle().x, tile.getRectangle().y, tile.getRectangle().width + 3, tile.getRectangle().height + 3);
            }
        }

        for (Shooter shooter : shooters) {
            for (Tile tile : tiles) {
                if (!shooter.getProjectile().getCenterRect().overlaps(tile.getRectangle())) {
                    batch.draw(tile.getSprite(), tile.getRectangle().x, tile.getRectangle().y, tile.getRectangle().width, tile.getRectangle().height);
                } else {
                    batch.draw(tile.getSprite(), tile.getRectangle().x, tile.getRectangle().y, tile.getRectangle().width + 3, tile.getRectangle().height + 3);
                }
            }
        }
    }

    private void setTexture(Array<Tile> tiles, Array<ArrowShape> arrows, Array<Shooter> shooters, Array<Deflector> deflectors, Array<Pusher> pushers) {

        for (Pusher pusher : pushers) {

            if (pusher.getDirection() == Direction.LEFT) {
                pusher.setBackground(new Sprite(assetLoader.pusherHubLeft()));
                pusher.setSprite(new Sprite(assetLoader.pusherLeft()));


            } else if (pusher.getDirection() == Direction.RIGHT) {
                pusher.setBackground(new Sprite(assetLoader.pusherHubRight()));
                pusher.setSprite(new Sprite(assetLoader.pusherRight()));

            } else if (pusher.getDirection() == Direction.UP) {
                pusher.setBackground(new Sprite(assetLoader.pusherHubUp()));
                pusher.setSprite(new Sprite(assetLoader.pusherUp()));

            } else if (pusher.getDirection() == Direction.DOWN) {
                pusher.setBackground(new Sprite(assetLoader.pusherHubDown()));
                pusher.setSprite(new Sprite(assetLoader.pusherDown()));
            }
        }

        for (Deflector deflector : deflectors) {

            if (deflector.isHitByProjectile() || deflector.isTappedByPlayer()) {
                deflector.setSprite(new Sprite(assetLoader.deflectorHit()));
            } else {
                deflector.setSprite(new Sprite(assetLoader.deflector()));
            }
        }

        for (Shooter shooter : shooters) {

            if (shooter.getDirection() == Direction.UP) {
                if (shooter.isTappedByPlayer()) {
                    shooter.setSprite(new Sprite(assetLoader.shooterUpWhite()));
                } else {
                    shooter.setSprite(new Sprite(assetLoader.shooterUp()));
                }
                shooter.getProjectile().setSprite(new Sprite(assetLoader.projectileUp()));

            } else if (shooter.getDirection() == Direction.DOWN) {
                if (shooter.isTappedByPlayer()) {
                    shooter.setSprite(new Sprite(assetLoader.shooterDownWhite()));
                } else {
                    shooter.setSprite(new Sprite(assetLoader.shooterDown()));
                }
                shooter.getProjectile().setSprite(new Sprite(assetLoader.projectileDown()));

            } else if (shooter.getDirection() == Direction.LEFT) {
                if (shooter.isTappedByPlayer()) {
                    shooter.setSprite(new Sprite(assetLoader.shooterLeftWhite()));
                } else {
                    shooter.setSprite(new Sprite(assetLoader.shooterLeft()));
                }
                shooter.getProjectile().setSprite(new Sprite(assetLoader.projectileLeft()));

            } else if (shooter.getDirection() == Direction.RIGHT) {
                if (shooter.isTappedByPlayer()) {
                    shooter.setSprite(new Sprite(assetLoader.shooterRighWhite()));
                } else {
                    shooter.setSprite(new Sprite(assetLoader.shooterRight()));
                }
                shooter.getProjectile().setSprite(new Sprite(assetLoader.projectileRight()));
            }
        }

        for (Tile tile : tiles) {

            if (tile.isTouched()) {
                tile.setSprite(new Sprite(assetLoader.tileRed2()));

            } else {
                tile.setSprite(new Sprite(assetLoader.tileBlue2()));
            }

            if (tile.isDone()) {
                tile.setSprite(new Sprite(assetLoader.tileYellow2()));
            }
        }

        for (ArrowShape arrow : arrows) {
            if (arrow instanceof SingleArrow) {
                if (arrow.isTappedByPlayer()) {
                    arrow.setSprite(new Sprite(assetLoader.singleArrowTouched()));
                } else {
                    arrow.setSprite(new Sprite(assetLoader.singleArrow()));
                }

            } else if (arrow instanceof DoubleArrow) {
                if (arrow.isTappedByPlayer()) {
                    arrow.setSprite(new Sprite(assetLoader.doubleArrowTouched()));
                } else {
                    arrow.setSprite(new Sprite(assetLoader.doubleArrow()));
                }
            }
        }
    }

}
