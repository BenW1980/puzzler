package game.peanutpanda.blocktoggler.renderers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

import game.peanutpanda.blocktoggler.gamedata.AssetLoader;
import game.peanutpanda.blocktoggler.gamedata.AudioHandler;
import game.peanutpanda.blocktoggler.gamedata.ScreenSize;

public class MainScreenRenderer {

    private boolean titleReady = false;
    private int width = 72;
    private int height = 72;
    private int space = width + 5;
    private int left = 15;
    private int upper = 120;
    private int lower = upper + space;
    private float startPos = ScreenSize.HEIGHT.getSize();

    private Rectangle bRect;
    private Rectangle lRect;
    private Rectangle o1Rect;
    private Rectangle cRect;
    private Rectangle kRect;
    private Rectangle sRect;

    private Rectangle tRect;
    private Rectangle o2Rect;
    private Rectangle g1Rect;
    private Rectangle g2Rect;
    private Rectangle l2Rect;
    private Rectangle eRect;

    private AudioHandler audioHandler;
    private Array<Rectangle> titleBlockArray;

    public MainScreenRenderer(AudioHandler audioHandler) {
        this.audioHandler = audioHandler;

        bRect = new Rectangle(left, startPos, width, height); //1
        lRect = new Rectangle(left + space, startPos, width, height); //2
        o1Rect = new Rectangle(left + space * 2, startPos, width, height); //3
        cRect = new Rectangle(left + space * 3, startPos, width, height); //4
        kRect = new Rectangle(left + space * 4, startPos, width, height); //5
        sRect = new Rectangle(left + space * 5, startPos, width, height); //6

        tRect = new Rectangle(left, startPos, width, height); //7
        o2Rect = new Rectangle(left + space, startPos, width, height); //8
        g1Rect = new Rectangle(left + space * 2, startPos, width, height); //9
        g2Rect = new Rectangle(left + space * 3, startPos, width, height); //10
        l2Rect = new Rectangle(left + space * 4, startPos, width, height); //11
        eRect = new Rectangle(left + space * 5, startPos, width, height); //12

        titleBlockArray = new Array<Rectangle>();

        titleBlockArray.add(bRect);
        titleBlockArray.add(lRect);
        titleBlockArray.add(o1Rect);
        titleBlockArray.add(cRect);
        titleBlockArray.add(kRect);
        titleBlockArray.add(sRect);

        titleBlockArray.add(tRect);
        titleBlockArray.add(o2Rect);
        titleBlockArray.add(g1Rect);
        titleBlockArray.add(g2Rect);
        titleBlockArray.add(l2Rect);
        titleBlockArray.add(eRect);
    }

    public void drawTitle(AssetLoader assetLoader, SpriteBatch batch) {

        batch.draw(assetLoader.B3(), bRect.x, bRect.y, bRect.width, bRect.height); //1
        batch.draw(assetLoader.L4(), lRect.x, lRect.y, lRect.width, lRect.height); //1
        batch.draw(assetLoader.O1(), o1Rect.x, o1Rect.y, o1Rect.width, o1Rect.height); //1
        batch.draw(assetLoader.C10(), cRect.x, cRect.y, cRect.width, cRect.height); //1
        batch.draw(assetLoader.K11(), kRect.x, kRect.y, kRect.width, kRect.height); //1
        batch.draw(assetLoader.S12(), sRect.x, sRect.y, sRect.width, sRect.height); //1
        batch.draw(assetLoader.T6(), tRect.x, tRect.y, tRect.width, tRect.height); //1
        batch.draw(assetLoader.O5(), o2Rect.x, o2Rect.y, o2Rect.width, o2Rect.height); //1
        batch.draw(assetLoader.G2(), g1Rect.x, g1Rect.y, g1Rect.width, g1Rect.height); //1
        batch.draw(assetLoader.G7(), g2Rect.x, g2Rect.y, g2Rect.width, g2Rect.height); //1
        batch.draw(assetLoader.L8(), l2Rect.x, l2Rect.y, l2Rect.width, l2Rect.height); //1
        batch.draw(assetLoader.E9(), eRect.x, eRect.y, eRect.width, eRect.height); //1

    }

//    public void playSound() {
//
//        for (int i = 0; i < titleBlockArray.size; i++) {
//            if (i < 5) {
//                if (titleBlockArray.get(i).y < ScreenSize.HEIGHT.getSize() - lower / 2 + 30
//                        && titleBlockArray.get(i).y > ScreenSize.HEIGHT.getSize() - lower / 2 + 20) {
//                    audioHandler.playPlopSound();
//                }
//
//            } else {
//
//                if (titleBlockArray.get(i).y < ScreenSize.HEIGHT.getSize() - upper / 2 + 10
//                        && titleBlockArray.get(i).y > ScreenSize.HEIGHT.getSize() - upper / 2) {
//                    audioHandler.playPlopSound();
//                }
//            }
//        }
//    }

    public void dropTitle() {

        float progress = 0.2f;
        boolean dropped1 = false;
        boolean dropped2 = false;
        boolean dropped3 = false;
        boolean dropped4 = false;
        boolean dropped5 = false;
        boolean dropped6 = false;
        boolean dropped7 = false;
        boolean dropped8 = false;
        boolean dropped9 = false;
        boolean dropped10 = false;
        boolean dropped11 = false;

        titleBlockArray.get(0).y = MathUtils.lerp(titleBlockArray.get(0).y, ScreenSize.HEIGHT.getSize() - lower, progress);

        if (titleBlockArray.get(0).y < ScreenSize.HEIGHT.getSize() - lower / 2) {
            dropped1 = true;
        }
        if (titleBlockArray.get(0).y < ScreenSize.HEIGHT.getSize() - lower - 2) {
            titleBlockArray.get(0).y = ScreenSize.HEIGHT.getSize() - lower;
        }
        //-------------------------
        if (dropped1) {
            titleBlockArray.get(1).y = MathUtils.lerp(titleBlockArray.get(1).y, ScreenSize.HEIGHT.getSize() - lower, progress);
            if (titleBlockArray.get(1).y < ScreenSize.HEIGHT.getSize() - lower / 2) {
                dropped2 = true;
            }
            if (titleBlockArray.get(1).y < ScreenSize.HEIGHT.getSize() - lower - 2) {
                titleBlockArray.get(1).y = ScreenSize.HEIGHT.getSize() - lower;
            }
        }
        //-------------------------
        if (dropped2) {
            titleBlockArray.get(2).y = MathUtils.lerp(titleBlockArray.get(2).y, ScreenSize.HEIGHT.getSize() - lower, progress);
            if (titleBlockArray.get(2).y < ScreenSize.HEIGHT.getSize() - lower / 2) {
                dropped3 = true;
            }
            if (titleBlockArray.get(2).y < ScreenSize.HEIGHT.getSize() - lower - 2) {
                titleBlockArray.get(2).y = ScreenSize.HEIGHT.getSize() - lower;
            }
        }
        //-------------------------
        if (dropped3) {
            titleBlockArray.get(3).y = MathUtils.lerp(titleBlockArray.get(3).y, ScreenSize.HEIGHT.getSize() - lower, progress);
            if (titleBlockArray.get(3).y < ScreenSize.HEIGHT.getSize() - lower / 2) {
                dropped4 = true;
            }
            if (titleBlockArray.get(3).y < ScreenSize.HEIGHT.getSize() - lower - 2) {
                titleBlockArray.get(3).y = ScreenSize.HEIGHT.getSize() - lower;
            }
        }
        //-------------------------
        if (dropped4) {
            titleBlockArray.get(4).y = MathUtils.lerp(titleBlockArray.get(4).y, ScreenSize.HEIGHT.getSize() - lower, progress);
            if (titleBlockArray.get(4).y < ScreenSize.HEIGHT.getSize() - lower / 2) {
                dropped5 = true;
            }
            if (titleBlockArray.get(4).y < ScreenSize.HEIGHT.getSize() - lower - 2) {
                titleBlockArray.get(4).y = ScreenSize.HEIGHT.getSize() - lower;
            }
        }
        //-------------------------
        if (dropped5) {
            titleBlockArray.get(5).y = MathUtils.lerp(titleBlockArray.get(5).y, ScreenSize.HEIGHT.getSize() - lower, progress);
            if (titleBlockArray.get(5).y < ScreenSize.HEIGHT.getSize() - lower / 2) {
                dropped6 = true;
                titleReady = true;
            }
            if (titleBlockArray.get(5).y < ScreenSize.HEIGHT.getSize() - lower - 2) {
                titleBlockArray.get(5).y = ScreenSize.HEIGHT.getSize() - lower;
            }
        }
        //-------------------------
        if (dropped6) {
            titleBlockArray.get(6).y = MathUtils.lerp(titleBlockArray.get(6).y, ScreenSize.HEIGHT.getSize() - upper, progress);
            if (titleBlockArray.get(6).y < ScreenSize.HEIGHT.getSize() - upper / 2) {
                dropped7 = true;
            }
            if (titleBlockArray.get(6).y < ScreenSize.HEIGHT.getSize() - upper - 2) {
                titleBlockArray.get(6).y = ScreenSize.HEIGHT.getSize() - upper;
            }
        }
        //-------------------------
        if (dropped7) {
            titleBlockArray.get(7).y = MathUtils.lerp(titleBlockArray.get(7).y, ScreenSize.HEIGHT.getSize() - upper, progress);
            if (titleBlockArray.get(7).y < ScreenSize.HEIGHT.getSize() - upper / 2) {
                dropped8 = true;
            }
            if (titleBlockArray.get(7).y < ScreenSize.HEIGHT.getSize() - upper - 2) {
                titleBlockArray.get(7).y = ScreenSize.HEIGHT.getSize() - upper;
            }
        }
        //-------------------------
        if (dropped8) {
            titleBlockArray.get(8).y = MathUtils.lerp(titleBlockArray.get(8).y, ScreenSize.HEIGHT.getSize() - upper, progress);
            if (titleBlockArray.get(8).y < ScreenSize.HEIGHT.getSize() - upper / 2) {
                dropped9 = true;
            }
            if (titleBlockArray.get(8).y < ScreenSize.HEIGHT.getSize() - upper - 2) {
                titleBlockArray.get(8).y = ScreenSize.HEIGHT.getSize() - upper;
            }
        }
        //-------------------------
        if (dropped9) {
            titleBlockArray.get(9).y = MathUtils.lerp(titleBlockArray.get(9).y, ScreenSize.HEIGHT.getSize() - upper, progress);
            if (titleBlockArray.get(9).y < ScreenSize.HEIGHT.getSize() - upper / 2) {
                dropped10 = true;
            }
            if (titleBlockArray.get(9).y < ScreenSize.HEIGHT.getSize() - upper - 2) {
                titleBlockArray.get(9).y = ScreenSize.HEIGHT.getSize() - upper;
            }
        }
        //-------------------------
        if (dropped10) {
            titleBlockArray.get(10).y = MathUtils.lerp(titleBlockArray.get(10).y, ScreenSize.HEIGHT.getSize() - upper, progress);
            if (titleBlockArray.get(10).y < ScreenSize.HEIGHT.getSize() - upper / 2) {
                dropped11 = true;
            }
            if (titleBlockArray.get(10).y < ScreenSize.HEIGHT.getSize() - upper - 2) {
                titleBlockArray.get(10).y = ScreenSize.HEIGHT.getSize() - upper;
            }
        }
        //-------------------------
        if (dropped11) {
            titleBlockArray.get(11).y = MathUtils.lerp(titleBlockArray.get(11).y, ScreenSize.HEIGHT.getSize() - upper, progress);
            if (titleBlockArray.get(11).y < ScreenSize.HEIGHT.getSize() - upper - 2) {
                titleBlockArray.get(11).y = ScreenSize.HEIGHT.getSize() - upper;
            }
        }

    }

    public boolean isTitleReady() {
        return titleReady;
    }

}
