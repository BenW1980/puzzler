package game.peanutpanda.blocktoggler.gameobjects;

public enum Direction {

    LEFT, RIGHT, UP, DOWN
}
