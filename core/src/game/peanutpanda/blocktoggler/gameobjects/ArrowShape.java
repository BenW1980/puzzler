package game.peanutpanda.blocktoggler.gameobjects;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

public abstract class ArrowShape extends Shape {

    private boolean pointerOneActive = false;
    private boolean pointerOneOppositeActive = false;
    private boolean pointerTwoActive = false;
    private boolean pointerTwoOppositeActive = false;
    private float angle;
    private Rectangle pointerOne;
    private Rectangle pointerOneOpposite;
    private Rectangle pointerTwo;
    private Rectangle pointerTwoOpposite;

    public abstract void createPointers();

    public void resetPointers() {
        setPointerOne(new Rectangle());
        setPointerOneOpposite(new Rectangle());
        setPointerTwo(new Rectangle());
        setPointerTwoOpposite(new Rectangle());
    }

    public void turn() {
        resetPointers();
        setPointerOneActive(false);
        setPointerOneOppositeActive(false);
        setPointerTwoActive(false);
        setPointerTwoOppositeActive(false);
        setAngle(MathUtils.lerpAngleDeg(getAngle(), getAngle() + 15, 0.6f));
    }

    public void createHorizontalPointers() {
        setPointerOne(new Rectangle(getRectangle().x - SIZE / 2, getRectangle().y, 10, 10));
        setPointerOneOpposite(new Rectangle(getRectangle().x + getRectangle().width + SIZE / 2, getRectangle().y, 10, 10));
    }


    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public Rectangle getPointerOne() {
        return pointerOne;
    }

    public void setPointerOne(Rectangle pointerOne) {
        this.pointerOne = pointerOne;
    }

    public Rectangle getPointerOneOpposite() {
        return pointerOneOpposite;
    }

    public void setPointerOneOpposite(Rectangle pointerOneOpposite) {
        this.pointerOneOpposite = pointerOneOpposite;
    }

    public boolean isPointerOneActive() {
        return pointerOneActive;
    }

    public void setPointerOneActive(boolean pointerOneActive) {
        this.pointerOneActive = pointerOneActive;
    }

    public boolean isPointerOneOppositeActive() {
        return pointerOneOppositeActive;
    }

    public void setPointerOneOppositeActive(boolean pointerOneOppositeActive) {
        this.pointerOneOppositeActive = pointerOneOppositeActive;
    }

    public Rectangle getPointerTwo() {
        return pointerTwo;
    }

    public void setPointerTwo(Rectangle pointerTwo) {
        this.pointerTwo = pointerTwo;
    }

    public Rectangle getPointerTwoOpposite() {
        return pointerTwoOpposite;
    }

    public void setPointerTwoOpposite(Rectangle pointerTwoOpposite) {
        this.pointerTwoOpposite = pointerTwoOpposite;
    }

    public boolean isPointerTwoActive() {
        return pointerTwoActive;
    }

    public void setPointerTwoActive(boolean pointerTwoActive) {
        this.pointerTwoActive = pointerTwoActive;
    }

    public boolean isPointerTwoOppositeActive() {
        return pointerTwoOppositeActive;
    }

    public void setPointerTwoOppositeActive(boolean pointerTwoOppositeActive) {
        this.pointerTwoOppositeActive = pointerTwoOppositeActive;
    }
}
