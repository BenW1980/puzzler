package game.peanutpanda.blocktoggler.gameobjects;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

import game.peanutpanda.blocktoggler.gamedata.GameState;

public class Shooter extends Shape {

    private boolean shooting;
    private Direction direction;
    private Projectile projectile;

    public Shooter(float tileX, float tileY, Direction direction) {
        this.direction = direction;
        getRectangle().set(tileX, tileY, SIZE, SIZE);
        projectile = new Projectile(getRectangle().x, getRectangle().y, direction);
    }

    public void shoot(Player player, Array<Tile> tiles, Array<Deflector> deflectors, GameState gameState) {

        projectile.move(projectile.getDirection());
        shooting = true;

        for (Deflector deflector : deflectors) {
            projectile.resetHits(deflector);
            if (projectile.getCenterRect().overlaps(deflector.getCenterRect())) {
                for (Tile tile : tiles) {
                    tile.setHit(false);
                }
            }
        }

        if (projectile.isOutOfBounds()) {
            shooting = false;
            for (Tile tile : tiles) {
                tile.setHit(false);
            }

            if (gameState == GameState.RUNNING) {
                player.setRectangle(new Rectangle());
                projectile = new Projectile(getRectangle().x, getRectangle().y, direction);
            }
        }
    }

    public Projectile getProjectile() {
        return projectile;
    }

    public boolean isShooting() {
        return shooting;
    }

    public void setShooting(boolean shooting) {
        this.shooting = shooting;
    }

    public Direction getDirection() {
        return direction;
    }
}
