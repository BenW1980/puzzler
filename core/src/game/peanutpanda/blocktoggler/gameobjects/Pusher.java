package game.peanutpanda.blocktoggler.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

public class Pusher extends Shape {

    private boolean pushing;
    private Direction direction;
    private Rectangle pushRect;
    private Rectangle backgroundRect;
    private Sprite background;

    public Pusher(float tileX, float tileY, Direction direction) {
        this.direction = direction;
        this.pushRect = new Rectangle();
        getRectangle().set(tileX, tileY, SIZE, SIZE);
        backgroundRect = new Rectangle();
        backgroundRect.set(getRectangle());
    }

    public void move(Direction direction) {

        int speed = 300;
        int maxMovement = 40;

        float accumulator = 0;
        float frameTime = Math.min(Gdx.graphics.getDeltaTime(), 1 / 60f);
        accumulator += frameTime;
        while (accumulator >= 0.1) {
            accumulator -= 0.1;
        }

        switch (direction) {
            case LEFT:
                getRectangle().x -= speed * accumulator;
                if (getRectangle().x < backgroundRect.x - maxMovement) {
                    getRectangle().setX(backgroundRect.x);
                    pushing = false;
                }
                break;

            case RIGHT:
                getRectangle().x += speed * accumulator;
                if (getRectangle().x > backgroundRect.x + maxMovement) {
                    getRectangle().setX(backgroundRect.x);
                    pushing = false;
                }
                break;

            case DOWN:
                getRectangle().y -= speed * accumulator;
                if (getRectangle().y < backgroundRect.y - maxMovement) {
                    getRectangle().setY(backgroundRect.y);
                    pushing = false;
                }
                break;

            case UP:
                getRectangle().y += speed * accumulator;
                if (getRectangle().y > backgroundRect.y + maxMovement) {
                    getRectangle().setY(backgroundRect.y);
                    pushing = false;
                }
                break;
        }
    }

    public void createRectangle() {

        switch (direction) {

            case LEFT:
                pushRect = new Rectangle(getRectangle().x - 25, getRectangle().y + 25, 10, 10);
                break;
            case RIGHT:
                pushRect = new Rectangle(getRectangle().x + SIZE + 25, getRectangle().y + 25, 10, 10);
                break;
            case DOWN:
                pushRect = new Rectangle(getRectangle().x + 25, getRectangle().y - 25, 10, 10);
                break;
            case UP:
                pushRect = new Rectangle(getRectangle().x + 25, getRectangle().y + SIZE + 25, 10, 10);
                break;
        }

    }

    public boolean isPushing() {
        return pushing;
    }

    public void setPushing(boolean pushing) {
        this.pushing = pushing;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Rectangle getPushRect() {
        return pushRect;
    }

    public void setPushRect(Rectangle pushRect) {
        this.pushRect = pushRect;
    }

    public Sprite getBackground() {
        return background;
    }

    public void setBackground(Sprite background) {
        this.background = background;
    }

    public Rectangle getBackgroundRect() {
        return backgroundRect;
    }

    public void setBackgroundRect(Rectangle backgroundRect) {
        this.backgroundRect = backgroundRect;
    }
}
