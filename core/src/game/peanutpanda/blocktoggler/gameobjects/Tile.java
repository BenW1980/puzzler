package game.peanutpanda.blocktoggler.gameobjects;

import com.badlogic.gdx.math.MathUtils;

import game.peanutpanda.blocktoggler.gamedata.ScreenSize;

public class Tile extends Shape {

    private boolean done = false;
    private boolean pressedDown = false;
    private boolean hit = false;
    private boolean moving = false;

    private float dirLeftStop;
    private float dirRightStop;
    private float dirDownStop;
    private float dirUpStop;

    private float dropSpot;

    public Tile(float tileX, float dropSpot) {
        this(tileX, dropSpot, false);
    }

    public Tile(float tileX, float dropSpot, boolean touched) {
        this.dropSpot = dropSpot;
        getRectangle().set(tileX, ScreenSize.HEIGHT.getSize(), SIZE, SIZE);
        setTouched(touched);
    }

    public void move(Direction direction) {

        float step = 0.2f;

        switch (direction) {
            case LEFT:
                getRectangle().x = MathUtils.lerp(getRectangle().x, dirLeftStop, step);
                if (getRectangle().x < dirLeftStop + 1) {
                    getRectangle().x = dirLeftStop;
                    setMoving(false);
                }
                break;

            case RIGHT:
                getRectangle().x = MathUtils.lerp(getRectangle().x, dirRightStop, step);
                if (getRectangle().x > dirRightStop - 1) {
                    getRectangle().x = dirRightStop;
                    setMoving(false);
                }
                break;

            case DOWN:
                getRectangle().y = MathUtils.lerp(getRectangle().y, dirDownStop, step);
                if (getRectangle().y < dirDownStop + 1) {
                    getRectangle().y = dirDownStop;
                    setMoving(false);
                }
                break;

            case UP:
                getRectangle().y = MathUtils.lerp(getRectangle().y, dirUpStop, step);
                if (getRectangle().y > dirUpStop - 1) {
                    getRectangle().y = dirUpStop;
                    setMoving(false);
                }
                break;
        }
    }

    public boolean isMoving() {
        return moving;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean isPressedDown() {
        return pressedDown;
    }

    public void setPressedDown(boolean pressedDown) {
        this.pressedDown = pressedDown;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    public void setDirLeftStop(float dirLeftStop) {
        this.dirLeftStop = dirLeftStop;
    }

    public void setDirRightStop(float dirRightStop) {
        this.dirRightStop = dirRightStop;
    }

    public void setDirDownStop(float dirDownStop) {
        this.dirDownStop = dirDownStop;
    }

    public void setDirUpStop(float dirUpStop) {
        this.dirUpStop = dirUpStop;
    }

    public float getDropSpot() {
        return dropSpot;
    }

    public void setDropSpot(float dropSpot) {
        this.dropSpot = dropSpot;
    }
}
