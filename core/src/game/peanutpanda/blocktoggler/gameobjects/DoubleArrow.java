package game.peanutpanda.blocktoggler.gameobjects;

import com.badlogic.gdx.math.Rectangle;

public class DoubleArrow extends ArrowShape {

    public DoubleArrow(float tileX, float tileY, int angle) {
        setAngle(angle);
        getRectangle().set(tileX, tileY, SIZE, SIZE);
        resetPointers();
    }

    @Override
    public void createPointers() {
        if (getAngle() % 90 == 0) {
            createVerticalPointers();
            createHorizontalPointers();
            setTouched(false);
        }
    }

    private void createVerticalPointers() {
        setPointerTwo(new Rectangle(getRectangle().x, getRectangle().y - 35, 10, 10));
        setPointerTwoOpposite(new Rectangle(getRectangle().x, getRectangle().y + getRectangle().height + 35, 10, 10));
    }

}