package game.peanutpanda.blocktoggler.gameobjects;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import game.peanutpanda.blocktoggler.levels.Level;

public class Player {

    private Rectangle rectangle;

    private Rectangle up;
    private Rectangle down;
    private Rectangle left;
    private Rectangle right;

    private int moves;
    private int stars;

    public Player() {
        rectangle = new Rectangle(0, 0, 1, 1);
    }

    public void addMove() {
        moves++;
    }

    public boolean hasThreeStars(Level level) {
        return moves <= level.getMinimumMoves();
    }

    public boolean hasTwoStars(Level level) {
        return moves > level.getMinimumMoves() && moves < level.getMinimumMoves() * 2;
    }

    public String movesAsString() {

        String number;

        if (moves < 10) {
            number = 0 + "" + moves;
        } else {
            number = "" + moves;
        }

        return number;
    }

    public boolean taps(Shape shape) {
        return this.rectangle.overlaps(shape.getRectangle());
    }

    public boolean taps(Rectangle rect) {
        return this.rectangle.overlaps(rect);
    }

    public void move(Vector3 worldCoordinates) {

        rectangle.y = worldCoordinates.y;
        rectangle.x = worldCoordinates.x;

    }

    public void createRectangles(Array<Rectangle> rectangles, Tile tile) {

        up = new Rectangle(tile.getRectangle().x + Shape.SIZE / 2, tile.getRectangle().y + tile.getRectangle().height, 1, Shape.SIZE / 2);
        down = new Rectangle(tile.getRectangle().x + tile.getRectangle().width, tile.getRectangle().y + Shape.SIZE / 2, Shape.SIZE / 2, 1);
        left = new Rectangle(tile.getRectangle().x - Shape.SIZE / 2, tile.getRectangle().y + Shape.SIZE / 2, tile.getRectangle().getWidth(), 1);
        right = new Rectangle(tile.getRectangle().x + tile.getRectangle().width - Shape.SIZE / 2, tile.getRectangle().y - Shape.SIZE / 2, 1, tile.getRectangle().height);

        rectangles.add(up);
        rectangles.add(down);
        rectangles.add(left);
        rectangles.add(right);
    }

    public void removeRectangles(Array<Rectangle> rectangles) {

        rectangles.removeValue(up, true);
        rectangles.removeValue(down, true);
        rectangles.removeValue(left, true);
        rectangles.removeValue(right, true);
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public int getMoves() {
        return moves;
    }

    public void setMoves(int moves) {
        this.moves = moves;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }
}
