package game.peanutpanda.blocktoggler.gameobjects;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

public class Deflector extends Shape {

    private float angle;
    private Rectangle centerRect;
    private boolean hit;
    private boolean hitByProjectile;

    public Deflector(float tileX, float tileY, int angle) {
        this.angle = angle;
        centerRect = new Rectangle(tileX + SIZE / 2 - 10, tileY + SIZE / 2 - 10, 20, 20);
        getRectangle().set(tileX, tileY, SIZE, SIZE);
    }

    public void turn() {
        angle = MathUtils.lerpAngleDeg(angle, angle + 15, 0.6f);
        if (angle == 45 || angle == 135 || angle == 225 || angle == 315) {
            setTouched(false);
        }
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public Rectangle getCenterRect() {
        return centerRect;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    public boolean isHitByProjectile() {
        return hitByProjectile;
    }

    public void setHitByProjectile(boolean hitByProjectile) {
        this.hitByProjectile = hitByProjectile;
    }
}