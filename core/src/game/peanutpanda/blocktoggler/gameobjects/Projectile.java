package game.peanutpanda.blocktoggler.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;

import game.peanutpanda.blocktoggler.gamedata.ScreenSize;

public class Projectile extends Shape {

    private Direction direction;
    private Rectangle centerRect;

    public Projectile(float tileX, float tileY, Direction direction) {
        this.direction = direction;

        switch (direction) {

            case LEFT:
                getRectangle().set(tileX, tileY, SIZE, SIZE);
                centerRect = new Rectangle(tileX + SIZE / 2 + 10, tileY + SIZE / 2 - 2, 5, 5);
                break;

            case RIGHT:
                getRectangle().set(tileX, tileY, SIZE, SIZE);
                centerRect = new Rectangle(tileX + SIZE / 2 - 13, tileY + SIZE / 2 - 2, 5, 5);
                break;

            case UP:
                getRectangle().set(tileX, tileY, SIZE, SIZE);
                centerRect = new Rectangle(tileX + SIZE / 2 - 2, tileY + SIZE / 2 - 13, 5, 5);
                break;

            case DOWN:
                getRectangle().set(tileX, tileY, SIZE, SIZE);
                centerRect = new Rectangle(tileX + SIZE / 2 - 2, tileY + SIZE / 2 + 8, 5, 5);
                break;
        }
    }

    public boolean isOutOfBounds() {

        return getRectangle().x < -20 || getRectangle().x > ScreenSize.WIDTH.getSize() + 20
                || getRectangle().y > ScreenSize.HEIGHT.getSize() || getRectangle().y < 0;
    }

    public void resetHits(Deflector deflector) {

        if (!getCenterRect().overlaps(deflector.getRectangle())) {
            deflector.setHit(false);
            deflector.setHitByProjectile(false);
        }
    }

    public void switchDirection(Deflector deflector) {

        if (!deflector.isHit()) {

            deflector.setHit(true);

            if (deflector.getAngle() == 45 || deflector.getAngle() == 225) {
                if (direction == Direction.LEFT) {
                    direction = Direction.UP;

                } else if (direction == Direction.RIGHT) {
                    direction = Direction.DOWN;

                } else if (direction == Direction.DOWN) {
                    direction = Direction.RIGHT;

                } else if (direction == Direction.UP) {
                    direction = Direction.LEFT;
                }

            } else if (deflector.getAngle() == 135 || deflector.getAngle() == 315) {
                if (direction == Direction.LEFT) {
                    direction = Direction.DOWN;

                } else if (direction == Direction.RIGHT) {
                    direction = Direction.UP;

                } else if (direction == Direction.DOWN) {
                    direction = Direction.LEFT;

                } else if (direction == Direction.UP) {
                    direction = Direction.RIGHT;
                }

            }
        }

    }

    public void move(Direction direction) {

        int speed = 900;

        float accumulator = 0;
        float frameTime = Math.min(Gdx.graphics.getDeltaTime(), 1 / 60f);
        accumulator += frameTime;
        while (accumulator >= 0.1) {
            accumulator -= 0.1;
        }

        switch (direction) {

            case LEFT:
                getRectangle().x -= speed * accumulator;
                centerRect.x -= speed * accumulator;
                break;

            case RIGHT:
                getRectangle().x += speed * accumulator;
                centerRect.x += speed * accumulator;
                break;

            case UP:
                getRectangle().y += speed * accumulator;
                centerRect.y += speed * accumulator;
                break;

            case DOWN:
                getRectangle().y -= speed * accumulator;
                centerRect.y -= speed * accumulator;
                break;
        }
    }

    public Direction getDirection() {
        return direction;
    }

    public Rectangle getCenterRect() {
        return centerRect;
    }
}