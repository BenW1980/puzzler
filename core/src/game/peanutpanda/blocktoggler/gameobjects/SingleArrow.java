package game.peanutpanda.blocktoggler.gameobjects;

import com.badlogic.gdx.math.Rectangle;

public class SingleArrow extends ArrowShape {

    public SingleArrow(float tileX, float tileY, int angle) {
        setAngle(angle);
        getRectangle().set(tileX, tileY, Shape.SIZE, Shape.SIZE);
        resetPointers();
    }

    @Override
    public void createPointers() {
        if (getAngle() == 90 || getAngle() == 270) {
            createHorizontalPointers();
            setTouched(false);

        } else if (getAngle() == 180 || getAngle() == 0) {
            createVerticalPointers();
            setTouched(false);
        }
    }

    private void createVerticalPointers() {
        setPointerOne(new Rectangle(getRectangle().x, getRectangle().y - Shape.SIZE / 2, 10, 10));
        setPointerOneOpposite(new Rectangle(getRectangle().x, getRectangle().y + getRectangle().height + Shape.SIZE / 2, 10, 10));
    }


}