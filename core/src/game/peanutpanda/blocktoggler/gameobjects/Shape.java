package game.peanutpanda.blocktoggler.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

public abstract class Shape {

    public final static int SIZE = 60;
    public final static int SINGLE_TILE_SIZE = Shape.SIZE + Shape.SIZE / 10 + 5;
    private boolean touched = false;
    private Rectangle rectangle;
    private Sprite sprite;
    private int width;
    private int height;
    private boolean tappedByPlayer;

    public Shape() {
        rectangle = new Rectangle();
    }

    public boolean isTouched() {
        return touched;
    }

    public void setTouched(boolean touched) {
        this.touched = touched;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isTappedByPlayer() {
        return tappedByPlayer;
    }

    public void setTappedByPlayer(boolean tappedByPlayer) {
        this.tappedByPlayer = tappedByPlayer;
    }
}
