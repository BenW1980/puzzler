package game.peanutpanda.blocktoggler.gamedata;


public class AudioHandler {

    private boolean soundOn = true;

    private AssetLoader assetLoader;

    public AudioHandler(AssetLoader assetLoader) {
        this.assetLoader = assetLoader;
    }

    public void toggleSound() {

        soundOn = !soundOn;
    }

    public void playTickSound() {

        if (soundOn) {
            assetLoader.tick().play(0.2f);
        }
    }

    public void playPingSound() {

        if (soundOn) {
            assetLoader.ping().play(0.1f);
        }
    }

    public void playPlopSound() {

        if (soundOn) {
            assetLoader.plop().play(0.5f);
        }
    }

    public void playMusic() {

        assetLoader.carefree().setLooping(true);

        if (soundOn) {
            assetLoader.carefree().setVolume(0.5f);
            assetLoader.carefree().play();
        } else {
            assetLoader.carefree().pause();
        }
    }

    public void resetMusic() {
        if (assetLoader.carefree().getPosition() > 310) {
            assetLoader.carefree().setPosition(0);
        }
        playMusic();
    }

    public boolean isSoundOn() {
        return soundOn;
    }

    public void setSoundOn(boolean soundOn) {
        this.soundOn = soundOn;
    }
}