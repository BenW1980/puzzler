package game.peanutpanda.blocktoggler.gamedata;

import com.badlogic.gdx.Game;

import game.peanutpanda.blocktoggler.screens.LoadingScreen;

public class BlockToggle extends Game {

    @Override
    public void create() {

        this.setScreen(new LoadingScreen());
    }

    @Override
    public void render() {
        super.render();
    }

}
