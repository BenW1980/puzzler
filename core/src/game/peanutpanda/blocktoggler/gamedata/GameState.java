package game.peanutpanda.blocktoggler.gamedata;

public enum GameState {

    RUNNING, LEVEL_COMPLETE, PAUSED

}
