package game.peanutpanda.blocktoggler.gamedata;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader.FreeTypeFontLoaderParameter;


public class AssetLoader {

    private final AssetManager assetManager = new AssetManager();

    public void startLoading() {

        assetManager.load("sounds/plop.ogg", Sound.class);
        assetManager.load("sounds/ping2.ogg", Sound.class);
        assetManager.load("sounds/tick.ogg", Sound.class);
        assetManager.load("sounds/dailyBeetle.ogg", Music.class);

        assetManager.load("images/tiles/tileBlue2.png", Texture.class);
        assetManager.load("images/tiles/tileRed2.png", Texture.class);
        assetManager.load("images/tiles/tileYellow2.png", Texture.class);

        assetManager.load("images/tiles/doubleArrow.png", Texture.class);
        assetManager.load("images/tiles/singleArrow.png", Texture.class);
        assetManager.load("images/tiles/doubleArrowTouched.png", Texture.class);
        assetManager.load("images/tiles/singleArrowTouched.png", Texture.class);

        assetManager.load("images/tiles/deflector.png", Texture.class);
        assetManager.load("images/tiles/deflectorHit.png", Texture.class);

        assetManager.load("images/tiles/shooterUp.png", Texture.class);
        assetManager.load("images/tiles/shooterDown.png", Texture.class);
        assetManager.load("images/tiles/shooterLeft.png", Texture.class);
        assetManager.load("images/tiles/shooterRight.png", Texture.class);
        assetManager.load("images/tiles/shooterUpWhite.png", Texture.class);
        assetManager.load("images/tiles/shooterDownWhite.png", Texture.class);
        assetManager.load("images/tiles/shooterLeftWhite.png", Texture.class);
        assetManager.load("images/tiles/shooterRightWhite.png", Texture.class);

        assetManager.load("images/tiles/projectileUp.png", Texture.class);
        assetManager.load("images/tiles/projectileDown.png", Texture.class);
        assetManager.load("images/tiles/projectileLeft.png", Texture.class);
        assetManager.load("images/tiles/projectileRight.png", Texture.class);

        assetManager.load("images/tiles/pusherLeft.png", Texture.class);
        assetManager.load("images/tiles/pusherRight.png", Texture.class);
        assetManager.load("images/tiles/pusherUp.png", Texture.class);
        assetManager.load("images/tiles/pusherDown.png", Texture.class);

        assetManager.load("images/tiles/pusherHubLeft.png", Texture.class);
        assetManager.load("images/tiles/pusherHubRight.png", Texture.class);
        assetManager.load("images/tiles/pusherHubDown.png", Texture.class);
        assetManager.load("images/tiles/pusherHubUp.png", Texture.class);

        assetManager.load("images/menu/starGold.png", Texture.class);

        assetManager.load("images/menu/refresh.png", Texture.class);
        assetManager.load("images/menu/refreshActive.png", Texture.class);
        assetManager.load("images/menu/menu.png", Texture.class);
        assetManager.load("images/menu/menuActive.png", Texture.class);
        assetManager.load("images/menu/volumeOff.png", Texture.class);
        assetManager.load("images/menu/volumeOn.png", Texture.class);

        assetManager.load("images/menu/arrowRight.png", Texture.class);
        assetManager.load("images/menu/arrowRightActive.png", Texture.class);
        assetManager.load("images/menu/levelCompleteEmpty.png", Texture.class);
        assetManager.load("images/menu/refreshActiveLevelComplete.png", Texture.class);
        assetManager.load("images/menu/menuActiveLevelComplete.png", Texture.class);

        assetManager.load("images/levels/01.png", Texture.class);
        assetManager.load("images/levels/02.png", Texture.class);
        assetManager.load("images/levels/03.png", Texture.class);
        assetManager.load("images/levels/04.png", Texture.class);
        assetManager.load("images/levels/05.png", Texture.class);
        assetManager.load("images/levels/06.png", Texture.class);
        assetManager.load("images/levels/07.png", Texture.class);
        assetManager.load("images/levels/08.png", Texture.class);
        assetManager.load("images/levels/09.png", Texture.class);
        assetManager.load("images/levels/10.png", Texture.class);
        assetManager.load("images/levels/11.png", Texture.class);
        assetManager.load("images/levels/12.png", Texture.class);
        assetManager.load("images/levels/13.png", Texture.class);
        assetManager.load("images/levels/14.png", Texture.class);
        assetManager.load("images/levels/15.png", Texture.class);
        assetManager.load("images/levels/16.png", Texture.class);
        assetManager.load("images/levels/17.png", Texture.class);
        assetManager.load("images/levels/18.png", Texture.class);
        assetManager.load("images/levels/19.png", Texture.class);
        assetManager.load("images/levels/20.png", Texture.class);

        assetManager.load("images/levels/01Active.png", Texture.class);
        assetManager.load("images/levels/02Active.png", Texture.class);
        assetManager.load("images/levels/03Active.png", Texture.class);
        assetManager.load("images/levels/04Active.png", Texture.class);
        assetManager.load("images/levels/05Active.png", Texture.class);
        assetManager.load("images/levels/06Active.png", Texture.class);
        assetManager.load("images/levels/07Active.png", Texture.class);
        assetManager.load("images/levels/08Active.png", Texture.class);
        assetManager.load("images/levels/09Active.png", Texture.class);
        assetManager.load("images/levels/10Active.png", Texture.class);
        assetManager.load("images/levels/11Active.png", Texture.class);
        assetManager.load("images/levels/12Active.png", Texture.class);
        assetManager.load("images/levels/13Active.png", Texture.class);
        assetManager.load("images/levels/14Active.png", Texture.class);
        assetManager.load("images/levels/15Active.png", Texture.class);
        assetManager.load("images/levels/16Active.png", Texture.class);
        assetManager.load("images/levels/17Active.png", Texture.class);
        assetManager.load("images/levels/18Active.png", Texture.class);
        assetManager.load("images/levels/19Active.png", Texture.class);
        assetManager.load("images/levels/20Active.png", Texture.class);

        assetManager.load("images/levels/home.png", Texture.class);
        assetManager.load("images/levels/homeActive.png", Texture.class);
        assetManager.load("images/levels/leftActive.png", Texture.class);
        assetManager.load("images/levels/rightActive.png", Texture.class);
        assetManager.load("images/levels/right.png", Texture.class);
        assetManager.load("images/levels/left.png", Texture.class);

        assetManager.load("images/levels/starsCompletedHeader.png", Texture.class);

        assetManager.load("images/levels/locked.png", Texture.class);
        assetManager.load("images/levels/noStars.png", Texture.class);
        assetManager.load("images/levels/oneStar.png", Texture.class);
        assetManager.load("images/levels/twoStars.png", Texture.class);
        assetManager.load("images/levels/threeStars.png", Texture.class);

        assetManager.load("images/levels/world_select1.png", Texture.class);
        assetManager.load("images/levels/world_select2.png", Texture.class);
        assetManager.load("images/levels/world_select3.png", Texture.class);

        assetManager.load("images/levels/world_TotalStarsComplete.png", Texture.class);
        assetManager.load("images/levels/world_TotalStarsIncomplete.png", Texture.class);

        assetManager.load("images/UI.png", Texture.class);
        assetManager.load("images/background.png", Texture.class);
        assetManager.load("images/backgroundWhite.png", Texture.class);
        assetManager.load("images/grijslaag.png", Texture.class);

        assetManager.load("images/oneStar.png", Texture.class);
        assetManager.load("images/twoStars.png", Texture.class);
        assetManager.load("images/threeStars.png", Texture.class);

        assetManager.load("images/1.png", Texture.class);
        assetManager.load("images/2.png", Texture.class);
        assetManager.load("images/3.png", Texture.class);
        assetManager.load("images/4.png", Texture.class);
        assetManager.load("images/5.png", Texture.class);
        assetManager.load("images/6.png", Texture.class);
        assetManager.load("images/7.png", Texture.class);
        assetManager.load("images/8.png", Texture.class);
        assetManager.load("images/9.png", Texture.class);
        assetManager.load("images/10.png", Texture.class);
        assetManager.load("images/11.png", Texture.class);
        assetManager.load("images/12.png", Texture.class);
        assetManager.load("images/13.png", Texture.class);

        assetManager.load("images/playTapped.png", Texture.class);
        assetManager.load("images/playUntapped.png", Texture.class);
        assetManager.load("images/twitter.png", Texture.class);
        assetManager.load("images/twitterOnclick.png", Texture.class);
        assetManager.load("images/credits.png", Texture.class);
        assetManager.load("images/creditsOnclick.png", Texture.class);
        assetManager.load("images/volumeMain.png", Texture.class);
        assetManager.load("images/volumeMainOnclick.png", Texture.class);

        assetManager.load("images/title/titleB3.png", Texture.class);
        assetManager.load("images/title/titleC10.png", Texture.class);
        assetManager.load("images/title/titleE9.png", Texture.class);
        assetManager.load("images/title/titleG2.png", Texture.class);
        assetManager.load("images/title/titleG7.png", Texture.class);
        assetManager.load("images/title/titleK11.png", Texture.class);
        assetManager.load("images/title/titleL4.png", Texture.class);
        assetManager.load("images/title/titleL8.png", Texture.class);
        assetManager.load("images/title/titleO1.png", Texture.class);
        assetManager.load("images/title/titleO5.png", Texture.class);
        assetManager.load("images/title/titleS12.png", Texture.class);
        assetManager.load("images/title/titleT6.png", Texture.class);

        assetManager.load("images/tutorial/0_tutWhiteTouch1.png", Texture.class);
        assetManager.load("images/tutorial/1_tutWhiteTouch1.png", Texture.class);
        assetManager.load("images/tutorial/2_tutBlueTouch1.png", Texture.class);
        assetManager.load("images/tutorial/3_tutBlueTouch2.png", Texture.class);
        assetManager.load("images/tutorial/4_tutBlueTouch1.png", Texture.class);
        assetManager.load("images/tutorial/arrowLeft.png", Texture.class);
        assetManager.load("images/tutorial/arrowLeftActive.png", Texture.class);

        assetManager.load("images/credits/credit.png", Texture.class);


        loadFontHandling();


    }

    public void loadFontHandling() {

        FileHandleResolver resolver = new InternalFileHandleResolver();
        assetManager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        assetManager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));

        FreeTypeFontLoaderParameter size65 = new FreeTypeFontLoaderParameter();
        size65.fontFileName = "fonts/Aller.ttf";
        size65.fontParameters.size = 65;
        assetManager.load("size65.ttf", BitmapFont.class, size65);

    }

    public Sound plop() {
        return assetManager.get("sounds/plop.ogg", Sound.class);
    }

    public Sound ping() {
        return assetManager.get("sounds/ping2.ogg", Sound.class);
    }

    public Sound tick() {
        return assetManager.get("sounds/tick.ogg", Sound.class);
    }

    public Music carefree() {
        return assetManager.get("sounds/dailyBeetle.ogg", Music.class);
    }

    public Texture UI() {
        return assetManager.get("images/UI.png", Texture.class);
    }

    public Texture moves1() {
        return assetManager.get("images/1.png", Texture.class);
    }

    public Texture moves2() {
        return assetManager.get("images/2.png", Texture.class);
    }

    public Texture moves3() {
        return assetManager.get("images/3.png", Texture.class);
    }

    public Texture moves4() {
        return assetManager.get("images/4.png", Texture.class);
    }

    public Texture moves5() {
        return assetManager.get("images/5.png", Texture.class);
    }

    public Texture moves6() {
        return assetManager.get("images/6.png", Texture.class);
    }

    public Texture moves7() {
        return assetManager.get("images/7.png", Texture.class);
    }

    public Texture moves8() {
        return assetManager.get("images/8.png", Texture.class);
    }

    public Texture moves9() {
        return assetManager.get("images/9.png", Texture.class);
    }

    public Texture moves10() {
        return assetManager.get("images/10.png", Texture.class);
    }

    public Texture moves11() {
        return assetManager.get("images/11.png", Texture.class);
    }

    public Texture moves12() {
        return assetManager.get("images/12.png", Texture.class);
    }

    public Texture moves13() {
        return assetManager.get("images/13.png", Texture.class);
    }

    public Texture background() {
        return assetManager.get("images/background.png", Texture.class);
    }

    public Texture backgroundWhite() {
        return assetManager.get("images/backgroundWhite.png", Texture.class);
    }

    public Texture oneStarGame() {
        return assetManager.get("images/oneStar.png", Texture.class);
    }

    public Texture twoStarsGame() {
        return assetManager.get("images/twoStars.png", Texture.class);
    }

    public Texture threeStarsGame() {
        return assetManager.get("images/threeStars.png", Texture.class);
    }

    public Texture worldTotalStarsComplete() {
        return assetManager.get("images/levels/world_TotalStarsComplete.png", Texture.class);
    }

    public Texture worldTotalStarsIncomplete() {
        return assetManager.get("images/levels/world_TotalStarsIncomplete.png", Texture.class);
    }

    public Texture worldSelect1() {
        return assetManager.get("images/levels/world_select1.png", Texture.class);
    }

    public Texture worldSelect2() {
        return assetManager.get("images/levels/world_select2.png", Texture.class);
    }

    public Texture worldSelect3() {
        return assetManager.get("images/levels/world_select3.png", Texture.class);
    }

    public Texture arrowRight() {
        return assetManager.get("images/menu/arrowRight.png", Texture.class);
    }

    public Texture arrowRightActive() {
        return assetManager.get("images/menu/arrowRightActive.png", Texture.class);
    }

    public Texture levelCompleteEmpty() {
        return assetManager.get("images/menu/levelCompleteEmpty.png", Texture.class);
    }

    public Texture refresh() {
        return assetManager.get("images/menu/refresh.png", Texture.class);
    }

    public Texture refreshActive() {
        return assetManager.get("images/menu/refreshActive.png", Texture.class);
    }

    public Texture refreshActiveLevelComplete() {
        return assetManager.get("images/menu/refreshActiveLevelComplete.png", Texture.class);
    }

    public Texture menuActiveLevelComplete() {
        return assetManager.get("images/menu/menuActiveLevelComplete.png", Texture.class);
    }

    public Texture volumeOn() {
        return assetManager.get("images/menu/volumeOn.png", Texture.class);
    }

    public Texture volumeOff() {
        return assetManager.get("images/menu/volumeOff.png", Texture.class);
    }

    public Texture menu() {
        return assetManager.get("images/menu/menu.png", Texture.class);
    }

    public Texture menuActive() {
        return assetManager.get("images/menu/menuActive.png", Texture.class);
    }

    public Texture playTapped() {
        return assetManager.get("images/playTapped.png", Texture.class);
    }

    public Texture playUntapped() {
        return assetManager.get("images/playUntapped.png", Texture.class);
    }

    public Texture tileBlue2() {
        return assetManager.get("images/tiles/tileBlue2.png", Texture.class);
    }

    public Texture tileRed2() {
        return assetManager.get("images/tiles/tileRed2.png", Texture.class);
    }

    public Texture tileYellow2() {
        return assetManager.get("images/tiles/tileYellow2.png", Texture.class);
    }

    public Texture singleArrow() {
        return assetManager.get("images/tiles/singleArrow.png", Texture.class);
    }

    public Texture doubleArrow() {
        return assetManager.get("images/tiles/doubleArrow.png", Texture.class);
    }

    public Texture singleArrowTouched() {
        return assetManager.get("images/tiles/singleArrowTouched.png", Texture.class);
    }

    public Texture doubleArrowTouched() {
        return assetManager.get("images/tiles/doubleArrowTouched.png", Texture.class);
    }

    public Texture shooterUp() {
        return assetManager.get("images/tiles/shooterUp.png", Texture.class);
    }

    public Texture shooterDown() {
        return assetManager.get("images/tiles/shooterDown.png", Texture.class);
    }

    public Texture shooterLeft() {
        return assetManager.get("images/tiles/shooterLeft.png", Texture.class);
    }

    public Texture shooterRight() {
        return assetManager.get("images/tiles/shooterRight.png", Texture.class);
    }

    public Texture shooterUpWhite() {
        return assetManager.get("images/tiles/shooterUpWhite.png", Texture.class);
    }

    public Texture shooterDownWhite() {
        return assetManager.get("images/tiles/shooterDownWhite.png", Texture.class);
    }

    public Texture shooterLeftWhite() {
        return assetManager.get("images/tiles/shooterLeftWhite.png", Texture.class);
    }

    public Texture shooterRighWhite() {
        return assetManager.get("images/tiles/shooterRightWhite.png", Texture.class);
    }

    public Texture projectileUp() {
        return assetManager.get("images/tiles/projectileUp.png", Texture.class);
    }

    public Texture projectileDown() {
        return assetManager.get("images/tiles/projectileDown.png", Texture.class);
    }

    public Texture projectileLeft() {
        return assetManager.get("images/tiles/projectileLeft.png", Texture.class);
    }

    public Texture projectileRight() {
        return assetManager.get("images/tiles/projectileRight.png", Texture.class);
    }

    public Texture pusherLeft() {
        return assetManager.get("images/tiles/pusherLeft.png", Texture.class);
    }

    public Texture pusherRight() {
        return assetManager.get("images/tiles/pusherRight.png", Texture.class);
    }

    public Texture pusherDown() {
        return assetManager.get("images/tiles/pusherDown.png", Texture.class);
    }

    public Texture pusherUp() {
        return assetManager.get("images/tiles/pusherUp.png", Texture.class);
    }

    public Texture pusherHubLeft() {
        return assetManager.get("images/tiles/pusherHubLeft.png", Texture.class);
    }

    public Texture pusherHubRight() {
        return assetManager.get("images/tiles/pusherHubRight.png", Texture.class);
    }

    public Texture pusherHubDown() {
        return assetManager.get("images/tiles/pusherHubDown.png", Texture.class);
    }

    public Texture pusherHubUp() {
        return assetManager.get("images/tiles/pusherHubUp.png", Texture.class);
    }

    public Texture deflector() {
        return assetManager.get("images/tiles/deflector.png", Texture.class);
    }

    public Texture deflectorHit() {
        return assetManager.get("images/tiles/deflectorHit.png", Texture.class);
    }

    public Texture starGold() {
        return assetManager.get("images/menu/starGold.png", Texture.class);
    }

    public Texture l1Active() {
        return assetManager.get("images/levels/01Active.png", Texture.class);
    }

    public Texture l2Active() {
        return assetManager.get("images/levels/02Active.png", Texture.class);
    }

    public Texture l3Active() {
        return assetManager.get("images/levels/03Active.png", Texture.class);
    }

    public Texture l4Active() {
        return assetManager.get("images/levels/04Active.png", Texture.class);
    }

    public Texture l5Active() {
        return assetManager.get("images/levels/05Active.png", Texture.class);
    }

    public Texture l6Active() {
        return assetManager.get("images/levels/06Active.png", Texture.class);
    }

    public Texture l7Active() {
        return assetManager.get("images/levels/07Active.png", Texture.class);
    }

    public Texture l8Active() {
        return assetManager.get("images/levels/08Active.png", Texture.class);
    }

    public Texture l9Active() {
        return assetManager.get("images/levels/09Active.png", Texture.class);
    }

    public Texture l10Active() {
        return assetManager.get("images/levels/10Active.png", Texture.class);
    }

    public Texture l11Active() {
        return assetManager.get("images/levels/11Active.png", Texture.class);
    }

    public Texture l12Active() {
        return assetManager.get("images/levels/12Active.png", Texture.class);
    }

    public Texture l13Active() {
        return assetManager.get("images/levels/13Active.png", Texture.class);
    }

    public Texture l14Active() {
        return assetManager.get("images/levels/14Active.png", Texture.class);
    }

    public Texture l15Active() {
        return assetManager.get("images/levels/15Active.png", Texture.class);
    }

    public Texture l16Active() {
        return assetManager.get("images/levels/16Active.png", Texture.class);
    }

    public Texture l17Active() {
        return assetManager.get("images/levels/17Active.png", Texture.class);
    }

    public Texture l18Active() {
        return assetManager.get("images/levels/18Active.png", Texture.class);
    }

    public Texture l19Active() {
        return assetManager.get("images/levels/19Active.png", Texture.class);
    }

    public Texture l20Active() {
        return assetManager.get("images/levels/20Active.png", Texture.class);
    }

    public Texture l1() {
        return assetManager.get("images/levels/01.png", Texture.class);
    }

    public Texture l2() {
        return assetManager.get("images/levels/02.png", Texture.class);
    }

    public Texture l3() {
        return assetManager.get("images/levels/03.png", Texture.class);
    }

    public Texture l4() {
        return assetManager.get("images/levels/04.png", Texture.class);
    }

    public Texture l5() {
        return assetManager.get("images/levels/05.png", Texture.class);
    }

    public Texture l6() {
        return assetManager.get("images/levels/06.png", Texture.class);
    }

    public Texture l7() {
        return assetManager.get("images/levels/07.png", Texture.class);
    }

    public Texture l8() {
        return assetManager.get("images/levels/08.png", Texture.class);
    }

    public Texture l9() {
        return assetManager.get("images/levels/09.png", Texture.class);
    }

    public Texture l10() {
        return assetManager.get("images/levels/10.png", Texture.class);
    }

    public Texture l11() {
        return assetManager.get("images/levels/11.png", Texture.class);
    }

    public Texture l12() {
        return assetManager.get("images/levels/12.png", Texture.class);
    }

    public Texture l13() {
        return assetManager.get("images/levels/13.png", Texture.class);
    }

    public Texture l14() {
        return assetManager.get("images/levels/14.png", Texture.class);
    }

    public Texture l15() {
        return assetManager.get("images/levels/15.png", Texture.class);
    }

    public Texture l16() {
        return assetManager.get("images/levels/16.png", Texture.class);
    }

    public Texture l17() {
        return assetManager.get("images/levels/17.png", Texture.class);
    }

    public Texture l18() {
        return assetManager.get("images/levels/18.png", Texture.class);
    }

    public Texture l19() {
        return assetManager.get("images/levels/19.png", Texture.class);
    }

    public Texture l20() {
        return assetManager.get("images/levels/20.png", Texture.class);
    }

    public Texture starsCompletedHeader() {
        return assetManager.get("images/levels/starsCompletedHeader.png", Texture.class);
    }

    public Texture locked() {
        return assetManager.get("images/levels/locked.png", Texture.class);
    }

    public Texture noStars() {
        return assetManager.get("images/levels/noStars.png", Texture.class);
    }

    public Texture oneStar() {
        return assetManager.get("images/levels/oneStar.png", Texture.class);
    }

    public Texture twoStars() {
        return assetManager.get("images/levels/twoStars.png", Texture.class);
    }

    public Texture threeStars() {
        return assetManager.get("images/levels/threeStars.png", Texture.class);
    }

    public Texture left() {
        return assetManager.get("images/levels/left.png", Texture.class);
    }

    public Texture right() {
        return assetManager.get("images/levels/right.png", Texture.class);
    }

    public Texture leftActive() {
        return assetManager.get("images/levels/leftActive.png", Texture.class);
    }

    public Texture rightActive() {
        return assetManager.get("images/levels/rightActive.png", Texture.class);
    }

    public Texture home() {
        return assetManager.get("images/levels/home.png", Texture.class);
    }

    public Texture homeActive() {
        return assetManager.get("images/levels/homeActive.png", Texture.class);
    }

    public Texture twButton() {
        return assetManager.get("images/twitter.png", Texture.class);
    }

    public Texture twButtonUP() {
        return assetManager.get("images/twitterOnclick.png", Texture.class);
    }

    public Texture credits() {
        return assetManager.get("images/credits.png", Texture.class);
    }

    public Texture creditsOnClick() {
        return assetManager.get("images/creditsOnclick.png", Texture.class);
    }

    public Texture volumeMain() {
        return assetManager.get("images/volumeMain.png", Texture.class);
    }

    public Texture volumeMainOnClick() {
        return assetManager.get("images/volumeMainOnclick.png", Texture.class);
    }

    public Texture B3() {
        return assetManager.get("images/title/titleB3.png", Texture.class);
    }

    public Texture C10() {
        return assetManager.get("images/title/titleC10.png", Texture.class);
    }

    public Texture E9() {
        return assetManager.get("images/title/titleE9.png", Texture.class);
    }

    public Texture G2() {
        return assetManager.get("images/title/titleG2.png", Texture.class);
    }

    public Texture G7() {
        return assetManager.get("images/title/titleG7.png", Texture.class);
    }

    public Texture K11() {
        return assetManager.get("images/title/titleK11.png", Texture.class);
    }

    public Texture L4() {
        return assetManager.get("images/title/titleL4.png", Texture.class);
    }

    public Texture L8() {
        return assetManager.get("images/title/titleL8.png", Texture.class);
    }

    public Texture O1() {
        return assetManager.get("images/title/titleO1.png", Texture.class);
    }

    public Texture O5() {
        return assetManager.get("images/title/titleO5.png", Texture.class);
    }

    public Texture S12() {
        return assetManager.get("images/title/titleS12.png", Texture.class);
    }

    public Texture T6() {
        return assetManager.get("images/title/titleT6.png", Texture.class);
    }

    public Texture grijslaag() {
        return assetManager.get("images/grijslaag.png", Texture.class);
    }

    public Texture tut1() {
        return assetManager.get("images/tutorial/1_tutWhiteTouch1.png", Texture.class);
    }

    public Texture tut2() {
        return assetManager.get("images/tutorial/2_tutBlueTouch1.png", Texture.class);
    }

    public Texture tut3() {
        return assetManager.get("images/tutorial/3_tutBlueTouch2.png", Texture.class);
    }

    public Texture tut4() {
        return assetManager.get("images/tutorial/4_tutBlueTouch1.png", Texture.class);
    }

    public Texture arrowLeft() {
        return assetManager.get("images/tutorial/arrowLeft.png", Texture.class);
    }

    public Texture arrowLeftActive() {
        return assetManager.get("images/tutorial/arrowLeftActive.png", Texture.class);
    }

    public Texture creditsScreen() {
        return assetManager.get("images/credits/credit.png", Texture.class);
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }
}
