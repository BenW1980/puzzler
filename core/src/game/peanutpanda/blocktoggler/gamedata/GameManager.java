package game.peanutpanda.blocktoggler.gamedata;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import game.peanutpanda.blocktoggler.gameobjects.Player;
import game.peanutpanda.blocktoggler.levels.Chapter;
import game.peanutpanda.blocktoggler.levels.Chapter1;
import game.peanutpanda.blocktoggler.levels.Chapter2;
import game.peanutpanda.blocktoggler.levels.Chapter3;
import game.peanutpanda.blocktoggler.levels.Level;

public class GameManager {

    private Player player;
    private Chapter chapter1;
    private Chapter chapter2;
    private Chapter chapter3;
    private Preferences prefs;

    public GameManager() {
        prefs = Gdx.app.getPreferences("game-prefs");
        player = new Player();
        chapter1 = new Chapter1();
        chapter2 = new Chapter2();
        chapter3 = new Chapter3();
    }

    public void unlockFirst19Levels() { //  debugging
        for (int i = 1; i < chapter1.getLevelMap().size() + 1; i++) {
            chapter1.getLevelMap().get(i).setUnlocked(true);
        }

    }

    public void unlockChapter1() { //  debugging
        for (int i = 1; i < chapter1.getLevelMap().size() + 1; i++) {
            chapter1.getLevelMap().get(i).setUnlocked(true);
        }
    }

    public void unlockChapter2() { //  debugging
        chapter2.setUnlocked(true);
        for (int i = 1; i < chapter2.getLevelMap().size() + 1; i++) {
            chapter2.getLevelMap().get(i).setUnlocked(true);
        }
    }

    public void unlockChapter3() { //  debugging
        chapter3.setUnlocked(true);
        for (int i = 1; i < chapter3.getLevelMap().size() + 1; i++) {
            chapter3.getLevelMap().get(i).setUnlocked(true);
        }
    }

    public void unlockGame() { //  debugging
        unlockChapter1();
        unlockChapter2();
        unlockChapter3();
    }

    private void loadChapterOneStars() {
        for (int j = 1; j <= chapter1.getLevelMap().size(); j++) {
            chapter1.getLevelMap().get(j).setStars(prefs.getInteger("stars_" + chapter1.getLevelMap().get(j).getLevelName()));
        }
    }

    private void loadChapterTwoStars() {
        for (int j = 1; j <= chapter2.getLevelMap().size(); j++) {
            chapter2.getLevelMap().get(j).setStars(prefs.getInteger("stars_" + chapter2.getLevelMap().get(j).getLevelName()));
        }
    }

    private void loadChapterThreeStars() {
        for (int j = 1; j <= chapter3.getLevelMap().size(); j++) {
            chapter3.getLevelMap().get(j).setStars(prefs.getInteger("stars_" + chapter3.getLevelMap().get(j).getLevelName()));
        }
    }

    private void loadChapterOneUnlocked() {
        for (int j = 2; j <= chapter1.getLevelMap().size(); j++) {
            chapter1.getLevelMap().get(j).setUnlocked(prefs.getBoolean("unlocked_" + chapter1.getLevelMap().get(j).getLevelName()));
        }
    }

    private void loadChapterTwoUnlocked() {
        for (int j = 1; j <= chapter2.getLevelMap().size(); j++) {
            chapter2.getLevelMap().get(j).setUnlocked(prefs.getBoolean("unlocked_" + chapter2.getLevelMap().get(j).getLevelName()));
        }
    }

    private void loadChapterThreeUnlocked() {
        for (int j = 1; j <= chapter3.getLevelMap().size(); j++) {
            chapter3.getLevelMap().get(j).setUnlocked(prefs.getBoolean("unlocked_" + chapter3.getLevelMap().get(j).getLevelName()));
        }
    }

    public int totalLevelStars() {

        int result = 0;

        for (Level level : chapter1.getLevelMap().values()) {
            result += level.getStars();
        }

        for (Level level : chapter2.getLevelMap().values()) {
            result += level.getStars();
        }

        for (Level level : chapter3.getLevelMap().values()) {
            result += level.getStars();
        }

        return result;
    }

    public void checkUnlockedChapters() {

        if (chapter1.getLevelMap().get(20).isCompleted()) {
            chapter2.getLevelMap().get(1).setUnlocked(true);
        }

        if (chapter2.getLevelMap().get(20).isCompleted()) {
            chapter3.getLevelMap().get(1).setUnlocked(true);
        }

    }

    private void loadUnlockedLevels() {
        loadChapterOneUnlocked();
        loadChapterTwoUnlocked();
        loadChapterThreeUnlocked();
    }

    private void loadLevelStars() {
        loadChapterOneStars();
        loadChapterTwoStars();
        loadChapterThreeStars();
    }

    public void loadProgress() {
        loadUnlockedLevels();
        loadLevelStars();
        player.setStars(totalLevelStars());
    }

    public void saveLevelUnlocked(String levelName) {
        prefs.putBoolean("unlocked_" + levelName, true);

    }

    public void saveLevelStars(String levelName, int stars) {
        prefs.putInteger("stars_" + levelName, stars);

    }

    public void saveAudioSettings(boolean onOff) {
        prefs.putBoolean("audio", onOff);
    }

    public boolean loadAudioSettings() {
        return prefs.getBoolean("audio");
    }

    public void saveTimesPlayed(int times) {
        prefs.putInteger("times", times);
    }

    public int getTimesPlayed() {
        return prefs.getInteger("times");
    }

    public Preferences getPrefs() {
        return prefs;
    }

    public Chapter getChapter1() {
        return chapter1;
    }

    public Chapter getChapter2() {
        return chapter2;
    }

    public Player getPlayer() {
        return player;
    }

    public Chapter getChapter3() {
        return chapter3;
    }

}
