package game.peanutpanda.blocktoggler.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.FitViewport;

import game.peanutpanda.blocktoggler.gamedata.AssetLoader;
import game.peanutpanda.blocktoggler.gamedata.AudioHandler;
import game.peanutpanda.blocktoggler.gamedata.GameManager;
import game.peanutpanda.blocktoggler.gamedata.ScreenSize;
import game.peanutpanda.blocktoggler.gameobjects.Player;

public class CreditsScreen implements Screen, InputProcessor {

    private OrthographicCamera camera;
    private SpriteBatch batch;
    private Player player;
    private GameManager gMan;
    private Vector3 worldCoordinates;
    private AudioHandler audioHandler;
    private AssetLoader assetLoader;
    private FitViewport viewport;

    private Sprite backToMain;
    private Rectangle backToMainRect;
    private Rectangle twRectPP;
    private Rectangle twRectEW;

    private Texture twButtonTexturePP;
    private Texture twButtonTextureEW;

    public CreditsScreen(OrthographicCamera camera, SpriteBatch batch, Player player, GameManager gMan, AudioHandler audioHandler, AssetLoader assetLoader) {

        Gdx.input.setInputProcessor(this);
        Gdx.input.setCatchBackKey(true);
        this.camera = camera;
        this.batch = batch;
        this.player = player;
        this.gMan = gMan;
        this.audioHandler = audioHandler;
        this.assetLoader = assetLoader;
        camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);

        backToMainRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 150, 40, 60, 52);
        backToMain = new Sprite(assetLoader.arrowLeft());
        backToMain.setBounds(backToMainRect.x, backToMainRect.y, backToMainRect.width, backToMainRect.height);

        twRectPP = new Rectangle(340, 675, 35, 30);
        twRectEW = new Rectangle(340, 537, 35, 30);
        twButtonTexturePP = assetLoader.twButton();
        twButtonTextureEW = assetLoader.twButton();

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();
        batch.draw(assetLoader.creditsScreen(), 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        backToMain.draw(batch);
        batch.draw(twButtonTexturePP, twRectPP.x, twRectPP.y, twRectPP.width, twRectPP.height);
        batch.draw(twButtonTextureEW, twRectEW.x, twRectEW.y, twRectEW.width, twRectEW.height);
        batch.end();

        audioHandler.resetMusic();

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        assetLoader.getAssetManager().dispose();
    }

    @Override
    public boolean keyDown(int keycode) {

        if (keycode == Input.Keys.BACK) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen(batch, camera, player, gMan, assetLoader, audioHandler));
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0) {
            return false;
        }

        worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        player.move(worldCoordinates);

        if (player.taps(backToMainRect)) {
            backToMain.setTexture(assetLoader.arrowLeftActive());
        } else {
            backToMain.setTexture(assetLoader.arrowLeft());
        }

        if (player.taps(twRectPP)) {
            twButtonTexturePP = assetLoader.twButtonUP();
        } else {
            twButtonTexturePP = assetLoader.twButton();
        }

        if (player.taps(twRectEW)) {
            twButtonTextureEW = assetLoader.twButtonUP();
        } else {
            twButtonTextureEW = assetLoader.twButton();
        }


        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0) {
            return false;
        }

        if (player.taps(backToMainRect)) {
            backToMain.setTexture(assetLoader.arrowLeft());
            ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen(batch, camera, player, gMan, assetLoader, audioHandler));
        }

        if (player.taps(twRectPP)) {
            twButtonTexturePP = assetLoader.twButton();
            if (!Gdx.net.openURI("twitter://user?user_id=741271262006251520")) { // open app
                Gdx.net.openURI("https://twitter.com/PeanutPandaApps"); // open site
            }
        }

        if (player.taps(twRectEW)) {
            twButtonTextureEW = assetLoader.twButton();
            if (!Gdx.net.openURI("twitter://user?user_id=753832720002789377")) { // open app
                Gdx.net.openURI("https://twitter.com/eva_willaert"); // open site
            }
        }

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        if (pointer > 0) {
            return false;
        }

        worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        player.move(worldCoordinates);

        if (player.taps(backToMainRect)) {
            backToMain.setTexture(assetLoader.arrowLeftActive());
        } else {
            backToMain.setTexture(assetLoader.arrowLeft());
        }

        if (player.taps(twRectPP)) {
            twButtonTexturePP = assetLoader.twButtonUP();
        } else {
            twButtonTexturePP = assetLoader.twButton();
        }

        if (player.taps(twRectEW)) {
            twButtonTextureEW = assetLoader.twButtonUP();
        } else {
            twButtonTextureEW = assetLoader.twButton();
        }


        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
