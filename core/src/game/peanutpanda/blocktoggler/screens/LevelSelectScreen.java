package game.peanutpanda.blocktoggler.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;

import game.peanutpanda.blocktoggler.gamedata.AssetLoader;
import game.peanutpanda.blocktoggler.gamedata.AudioHandler;
import game.peanutpanda.blocktoggler.gamedata.GameManager;
import game.peanutpanda.blocktoggler.gamedata.ScreenSize;
import game.peanutpanda.blocktoggler.gameobjects.Player;
import game.peanutpanda.blocktoggler.levels.Chapter;

public class LevelSelectScreen implements Screen, InputProcessor, GestureListener {

    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Player player;
    private boolean scrollingLeft = false;
    private boolean scrollingRight = false;
    private boolean screenInitializedRight = false;
    private boolean somethingIsScrolling = false;

    private Rectangle level1_1Rect;
    private Rectangle level1_2Rect;
    private Rectangle level1_3Rect;
    private Rectangle level1_4Rect;
    private Rectangle level1_5Rect;
    private Rectangle level1_6Rect;
    private Rectangle level1_7Rect;
    private Rectangle level1_8Rect;
    private Rectangle level1_9Rect;
    private Rectangle level1_10Rect;
    private Rectangle level1_11Rect;
    private Rectangle level1_12Rect;
    private Rectangle level1_13Rect;
    private Rectangle level1_14Rect;
    private Rectangle level1_15Rect;
    private Rectangle level1_16Rect;
    private Rectangle level1_17Rect;
    private Rectangle level1_18Rect;
    private Rectangle level1_19Rect;
    private Rectangle level1_20Rect;

    private Rectangle level2_1Rect;
    private Rectangle level2_2Rect;
    private Rectangle level2_3Rect;
    private Rectangle level2_4Rect;
    private Rectangle level2_5Rect;
    private Rectangle level2_6Rect;
    private Rectangle level2_7Rect;
    private Rectangle level2_8Rect;
    private Rectangle level2_9Rect;
    private Rectangle level2_10Rect;
    private Rectangle level2_11Rect;
    private Rectangle level2_12Rect;
    private Rectangle level2_13Rect;
    private Rectangle level2_14Rect;
    private Rectangle level2_15Rect;
    private Rectangle level2_16Rect;
    private Rectangle level2_17Rect;
    private Rectangle level2_18Rect;
    private Rectangle level2_19Rect;
    private Rectangle level2_20Rect;

    private Rectangle level3_1Rect;
    private Rectangle level3_2Rect;
    private Rectangle level3_3Rect;
    private Rectangle level3_4Rect;
    private Rectangle level3_5Rect;
    private Rectangle level3_6Rect;
    private Rectangle level3_7Rect;
    private Rectangle level3_8Rect;
    private Rectangle level3_9Rect;
    private Rectangle level3_10Rect;
    private Rectangle level3_11Rect;
    private Rectangle level3_12Rect;
    private Rectangle level3_13Rect;
    private Rectangle level3_14Rect;
    private Rectangle level3_15Rect;
    private Rectangle level3_16Rect;
    private Rectangle level3_17Rect;
    private Rectangle level3_18Rect;
    private Rectangle level3_19Rect;
    private Rectangle level3_20Rect;

    private Rectangle leftRect;
    private Rectangle rightRect;
    private Rectangle homeRect;
    private Rectangle homeTouchRect;

    private Texture leftTexture;
    private Texture rightTexture;
    private Texture homeTexture;

    private Array<Rectangle> allRects;
    private Array<Array<Rectangle>> allRectArrays;
    private GameManager gMan;
    private AudioHandler audioHandler;
    private AssetLoader assetLoader;
    private Chapter chapter;
    private int levelWidth = 100;
    private int levelHeight = 100;
    private int row1H = 240;
    private int row2H = 345;
    private int row3H = 450;
    private int row4H = 555;
    private int row5H = 660;
    private int col1 = 32;
    private int col2 = 137;
    private int col3 = 242;
    private int col4 = 347;
    private FitViewport viewport;
    private BitmapFont font;

    private Texture level1_1Texture;
    private Texture level1_2Texture;
    private Texture level1_3Texture;
    private Texture level1_4Texture;
    private Texture level1_5Texture;
    private Texture level1_6Texture;
    private Texture level1_7Texture;
    private Texture level1_8Texture;
    private Texture level1_9Texture;
    private Texture level1_10Texture;
    private Texture level1_11Texture;
    private Texture level1_12Texture;
    private Texture level1_13Texture;
    private Texture level1_14Texture;
    private Texture level1_15Texture;
    private Texture level1_16Texture;
    private Texture level1_17Texture;
    private Texture level1_18Texture;
    private Texture level1_19Texture;
    private Texture level1_20Texture;

    private Texture level2_1Texture;
    private Texture level2_2Texture;
    private Texture level2_3Texture;
    private Texture level2_4Texture;
    private Texture level2_5Texture;
    private Texture level2_6Texture;
    private Texture level2_7Texture;
    private Texture level2_8Texture;
    private Texture level2_9Texture;
    private Texture level2_10Texture;
    private Texture level2_11Texture;
    private Texture level2_12Texture;
    private Texture level2_13Texture;
    private Texture level2_14Texture;
    private Texture level2_15Texture;
    private Texture level2_16Texture;
    private Texture level2_17Texture;
    private Texture level2_18Texture;
    private Texture level2_19Texture;
    private Texture level2_20Texture;

    private Texture level3_1Texture;
    private Texture level3_2Texture;
    private Texture level3_3Texture;
    private Texture level3_4Texture;
    private Texture level3_5Texture;
    private Texture level3_6Texture;
    private Texture level3_7Texture;
    private Texture level3_8Texture;
    private Texture level3_9Texture;
    private Texture level3_10Texture;
    private Texture level3_11Texture;
    private Texture level3_12Texture;
    private Texture level3_13Texture;
    private Texture level3_14Texture;
    private Texture level3_15Texture;
    private Texture level3_16Texture;
    private Texture level3_17Texture;
    private Texture level3_18Texture;
    private Texture level3_19Texture;
    private Texture level3_20Texture;

    private Array<Texture> levelButtonTextures;
    private Array<Texture> levelButtonActiveTextures;

    private Array<Rectangle> rectArray1;
    private Array<Rectangle> rectArray2;
    private Array<Rectangle> rectArray3;

    private Array<Texture> textureArray1;
    private Array<Texture> textureArray2;
    private Array<Texture> textureArray3;

    public LevelSelectScreen(SpriteBatch batch, OrthographicCamera camera, Player player, GameManager gMan, Chapter chapter, AudioHandler audioHandler, AssetLoader assetLoader) {

        InputMultiplexer im = new InputMultiplexer();
        GestureDetector gd = new GestureDetector(this);
        im.addProcessor(gd);
        im.addProcessor(this);
        Gdx.input.setInputProcessor(im);

        Gdx.input.setCatchBackKey(true);
        this.batch = batch;
        this.camera = camera;
        this.player = player;
        this.chapter = chapter;
        this.allRects = new Array<Rectangle>();
        this.allRectArrays = new Array<Array<Rectangle>>();
        this.gMan = gMan;
        this.audioHandler = audioHandler;
        this.assetLoader = assetLoader;
        viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);
        font = assetLoader.getAssetManager().get("size65.ttf", BitmapFont.class);
        font.getData().setScale(0.77f);

        levelButtonTextures = new Array<Texture>();
        levelButtonActiveTextures = new Array<Texture>();

        rectArray1 = new Array<Rectangle>();
        rectArray2 = new Array<Rectangle>();
        rectArray3 = new Array<Rectangle>();

        textureArray1 = new Array<Texture>();
        textureArray2 = new Array<Texture>();
        textureArray3 = new Array<Texture>();

        if (chapter == gMan.getChapter1()) {
            drawChapter1Rects();
        } else if (chapter == gMan.getChapter2()) {
            drawChapter2Rects();
        } else if (chapter == gMan.getChapter3()) {
            drawChapter3Rects();
        }

        levelButtonTextures.add(assetLoader.l1());
        levelButtonActiveTextures.add(assetLoader.l1Active());
        levelButtonTextures.add(assetLoader.l2());
        levelButtonActiveTextures.add(assetLoader.l2Active());
        levelButtonTextures.add(assetLoader.l3());
        levelButtonActiveTextures.add(assetLoader.l3Active());
        levelButtonTextures.add(assetLoader.l4());
        levelButtonActiveTextures.add(assetLoader.l4Active());
        levelButtonTextures.add(assetLoader.l5());
        levelButtonActiveTextures.add(assetLoader.l5Active());
        levelButtonTextures.add(assetLoader.l6());
        levelButtonActiveTextures.add(assetLoader.l6Active());
        levelButtonTextures.add(assetLoader.l7());
        levelButtonActiveTextures.add(assetLoader.l7Active());
        levelButtonTextures.add(assetLoader.l8());
        levelButtonActiveTextures.add(assetLoader.l8Active());
        levelButtonTextures.add(assetLoader.l9());
        levelButtonActiveTextures.add(assetLoader.l9Active());
        levelButtonTextures.add(assetLoader.l10());
        levelButtonActiveTextures.add(assetLoader.l10Active());
        levelButtonTextures.add(assetLoader.l11());
        levelButtonActiveTextures.add(assetLoader.l11Active());
        levelButtonTextures.add(assetLoader.l12());
        levelButtonActiveTextures.add(assetLoader.l12Active());
        levelButtonTextures.add(assetLoader.l13());
        levelButtonActiveTextures.add(assetLoader.l13Active());
        levelButtonTextures.add(assetLoader.l14());
        levelButtonActiveTextures.add(assetLoader.l14Active());
        levelButtonTextures.add(assetLoader.l15());
        levelButtonActiveTextures.add(assetLoader.l15Active());
        levelButtonTextures.add(assetLoader.l16());
        levelButtonActiveTextures.add(assetLoader.l16Active());
        levelButtonTextures.add(assetLoader.l17());
        levelButtonActiveTextures.add(assetLoader.l17Active());
        levelButtonTextures.add(assetLoader.l18());
        levelButtonActiveTextures.add(assetLoader.l18Active());
        levelButtonTextures.add(assetLoader.l19());
        levelButtonActiveTextures.add(assetLoader.l19Active());
        levelButtonTextures.add(assetLoader.l20());
        levelButtonActiveTextures.add(assetLoader.l20Active());


        rectArray1.add(level1_1Rect);
        rectArray1.add(level1_2Rect);
        rectArray1.add(level1_3Rect);
        rectArray1.add(level1_4Rect);
        rectArray1.add(level1_5Rect);
        rectArray1.add(level1_6Rect);
        rectArray1.add(level1_7Rect);
        rectArray1.add(level1_8Rect);
        rectArray1.add(level1_9Rect);
        rectArray1.add(level1_10Rect);
        rectArray1.add(level1_11Rect);
        rectArray1.add(level1_12Rect);
        rectArray1.add(level1_13Rect);
        rectArray1.add(level1_14Rect);
        rectArray1.add(level1_15Rect);
        rectArray1.add(level1_16Rect);
        rectArray1.add(level1_17Rect);
        rectArray1.add(level1_18Rect);
        rectArray1.add(level1_19Rect);
        rectArray1.add(level1_20Rect);

        rectArray2.add(level2_1Rect);
        rectArray2.add(level2_2Rect);
        rectArray2.add(level2_3Rect);
        rectArray2.add(level2_4Rect);
        rectArray2.add(level2_5Rect);
        rectArray2.add(level2_6Rect);
        rectArray2.add(level2_7Rect);
        rectArray2.add(level2_8Rect);
        rectArray2.add(level2_9Rect);
        rectArray2.add(level2_10Rect);
        rectArray2.add(level2_11Rect);
        rectArray2.add(level2_12Rect);
        rectArray2.add(level2_13Rect);
        rectArray2.add(level2_14Rect);
        rectArray2.add(level2_15Rect);
        rectArray2.add(level2_16Rect);
        rectArray2.add(level2_17Rect);
        rectArray2.add(level2_18Rect);
        rectArray2.add(level2_19Rect);
        rectArray2.add(level2_20Rect);

        rectArray3.add(level3_1Rect);
        rectArray3.add(level3_2Rect);
        rectArray3.add(level3_3Rect);
        rectArray3.add(level3_4Rect);
        rectArray3.add(level3_5Rect);
        rectArray3.add(level3_6Rect);
        rectArray3.add(level3_7Rect);
        rectArray3.add(level3_8Rect);
        rectArray3.add(level3_9Rect);
        rectArray3.add(level3_10Rect);
        rectArray3.add(level3_11Rect);
        rectArray3.add(level3_12Rect);
        rectArray3.add(level3_13Rect);
        rectArray3.add(level3_14Rect);
        rectArray3.add(level3_15Rect);
        rectArray3.add(level3_16Rect);
        rectArray3.add(level3_17Rect);
        rectArray3.add(level3_18Rect);
        rectArray3.add(level3_19Rect);
        rectArray3.add(level3_20Rect);


        allRects.addAll(rectArray1);
        allRects.addAll(rectArray2);
        allRects.addAll(rectArray3);

        allRectArrays.add(rectArray1);
        allRectArrays.add(rectArray2);
        allRectArrays.add(rectArray3);

        Texture lockedTexture = assetLoader.locked();

        camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());

        textureArray1.add(level1_1Texture);
        textureArray1.add(level1_2Texture);
        textureArray1.add(level1_3Texture);
        textureArray1.add(level1_4Texture);
        textureArray1.add(level1_5Texture);
        textureArray1.add(level1_6Texture);
        textureArray1.add(level1_7Texture);
        textureArray1.add(level1_8Texture);
        textureArray1.add(level1_9Texture);
        textureArray1.add(level1_10Texture);
        textureArray1.add(level1_11Texture);
        textureArray1.add(level1_12Texture);
        textureArray1.add(level1_13Texture);
        textureArray1.add(level1_14Texture);
        textureArray1.add(level1_15Texture);
        textureArray1.add(level1_16Texture);
        textureArray1.add(level1_17Texture);
        textureArray1.add(level1_18Texture);
        textureArray1.add(level1_19Texture);
        textureArray1.add(level1_20Texture);

        textureArray2.add(level2_1Texture);
        textureArray2.add(level2_2Texture);
        textureArray2.add(level2_3Texture);
        textureArray2.add(level2_4Texture);
        textureArray2.add(level2_5Texture);
        textureArray2.add(level2_6Texture);
        textureArray2.add(level2_7Texture);
        textureArray2.add(level2_8Texture);
        textureArray2.add(level2_9Texture);
        textureArray2.add(level2_10Texture);
        textureArray2.add(level2_11Texture);
        textureArray2.add(level2_12Texture);
        textureArray2.add(level2_13Texture);
        textureArray2.add(level2_14Texture);
        textureArray2.add(level2_15Texture);
        textureArray2.add(level2_16Texture);
        textureArray2.add(level2_17Texture);
        textureArray2.add(level2_18Texture);
        textureArray2.add(level2_19Texture);
        textureArray2.add(level2_20Texture);

        textureArray3.add(level3_1Texture);
        textureArray3.add(level3_2Texture);
        textureArray3.add(level3_3Texture);
        textureArray3.add(level3_4Texture);
        textureArray3.add(level3_5Texture);
        textureArray3.add(level3_6Texture);
        textureArray3.add(level3_7Texture);
        textureArray3.add(level3_8Texture);
        textureArray3.add(level3_9Texture);
        textureArray3.add(level3_10Texture);
        textureArray3.add(level3_11Texture);
        textureArray3.add(level3_12Texture);
        textureArray3.add(level3_13Texture);
        textureArray3.add(level3_14Texture);
        textureArray3.add(level3_15Texture);
        textureArray3.add(level3_16Texture);
        textureArray3.add(level3_17Texture);
        textureArray3.add(level3_18Texture);
        textureArray3.add(level3_19Texture);
        textureArray3.add(level3_20Texture);

        for (int i = 0; i < 20; i++) {
            textureArray1.set(i, gMan.getChapter1().getLevelMap().get(i + 1).isUnlocked() ? levelButtonTextures.get(i) : lockedTexture);
        }

        for (int i = 0; i < 20; i++) {
            textureArray2.set(i, gMan.getChapter2().getLevelMap().get(i + 1).isUnlocked() ? levelButtonTextures.get(i) : lockedTexture);
        }

        for (int i = 0; i < 20; i++) {
            textureArray3.set(i, gMan.getChapter3().getLevelMap().get(i + 1).isUnlocked() ? levelButtonTextures.get(i) : lockedTexture);
        }

        leftRect = new Rectangle(37, 65, 45, 45);
        rightRect = new Rectangle(400, 65, 45, 45);
        homeRect = new Rectangle(ScreenSize.WIDTH.getSize() - 48, ScreenSize.HEIGHT.getSize() - 95, 40, 34);
        homeTouchRect = new Rectangle(ScreenSize.WIDTH.getSize() - 60, ScreenSize.HEIGHT.getSize() - 110, 50, 50);

        leftTexture = assetLoader.left();
        rightTexture = assetLoader.right();
        homeTexture = assetLoader.home();

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();

        audioHandler.resetMusic();

        batch.draw(assetLoader.background(), 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        batch.draw(assetLoader.starsCompletedHeader(), ScreenSize.WIDTH.getSize() - 205, ScreenSize.HEIGHT.getSize() - 100, 205, 100);

        if (chapter.stars() == 60) {
            batch.draw(assetLoader.worldTotalStarsComplete(), 435, 750, 35, 35);
        } else {
            batch.draw(assetLoader.worldTotalStarsIncomplete(), 435, 750, 35, 35);
        }

        font.draw(batch, chapter.starsAsString(), 282, 786);
        font.draw(batch, "" + 60, 357, 786);

        for (int i = 0; i < 20; i++) {
            batch.draw(textureArray1.get(i), rectArray1.get(i).x, rectArray1.get(i).y, rectArray1.get(i).width, rectArray1.get(i).height);
        }

        for (int i = 0; i < 20; i++) {
            batch.draw(textureArray2.get(i), rectArray2.get(i).x, rectArray2.get(i).y, rectArray2.get(i).width, rectArray2.get(i).height);
        }

        for (int i = 0; i < 20; i++) {
            batch.draw(textureArray3.get(i), rectArray3.get(i).x, rectArray3.get(i).y, rectArray3.get(i).width, rectArray3.get(i).height);
        }


        // shows earned sterrekes per levelSelect block:

        if (!somethingIsScrolling) {
            for (Array<Rectangle> rectArray : allRectArrays) {
                showStarsGainedPerLevel(rectArray);
            }
        }

        if (chapter == gMan.getChapter1()) {
            batch.draw(assetLoader.worldSelect1(), 32, 50, 417, 73);

        } else if (chapter == gMan.getChapter2()) {
            batch.draw(assetLoader.worldSelect2(), 32, 50, 417, 73);

        } else if (chapter == gMan.getChapter3()) {
            batch.draw(assetLoader.worldSelect3(), 32, 50, 417, 73);
        }

        batch.draw(leftTexture, leftRect.x, leftRect.y, leftRect.width, leftRect.height);
        batch.draw(rightTexture, rightRect.x, rightRect.y, rightRect.width, rightRect.height);
        batch.draw(homeTexture, homeRect.x, homeRect.y, homeRect.width, homeRect.height);

        batch.end();

        // level-init (auto-scroll)

        int scrollSpeed = 2000;
        if (!screenInitializedRight) {
            if (chapter.getChapterNumber() == 1) {
                if (level1_1Rect.x > col1) {
                    somethingIsScrolling = true;
                    for (Rectangle rect : allRects) {
                        rect.x -= scrollSpeed * delta;
                    }
                    if (level1_1Rect.x < col1) {
                        stopScrollingOnInit();
                        screenInitializedRight = true;
                        somethingIsScrolling = false;
                    }
                }

            } else if (chapter.getChapterNumber() == 2) {
                if (level2_1Rect.x > col1) {
                    somethingIsScrolling = true;
                    for (Rectangle rect : allRects) {
                        rect.x -= scrollSpeed * delta;
                    }
                    if (level2_1Rect.x < col1) {
                        stopScrollingOn2Right();
                        screenInitializedRight = true;
                        somethingIsScrolling = false;
                    }
                }

            } else if (chapter.getChapterNumber() == 3) {
                if (level3_1Rect.x > col1) {
                    somethingIsScrolling = true;
                    for (Rectangle rect : allRects) {
                        rect.x -= scrollSpeed * delta;
                    }
                    if (level3_1Rect.x < col1) {
                        stopScrollingOn3Right();
                        screenInitializedRight = true;
                        somethingIsScrolling = false;
                    }
                }
            }
        }

        // scrolling
        if (scrollingRight) {
            somethingIsScrolling = true;
            for (Rectangle rect : allRects) {
                rect.x -= scrollSpeed * delta;
            }

            if (chapter == gMan.getChapter1()) {
                if (level1_4Rect.x < 0 - 127) {
                    scrollingRight = false;
                    stopScrollingOn2Right();
                    somethingIsScrolling = false;

                }
            } else if (chapter == gMan.getChapter2()) {
                if (level2_4Rect.x < 0 - 127) {
                    scrollingRight = false;
                    stopScrollingOn3Right();
                    somethingIsScrolling = false;
                }
            }
        }

        if (scrollingLeft) {
            somethingIsScrolling = true;
            for (Rectangle rect : allRects) {
                rect.x += scrollSpeed * delta;
            }

            if (chapter == gMan.getChapter3()) {
                if (level3_1Rect.x > ScreenSize.WIDTH.getSize() + col1) {
                    scrollingLeft = false;
                    stopScrollingOn2Right();
                    somethingIsScrolling = false;
                }

            } else if (chapter == gMan.getChapter2()) {
                if (level2_1Rect.x > ScreenSize.WIDTH.getSize() + col1) {
                    scrollingLeft = false;
                    stopScrollingOnInit();
                    somethingIsScrolling = false;
                }
            }
        }
        gMan.checkUnlockedChapters();
    }

    private void showStarsGainedPerLevel(Array<Rectangle> rectArray) {

        for (int i = 0; i < rectArray.size; i++) {
            if (chapter.getLevelMap().get(i + 1).isUnlocked()) {
                int starsWidth = 70;
                int starsHeight = 18;
                if (chapter.getLevelMap().get(i + 1).getStars() == 0) {
                    batch.draw(assetLoader.noStars(), rectArray.get(i).x + 15, rectArray.get(i).y + 12, starsWidth, starsHeight);

                } else if (chapter.getLevelMap().get(i + 1).getStars() == 1) {
                    batch.draw(assetLoader.oneStar(), rectArray.get(i).x + 15, rectArray.get(i).y + 12, starsWidth, starsHeight);

                } else if (chapter.getLevelMap().get(i + 1).getStars() == 2) {
                    batch.draw(assetLoader.twoStars(), rectArray.get(i).x + 15, rectArray.get(i).y + 12, starsWidth, starsHeight);

                } else if (chapter.getLevelMap().get(i + 1).getStars() == 3) {
                    batch.draw(assetLoader.threeStars(), rectArray.get(i).x + 15, rectArray.get(i).y + 12, starsWidth, starsHeight);

                }
            }
        }
    }

    private void drawChapter1Rects() {
        level1_1Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_2Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_3Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_4Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_5Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_6Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_7Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_8Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_9Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_10Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_11Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_12Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_13Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_14Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_15Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_16Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_17Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_18Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_19Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_20Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        level2_1Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_2Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_3Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_4Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_5Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_6Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_7Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_8Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_9Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_10Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_11Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_12Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_13Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_14Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_15Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_16Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_17Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_18Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_19Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_20Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        level3_1Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_2Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_3Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_4Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_5Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_6Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_7Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_8Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_9Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_10Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_11Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_12Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_13Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_14Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_15Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_16Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_17Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_18Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_19Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_20Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 3, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
    }

    private void drawChapter2Rects() {
        level1_1Rect = new Rectangle(col1, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_2Rect = new Rectangle(col2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_3Rect = new Rectangle(col3, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_4Rect = new Rectangle(col4, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_5Rect = new Rectangle(col1, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_6Rect = new Rectangle(col2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_7Rect = new Rectangle(col3, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_8Rect = new Rectangle(col4, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_9Rect = new Rectangle(col1, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_10Rect = new Rectangle(col2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_11Rect = new Rectangle(col3, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_12Rect = new Rectangle(col4, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_13Rect = new Rectangle(col1, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_14Rect = new Rectangle(col2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_15Rect = new Rectangle(col3, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_16Rect = new Rectangle(col4, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_17Rect = new Rectangle(col1, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_18Rect = new Rectangle(col2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_19Rect = new Rectangle(col3, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_20Rect = new Rectangle(col4, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        level2_1Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_2Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_3Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_4Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_5Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_6Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_7Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_8Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_9Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_10Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_11Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_12Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_13Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_14Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_15Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_16Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_17Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_18Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_19Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_20Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        level3_1Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_2Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_3Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_4Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_5Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_6Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_7Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_8Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_9Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_10Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_11Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_12Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_13Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_14Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_15Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_16Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_17Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_18Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_19Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_20Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
    }

    private void drawChapter3Rects() {
        level1_1Rect = new Rectangle(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_2Rect = new Rectangle(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_3Rect = new Rectangle(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_4Rect = new Rectangle(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_5Rect = new Rectangle(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_6Rect = new Rectangle(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_7Rect = new Rectangle(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_8Rect = new Rectangle(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_9Rect = new Rectangle(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_10Rect = new Rectangle(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_11Rect = new Rectangle(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_12Rect = new Rectangle(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_13Rect = new Rectangle(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_14Rect = new Rectangle(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_15Rect = new Rectangle(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_16Rect = new Rectangle(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_17Rect = new Rectangle(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_18Rect = new Rectangle(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_19Rect = new Rectangle(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_20Rect = new Rectangle(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        level2_1Rect = new Rectangle(col1, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_2Rect = new Rectangle(col2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_3Rect = new Rectangle(col3, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_4Rect = new Rectangle(col4, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_5Rect = new Rectangle(col1, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_6Rect = new Rectangle(col2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_7Rect = new Rectangle(col3, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_8Rect = new Rectangle(col4, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_9Rect = new Rectangle(col1, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_10Rect = new Rectangle(col2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_11Rect = new Rectangle(col3, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_12Rect = new Rectangle(col4, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_13Rect = new Rectangle(col1, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_14Rect = new Rectangle(col2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_15Rect = new Rectangle(col3, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_16Rect = new Rectangle(col4, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_17Rect = new Rectangle(col1, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_18Rect = new Rectangle(col2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_19Rect = new Rectangle(col3, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_20Rect = new Rectangle(col4, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        level3_1Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_2Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_3Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_4Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_5Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_6Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_7Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_8Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_9Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_10Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_11Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_12Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_13Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_14Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_15Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_16Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_17Rect = new Rectangle(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_18Rect = new Rectangle(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_19Rect = new Rectangle(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_20Rect = new Rectangle(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
    }


    private void stopScrollingOnInit() {

        level1_1Rect.set(col1, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_2Rect.set(col2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_3Rect.set(col3, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_4Rect.set(col4, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_5Rect.set(col1, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_6Rect.set(col2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_7Rect.set(col3, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_8Rect.set(col4, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_9Rect.set(col1, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_10Rect.set(col2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_11Rect.set(col3, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_12Rect.set(col4, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_13Rect.set(col1, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_14Rect.set(col2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_15Rect.set(col3, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_16Rect.set(col4, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_17Rect.set(col1, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_18Rect.set(col2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_19Rect.set(col3, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_20Rect.set(col4, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        level2_1Rect.set(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_2Rect.set(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_3Rect.set(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_4Rect.set(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_5Rect.set(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_6Rect.set(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_7Rect.set(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_8Rect.set(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_9Rect.set(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_10Rect.set(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_11Rect.set(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_12Rect.set(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_13Rect.set(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_14Rect.set(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_15Rect.set(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_16Rect.set(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_17Rect.set(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_18Rect.set(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_19Rect.set(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_20Rect.set(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        level3_1Rect.set(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_2Rect.set(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_3Rect.set(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_4Rect.set(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_5Rect.set(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_6Rect.set(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_7Rect.set(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_8Rect.set(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_9Rect.set(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_10Rect.set(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_11Rect.set(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_12Rect.set(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_13Rect.set(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_14Rect.set(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_15Rect.set(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_16Rect.set(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_17Rect.set(col1 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_18Rect.set(col2 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_19Rect.set(col3 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_20Rect.set(col4 + ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        this.chapter = gMan.getChapter1();
    }

    private void stopScrollingOn2Right() {

        level1_1Rect.set(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_2Rect.set(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_3Rect.set(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_4Rect.set(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_5Rect.set(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_6Rect.set(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_7Rect.set(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_8Rect.set(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_9Rect.set(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_10Rect.set(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_11Rect.set(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_12Rect.set(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_13Rect.set(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_14Rect.set(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_15Rect.set(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_16Rect.set(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_17Rect.set(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_18Rect.set(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_19Rect.set(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_20Rect.set(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        level2_1Rect.set(col1, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_2Rect.set(col2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_3Rect.set(col3, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_4Rect.set(col4, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_5Rect.set(col1, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_6Rect.set(col2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_7Rect.set(col3, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_8Rect.set(col4, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_9Rect.set(col1, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_10Rect.set(col2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_11Rect.set(col3, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_12Rect.set(col4, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_13Rect.set(col1, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_14Rect.set(col2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_15Rect.set(col3, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_16Rect.set(col4, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_17Rect.set(col1, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_18Rect.set(col2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_19Rect.set(col3, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_20Rect.set(col4, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        level3_1Rect.set(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_2Rect.set(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_3Rect.set(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_4Rect.set(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_5Rect.set(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_6Rect.set(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_7Rect.set(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_8Rect.set(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_9Rect.set(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_10Rect.set(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_11Rect.set(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_12Rect.set(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_13Rect.set(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_14Rect.set(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_15Rect.set(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_16Rect.set(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_17Rect.set(col1 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_18Rect.set(col2 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_19Rect.set(col3 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_20Rect.set(col4 + ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        this.chapter = gMan.getChapter2();
    }

    private void stopScrollingOn3Right() {

        level1_1Rect.set(col1 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_2Rect.set(col2 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_3Rect.set(col3 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_4Rect.set(col4 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level1_5Rect.set(col1 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_6Rect.set(col2 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_7Rect.set(col3 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_8Rect.set(col4 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level1_9Rect.set(col1 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_10Rect.set(col2 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_11Rect.set(col3 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_12Rect.set(col4 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level1_13Rect.set(col1 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_14Rect.set(col2 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_15Rect.set(col3 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_16Rect.set(col4 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level1_17Rect.set(col1 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_18Rect.set(col2 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_19Rect.set(col3 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level1_20Rect.set(col4 - ScreenSize.WIDTH.getSize() * 2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        level2_1Rect.set(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_2Rect.set(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_3Rect.set(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_4Rect.set(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level2_5Rect.set(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_6Rect.set(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_7Rect.set(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_8Rect.set(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level2_9Rect.set(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_10Rect.set(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_11Rect.set(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_12Rect.set(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level2_13Rect.set(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_14Rect.set(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_15Rect.set(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_16Rect.set(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level2_17Rect.set(col1 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_18Rect.set(col2 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_19Rect.set(col3 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level2_20Rect.set(col4 - ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        level3_1Rect.set(col1, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_2Rect.set(col2, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_3Rect.set(col3, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_4Rect.set(col4, ScreenSize.HEIGHT.getSize() - row1H, levelWidth, levelHeight);
        level3_5Rect.set(col1, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_6Rect.set(col2, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_7Rect.set(col3, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_8Rect.set(col4, ScreenSize.HEIGHT.getSize() - row2H, levelWidth, levelHeight);
        level3_9Rect.set(col1, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_10Rect.set(col2, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_11Rect.set(col3, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_12Rect.set(col4, ScreenSize.HEIGHT.getSize() - row3H, levelWidth, levelHeight);
        level3_13Rect.set(col1, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_14Rect.set(col2, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_15Rect.set(col3, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_16Rect.set(col4, ScreenSize.HEIGHT.getSize() - row4H, levelWidth, levelHeight);
        level3_17Rect.set(col1, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_18Rect.set(col2, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_19Rect.set(col3, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);
        level3_20Rect.set(col4, ScreenSize.HEIGHT.getSize() - row5H, levelWidth, levelHeight);

        this.chapter = gMan.getChapter3();
    }

    public void buttonsDown(int screenX, int screenY) {

        Vector3 worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        player.move(worldCoordinates);

        if (player.taps(leftRect)) {
            leftTexture = assetLoader.leftActive();
        } else {
            leftTexture = assetLoader.left();
        }

        if (player.taps(rightRect)) {
            rightTexture = assetLoader.rightActive();
        } else {
            rightTexture = assetLoader.right();
        }

        if (player.taps(homeTouchRect)) {
            homeTexture = assetLoader.homeActive();
        } else {
            homeTexture = assetLoader.home();
        }

        for (int i = 0; i < 20; i++) {
            if (gMan.getChapter1().getLevelMap().get(i + 1).isUnlocked()) {
                if (player.taps(rectArray1.get(i))) {
                    textureArray1.set(i, levelButtonActiveTextures.get(i));

                } else {
                    textureArray1.set(i, levelButtonTextures.get(i));
                }
            }
        }

        for (int i = 0; i < 20; i++) {
            if (gMan.getChapter2().getLevelMap().get(i + 1).isUnlocked()) {
                if (player.taps(rectArray2.get(i))) {
                    textureArray2.set(i, levelButtonActiveTextures.get(i));

                } else {
                    textureArray2.set(i, levelButtonTextures.get(i));
                }
            }
        }

        for (int i = 0; i < 20; i++) {
            if (gMan.getChapter3().getLevelMap().get(i + 1).isUnlocked()) {
                if (player.taps(rectArray3.get(i))) {
                    textureArray3.set(i, levelButtonActiveTextures.get(i));

                } else {
                    textureArray3.set(i, levelButtonTextures.get(i));
                }
            }
        }
    }

    public void buttonsUp() {

        if (player.taps(leftRect)) {
            leftTexture = assetLoader.left();
        }

        if (player.taps(rightRect)) {
            rightTexture = assetLoader.right();
        }

        if (player.taps(homeTouchRect)) {
            homeTexture = assetLoader.home();
            ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen(batch, camera, player, gMan, assetLoader, audioHandler));
        }

        for (int i = 0; i < 20; i++) {
            if (player.taps(rectArray1.get(i))) {
                if (gMan.getChapter1().getLevelMap().get(i + 1).isUnlocked()) {
                    textureArray1.set(i, levelButtonTextures.get(i));
                }

            }
        }

        for (int i = 0; i < 20; i++) {
            if (player.taps(rectArray2.get(i))) {
                if (gMan.getChapter2().getLevelMap().get(i + 1).isUnlocked()) {
                    textureArray2.set(i, levelButtonTextures.get(i));
                }

            }
        }

        for (int i = 0; i < 20; i++) {
            if (player.taps(rectArray3.get(i))) {
                if (gMan.getChapter3().getLevelMap().get(i + 1).isUnlocked()) {
                    textureArray3.set(i, levelButtonTextures.get(i));
                }

            }
        }
    }

    public void selectLevel() {

        for (Array<Rectangle> rectArray : allRectArrays) {
            for (int i = 0; i < rectArray.size; i++) {
                if (player.taps(rectArray.get(i)) && chapter.getLevelMap().get(i + 1).isUnlocked()) {
                    if (chapter.getChapterNumber() == 1 && chapter.getLevelMap().get(i + 1).getCurrentLevel() == 1) {
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new TutorialScreen(batch, camera, player, gMan, chapter, audioHandler, assetLoader));
                    } else {
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new GameScreen(batch, camera, player, chapter, chapter.getLevelMap().get(i + 1), gMan, audioHandler, assetLoader));
                    }
                }
            }
        }
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0) {
            return false;
        }

        buttonsDown(screenX, screenY);

        if (!somethingIsScrolling) {

            if (chapter == gMan.getChapter1()) {
                if (player.taps(rightRect)) {
                    scrollingRight = true;
                }

            } else if (chapter == gMan.getChapter2()) {
                if (player.taps(leftRect)) {
                    scrollingLeft = true;
                } else if (player.taps(rightRect)) {
                    scrollingRight = true;
                }

            } else if (chapter == gMan.getChapter3()) {
                if (player.taps(leftRect)) {
                    scrollingLeft = true;
                }
            }
        }

        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0) {
            return false;
        }

        buttonsUp();
        selectLevel();

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        if (pointer > 0) {
            return false;
        }

        buttonsDown(screenX, screenY);

        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {

        buttonsUp();

        if (Math.abs(velocityX) > Math.abs(velocityY)) {

            if (velocityX > 0) {

                if (!somethingIsScrolling) {

                    if (chapter == gMan.getChapter1()) {
                        return false;

                    } else if (chapter == gMan.getChapter2()) {
                        scrollingLeft = true;

                    } else if (chapter == gMan.getChapter3()) {
                        scrollingLeft = true;
                    }
                }

            } else if (velocityX < 0) {

                if (!somethingIsScrolling) {

                    if (chapter == gMan.getChapter3()) {
                        return false;

                    } else if (chapter == gMan.getChapter1()) {
                        scrollingRight = true;

                    } else if (chapter == gMan.getChapter2()) {
                        scrollingRight = true;

                    }
                }

            }

        } else {

            selectLevel();
        }

        return true;
    }

    @Override
    public boolean keyDown(int keycode) {

        if (keycode == Input.Keys.BACK) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen(batch, camera, player, gMan, assetLoader, audioHandler));
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void show() {
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        assetLoader.getAssetManager().dispose();

    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
