package game.peanutpanda.blocktoggler.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.FitViewport;

import game.peanutpanda.blocktoggler.gamedata.AssetLoader;
import game.peanutpanda.blocktoggler.gamedata.AudioHandler;
import game.peanutpanda.blocktoggler.gamedata.GameManager;
import game.peanutpanda.blocktoggler.gamedata.ScreenSize;
import game.peanutpanda.blocktoggler.gameobjects.Player;
import game.peanutpanda.blocktoggler.levels.Chapter;

public class TutorialScreen implements Screen, InputProcessor {

    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Player player;
    private GameManager gMan;
    private Chapter chapter;
    private AudioHandler audioHandler;
    private AssetLoader assetLoader;
    private FitViewport viewport;
    private Vector3 worldCoordinates;

    private float text1DrawLength = 0.0f;
    private float text2DrawLength = 0.0f;
    private float text3DrawLength = 0.0f;

    private String text1;
    private String text2;
    private String text3;

    private BitmapFont font;

    private Sprite tutSprite;
    private float timePassed = 0;
    private float alphaTime = 0f;
    private boolean text1Done = false;

    private Sprite nextLevel;
    private Rectangle nextLevelRectangle;


    public TutorialScreen(SpriteBatch batch, OrthographicCamera camera, Player player, GameManager gMan, Chapter chapter, AudioHandler audioHandler, AssetLoader assetLoader) {

        Gdx.input.setInputProcessor(this);
        Gdx.input.setCatchBackKey(true);
        this.batch = batch;
        this.camera = camera;
        this.player = player;
        this.gMan = gMan;
        this.chapter = chapter;
        this.audioHandler = audioHandler;
        this.assetLoader = assetLoader;
        camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);

        text1 = "Tapping a block will toggle itself and\nall of its neighbouring blocks.";
        text2 = "A level is finished when there are no\nmore white blocks in play.";
        text3 = "As you advance you will come across\nspecial tiles that will help you to \ncomplete a level. \n\nTry to find out what they do!";

        font = assetLoader.getAssetManager().get("size65.ttf", BitmapFont.class);
        font.getData().setScale(0.4f);

        tutSprite = new Sprite(assetLoader.tut1());
        tutSprite.setBounds(ScreenSize.WIDTH.getSize() / 2 - 100, ScreenSize.HEIGHT.getSize() - 400, 200, 200);
        tutSprite.setAlpha(0);

        nextLevelRectangle = new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 150, 40, 60, 52);
        nextLevel = new Sprite(assetLoader.arrowRight());
        nextLevel.setBounds(nextLevelRectangle.x, nextLevelRectangle.y, nextLevelRectangle.width, nextLevelRectangle.height);

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        batch.setProjectionMatrix(camera.combined);

        float textSpeed = 0.8f;

        if (text1Done) {
            timePassed += delta;
            alphaTime += delta / 1.5f;
        }

        if (alphaTime > 0.9f) {
            alphaTime = 1;
        }

        if ((int) text1DrawLength < text1.length()) {
            text1DrawLength += textSpeed;
        }

        if ((int) text2DrawLength < text2.length() && text1DrawLength > text1.length() - 1) {
            text2DrawLength += textSpeed;
            text1Done = true;
        }

        if ((int) text3DrawLength < text3.length() && text2DrawLength > text2.length() - 1) {
            text3DrawLength += textSpeed;
        }

        batch.begin();
        batch.draw(assetLoader.background(), 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());

        font.draw(batch, "HOW TO PLAY", ScreenSize.WIDTH.getSize() / 2 - 100, ScreenSize.HEIGHT.getSize() - 30);

        font.draw(batch, text1.substring(0, (int) text1DrawLength), 20, ScreenSize.HEIGHT.getSize() - 120);

        tutSprite.setAlpha(alphaTime);
        nextLevel.draw(batch);

        if (timePassed > 0.2f && timePassed < 0.7f) {
            tutSprite.setTexture(assetLoader.tut1());

        } else if (timePassed > 0.7f && timePassed < 1.2f) {
            tutSprite.setTexture(assetLoader.tut2());

        } else if (timePassed > 1.2f && timePassed < 1.7f) {
            tutSprite.setTexture(assetLoader.tut3());

        } else if (timePassed > 1.7f && timePassed < 2.3f) {
            tutSprite.setTexture(assetLoader.tut4());

        } else if (timePassed > 2.3f) {
            timePassed = 0;
        }

        tutSprite.draw(batch);

        font.draw(batch, text2.substring(0, (int) text2DrawLength), 20, ScreenSize.HEIGHT.getSize() - 440);
        font.draw(batch, text3.substring(0, (int) text3DrawLength), 20, ScreenSize.HEIGHT.getSize() - 540);

        batch.end();

        audioHandler.resetMusic();

    }

    @Override
    public boolean keyDown(int keycode) {

        if (keycode == Input.Keys.BACK) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new LevelSelectScreen(batch, camera, player, gMan, chapter, audioHandler, assetLoader));
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0) {
            return false;
        }

        worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        player.move(worldCoordinates);

        if (player.taps(nextLevelRectangle)) {
            nextLevel.setTexture(assetLoader.arrowRightActive());
        } else {
            nextLevel.setTexture(assetLoader.arrowRight());
        }


        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0) {
            return false;
        }

        if (player.taps(nextLevelRectangle)) {
            nextLevel.setTexture(assetLoader.arrowRight());
            ((Game) Gdx.app.getApplicationListener()).setScreen(new GameScreen(batch, camera, player, chapter, chapter.getLevelMap().get(1), gMan, audioHandler, assetLoader));

        }

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        if (pointer > 0) {
            return false;
        }

        worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        player.move(worldCoordinates);

        if (player.taps(nextLevelRectangle)) {
            nextLevel.setTexture(assetLoader.arrowRightActive());
        } else {
            nextLevel.setTexture(assetLoader.arrowRight());
        }

        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        assetLoader.getAssetManager().dispose();

    }
}
