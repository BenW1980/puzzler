package game.peanutpanda.blocktoggler.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.BooleanArray;
import com.badlogic.gdx.utils.viewport.FitViewport;

import game.peanutpanda.blocktoggler.gamedata.AssetLoader;
import game.peanutpanda.blocktoggler.gamedata.AudioHandler;
import game.peanutpanda.blocktoggler.gamedata.GameManager;
import game.peanutpanda.blocktoggler.gamedata.GameState;
import game.peanutpanda.blocktoggler.gamedata.ScreenSize;
import game.peanutpanda.blocktoggler.gameobjects.ArrowShape;
import game.peanutpanda.blocktoggler.gameobjects.Deflector;
import game.peanutpanda.blocktoggler.gameobjects.Direction;
import game.peanutpanda.blocktoggler.gameobjects.Player;
import game.peanutpanda.blocktoggler.gameobjects.Pusher;
import game.peanutpanda.blocktoggler.gameobjects.Shooter;
import game.peanutpanda.blocktoggler.gameobjects.Tile;
import game.peanutpanda.blocktoggler.levels.Chapter;
import game.peanutpanda.blocktoggler.levels.Level;
import game.peanutpanda.blocktoggler.renderers.TileRenderer;

public class GameScreen implements Screen, InputProcessor {

    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Player player;

    private Array<Tile> tiles;
    private Array<ArrowShape> arrows;
    private Array<Shooter> shooters;
    private Array<Deflector> deflectors;
    private Array<Rectangle> rectangles;
    private Array<Pusher> pushers;

    private Level level;
    private TileRenderer tileRenderer;
    private GameState gameState;
    private float timePassed = 0;
    private ShapeRenderer shapeRenderer;
    private float drawStarTime;
    private boolean starsRendered = false;
    private GameManager gMan;
    private AudioHandler audioHandler;
    private Chapter chapter;
    private boolean shooterActive;
    private boolean tileMoving;
    private AssetLoader assetLoader;

    private Direction direction;

    private FitViewport viewport;

    // GAMERUNNING
    private Rectangle refreshRectangle;
    private Rectangle backToMenuRectangle;
    private Rectangle toggleVolumeRectangle;
    private Texture menuTexture;
    private Texture refreshTexture;
    private Texture volumeTexture;

    //LEVELCOMPLETE
    private Rectangle levelCompletePopup;
    private Rectangle returnToLevelRectangle;
    private Rectangle nextLevelRectangle;
    private Rectangle backToLevelScreenRectangle;
    private Sprite star1;
    private Sprite star2;
    private Sprite star3;
    private Sprite backToLevelSelect;
    private Sprite returnToLevel;
    private Sprite nextLevel;
    private float timePassed1;
    private float timePassed2;
    private float timePassed3;
    private float timePassedButtons1;
    private float timePassedButtons2;
    private float timePassedButtons3;

    private boolean touchingDown;
    private BooleanArray droppedArray;

    private BitmapFont debugFont = new BitmapFont();
    private BitmapFont font;

    private boolean tilesDropped = false;

    private Sprite grijsSprite;
    private int popupHeight = 226;

    public GameScreen(SpriteBatch batch, OrthographicCamera camera, Player player, Chapter chapter, Level level, GameManager gMan, AudioHandler audioHandler, AssetLoader assetLoader) {

        Gdx.input.setInputProcessor(this);
        Gdx.input.setCatchBackKey(true);
        this.assetLoader = assetLoader;
        this.batch = batch;
        this.camera = camera;
        gameState = GameState.PAUSED;
        camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.level = level;
        this.player = player;
        rectangles = new Array<Rectangle>();
        tileRenderer = new TileRenderer(batch, assetLoader);
        shapeRenderer = new ShapeRenderer();
        this.gMan = gMan;
        this.audioHandler = audioHandler;
        this.chapter = chapter;
        this.font = assetLoader.getAssetManager().get("size65.ttf", BitmapFont.class);
        font.getData().setScale(1f, 1f);

        //RUNNING RECTS
        backToMenuRectangle = new Rectangle(ScreenSize.WIDTH.getSize() - 160, ScreenSize.HEIGHT.getSize() - 150, 40, 35);
        toggleVolumeRectangle = new Rectangle(ScreenSize.WIDTH.getSize() - 105, ScreenSize.HEIGHT.getSize() - 150, 40, 35);
        refreshRectangle = new Rectangle(ScreenSize.WIDTH.getSize() - 50, ScreenSize.HEIGHT.getSize() - 150, 40, 35);
        menuTexture = assetLoader.menu();
        refreshTexture = assetLoader.refresh();
        volumeTexture = audioHandler.isSoundOn() ? assetLoader.volumeOn() : assetLoader.volumeOff();

        viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);

        grijsSprite = new Sprite(assetLoader.grijslaag());
        grijsSprite.setAlpha(0.7f);
        grijsSprite.setBounds(0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize() - 110);

        refreshLevel();

    }

    private void initShapeArrays() {

        tiles = chapter.getTiles();
        arrows = chapter.getArrows();
        shooters = chapter.getShooters();
        deflectors = chapter.getDeflectors();
        pushers = chapter.getPushers();
        droppedArray = new BooleanArray();

    }

    public void dropTiles() {

        for (int i = 0; i < tiles.size; i++) {
            if (tiles.get(tiles.size - 1).getRectangle().y == tiles.get(tiles.size - 1).getDropSpot()) {
                tilesDropped = true;
                gameState = GameState.RUNNING;
            }
        }

        for (int i = 0; i < droppedArray.size; i++) {
            if (droppedArray.get(i)) {
                tiles.get(i).getRectangle().y = MathUtils.lerp(tiles.get(i).getRectangle().y, tiles.get(i).getDropSpot(), 0.2f);
                if (tiles.get(i).getRectangle().y < tiles.get(i).getDropSpot() + 100) {
                    if (i < droppedArray.size - 1) {
                        droppedArray.set(i + 1, true);
                    }
                    if (tiles.get(i).getRectangle().y < tiles.get(i).getDropSpot() + 50 && tiles.get(i).getRectangle().y > tiles.get(i).getDropSpot() + 40) {
                        audioHandler.playPlopSound();
                    }
                    if (tiles.get(i).getRectangle().y < tiles.get(i).getDropSpot() + 2) {
                        tiles.get(i).getRectangle().y = tiles.get(i).getDropSpot();
                    }

                }
            }
        }
    }

    private void debugInfo() {

        debugFont.getData().setScale(1.2f, 1.2f);

        debugFont.setColor(Color.WHITE);

        debugFont.draw(batch, "backToLevelScreenSprite.x " + backToLevelSelect.getBoundingRectangle().x, 20, ScreenSize.HEIGHT.getSize() - 160);
        debugFont.draw(batch, "backToLevelScreenRectangle.x " + backToLevelScreenRectangle.x, 20, ScreenSize.HEIGHT.getSize() - 190);

    }

    private void shapeRenderer() {

        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.WHITE);

//        shapeRenderer.rect(player.getRectangle().x, player.getRectangle().y, player.getRectangle().width, player.getRectangle().height);

        shapeRenderer.end();
    }

    private void resetLevel() {

        tilesDropped = false;
        gameState = GameState.PAUSED;

        level = chapter.getLevelMap().get(level.getCurrentLevel());

        chapter.start(level);

        initShapeArrays();
        timePassed = 0;
        drawStarTime = 0;
        starsRendered = false;

        player.setMoves(0);

        for (ArrowShape arrow : arrows) {
            arrow.setPointerOneActive(false);
            arrow.setPointerOneOppositeActive(false);
        }

        for (int i = 0; i < tiles.size; i++) {
            droppedArray.add(false);
        }

        droppedArray.set(0, true);

        createLevelCompleteSprites();

    }

    private void refreshLevel() {
        level = chapter.getLevelMap().get(level.getCurrentLevel());
        resetLevel();
    }

    private void nextLevel() {

        if (level.getCurrentLevel() == chapter.getLevelMap().size()) {

            if (chapter.getChapterNumber() == 1) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new LevelSelectScreen(batch, camera, player, gMan, gMan.getChapter2(), audioHandler, assetLoader));
            } else if (chapter.getChapterNumber() == 2) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new LevelSelectScreen(batch, camera, player, gMan, gMan.getChapter3(), audioHandler, assetLoader));
            } else if (chapter.getChapterNumber() == 3) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new LevelSelectScreen(batch, camera, player, gMan, gMan.getChapter3(), audioHandler, assetLoader));
            }

        } else {
            level = chapter.getLevelMap().get(level.getCurrentLevel() + 1);
            resetLevel();
        }
    }

    @Override
    public void show() {

    }

    private Texture renderMovesUI() {

        Texture minimumMovesTexture = null;

        switch (level.getMinimumMoves()) {

            case 1:
                minimumMovesTexture = assetLoader.moves1();
                break;
            case 2:
                minimumMovesTexture = assetLoader.moves2();
                break;
            case 3:
                minimumMovesTexture = assetLoader.moves3();
                break;
            case 4:
                minimumMovesTexture = assetLoader.moves4();
                break;
            case 5:
                minimumMovesTexture = assetLoader.moves5();
                break;
            case 6:
                minimumMovesTexture = assetLoader.moves6();
                break;
            case 7:
                minimumMovesTexture = assetLoader.moves7();
                break;
            case 8:
                minimumMovesTexture = assetLoader.moves8();
                break;
            case 9:
                minimumMovesTexture = assetLoader.moves9();
                break;
            case 10:
                minimumMovesTexture = assetLoader.moves10();
                break;
            case 11:
                minimumMovesTexture = assetLoader.moves11();
                break;
            case 12:
                minimumMovesTexture = assetLoader.moves12();
                break;
            case 13:
                minimumMovesTexture = assetLoader.moves13();
                break;

        }
        return minimumMovesTexture;
    }

    private void drawUI(SpriteBatch batch) {

        batch.draw(assetLoader.UI(), 0, ScreenSize.HEIGHT.getSize() - 155, ScreenSize.WIDTH.getSize(), 160);
        batch.draw(renderMovesUI(), 295, ScreenSize.HEIGHT.getSize() - 33, 25, 23);
        batch.draw(menuTexture, ScreenSize.WIDTH.getSize() - 160, ScreenSize.HEIGHT.getSize() - 150, 40, 35);
        batch.draw(volumeTexture, ScreenSize.WIDTH.getSize() - 105, ScreenSize.HEIGHT.getSize() - 150, 40, 35);
        batch.draw(refreshTexture, ScreenSize.WIDTH.getSize() - 50, ScreenSize.HEIGHT.getSize() - 150, 40, 35);

        if (player.hasThreeStars(level)) {
            batch.draw(assetLoader.threeStarsGame(), 180, 712, 152, 45);

        } else if (player.hasTwoStars(level)) {
            batch.draw(assetLoader.twoStarsGame(), 180, 712, 152, 45);

        } else {
            batch.draw(assetLoader.oneStarGame(), 180, 712, 152, 45);
        }

        if (player.getMoves() < 100) {
            font.draw(batch, "" + player.movesAsString(), 55, 760);
        } else {
            font.draw(batch, "99+", 22, 760);
        }

        font.draw(batch, "" + level.levelAsString(), 390, 760);
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        batch.setProjectionMatrix(camera.combined);

        if (!tilesDropped) {
            dropTiles();
        }

        audioHandler.resetMusic();

        switchProjectileDirection();

        changeDeflectorTexture();

        activateShooter();

        checkShooterOverlap();

        movePusher();

        checkTilePusherCollision();

        moveTile();

        turnDeflectors();

        createArrowPointer();

        checkIfArrowOverlapsTile();

        checkIfLevelCompleted(delta);

        batch.begin();

        batch.draw(assetLoader.background(), 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());

        tileRenderer.render(tiles, arrows, shooters, deflectors, pushers);

        drawUI(batch);

        if (gameState == GameState.LEVEL_COMPLETE) {
            grijsSprite.draw(batch);
            saveProgress();
            drawLevelComplete(delta);
        }

//        debugInfo();
        batch.end();
    }

    private void createLevelCompleteSprites() {

        int starWidth = 73;
        int starHeight = 70;
        timePassed1 = 0;
        timePassed2 = 0;
        timePassed3 = 0;
        timePassedButtons1 = 0;
        timePassedButtons2 = 0;
        timePassedButtons3 = 0;

        int popupWidth = 300;
        levelCompletePopup = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - popupWidth / 2, 0, popupWidth, popupHeight);

        int buttonWidth = 52;
        int buttonHeight = 45;
        backToLevelScreenRectangle = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 35, ScreenSize.HEIGHT.getSize() / 2 - 113, buttonWidth, buttonHeight);
        returnToLevelRectangle = new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 25, ScreenSize.HEIGHT.getSize() / 2 - 113, buttonWidth, buttonHeight);
        nextLevelRectangle = new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 85, ScreenSize.HEIGHT.getSize() / 2 - 113, buttonWidth, buttonHeight);

        star1 = new Sprite(assetLoader.starGold());
        star2 = new Sprite(assetLoader.starGold());
        star3 = new Sprite(assetLoader.starGold());
        star1.setBounds(ScreenSize.WIDTH.getSize() / 2 - 118, ScreenSize.HEIGHT.getSize() / 2 - 13, starWidth, starHeight);
        star1.setAlpha(0);
        star2.setBounds(ScreenSize.WIDTH.getSize() / 2 - 36, ScreenSize.HEIGHT.getSize() / 2 - 13, starWidth, starHeight);
        star2.setAlpha(0);
        star3.setBounds(ScreenSize.WIDTH.getSize() / 2 + 45, ScreenSize.HEIGHT.getSize() / 2 - 13, starWidth, starHeight);
        star3.setAlpha(0);

        backToLevelSelect = new Sprite(assetLoader.menu());
        backToLevelSelect.setAlpha(0);
        backToLevelSelect.setBounds(backToLevelScreenRectangle.x, backToLevelScreenRectangle.y, backToLevelScreenRectangle.width, backToLevelScreenRectangle.height);

        returnToLevel = new Sprite(assetLoader.refresh());
        returnToLevel.setAlpha(0);
        returnToLevel.setBounds(returnToLevelRectangle.x, returnToLevelRectangle.y, returnToLevelRectangle.width, returnToLevelRectangle.height);

        nextLevel = new Sprite(assetLoader.arrowRight());
        nextLevel.setAlpha(0);
        nextLevel.setBounds(nextLevelRectangle.x, nextLevelRectangle.y, nextLevelRectangle.width, nextLevelRectangle.height);

    }

    private void drawLevelComplete(float delta) {

        int fadeTime = 3;
        float progress = 0.2f;
        float stopLocPopup = ScreenSize.HEIGHT.getSize() / 2 - popupHeight / 2;
        float stopLocButton = ScreenSize.HEIGHT.getSize() / 2 - 95;

        levelCompletePopup.y = MathUtils.lerp(levelCompletePopup.y, stopLocPopup, progress);

        backToLevelSelect.setY(backToLevelScreenRectangle.y);
        returnToLevel.setY(returnToLevelRectangle.y);
        nextLevel.setY(nextLevelRectangle.y);

        if (levelCompletePopup.y > stopLocPopup - 1) {
            drawStarTime += delta;
        }

        batch.draw(assetLoader.levelCompleteEmpty(), levelCompletePopup.x, levelCompletePopup.y, levelCompletePopup.width, levelCompletePopup.height);

        if (player.hasThreeStars(level)) {

            level.addStars(3);
            player.setStars(gMan.totalLevelStars());


            if (drawStarTime > 0.2f) {
                timePassed1 += delta * fadeTime;

                star1.setAlpha(timePassed1);
                if (timePassed1 > 1) {
                    star1.setAlpha(1);
                }
            }

            if (drawStarTime > 0.4f) {
                timePassed2 += delta * fadeTime;

                star2.setAlpha(timePassed2);
                if (timePassed2 > 1) {
                    star2.setAlpha(1);
                }
            }

            if (drawStarTime > 0.6f) {
                timePassed3 += delta * fadeTime;

                star3.setAlpha(timePassed3);
                if (timePassed3 > 1) {
                    star3.setAlpha(1);
                    starsRendered = true;
                }

            }

        } else if (player.hasTwoStars(level)) {

            level.addStars(2);
            player.setStars(gMan.totalLevelStars());

            if (drawStarTime > 0.2f) {
                timePassed1 += delta * fadeTime;

                star1.setAlpha(timePassed1);
                if (timePassed1 > 1) {
                    star1.setAlpha(1);
                }
            }

            if (drawStarTime > 0.4f) {
                timePassed2 += delta * fadeTime;

                star2.setAlpha(timePassed2);
                if (timePassed2 > 1) {
                    star2.setAlpha(1);
                    starsRendered = true;
                }
            }

        } else {

            level.addStars(1);
            player.setStars(gMan.totalLevelStars());

            if (drawStarTime > 0.2f) {
                timePassed1 += delta * fadeTime;

                star1.setAlpha(timePassed1);
                if (timePassed1 > 1) {
                    star1.setAlpha(1);
                    starsRendered = true;
                }
            }
        }

        star1.draw(batch);
        star2.draw(batch);
        star3.draw(batch);

        if (starsRendered) {

            timePassedButtons1 += delta * fadeTime;

            if (timePassedButtons1 > 1) {
                timePassedButtons1 = 1;
            }
            if (timePassedButtons2 > 1) {
                timePassedButtons2 = 1;
            }
            if (timePassedButtons3 > 1) {
                timePassedButtons3 = 1;
            }

            backToLevelSelect.draw(batch);
            returnToLevel.draw(batch);
            nextLevel.draw(batch);

            backToLevelScreenRectangle.y = MathUtils.lerp(backToLevelScreenRectangle.y, stopLocButton, progress);

            if (backToLevelScreenRectangle.y > stopLocButton - 10) {
                returnToLevelRectangle.y = MathUtils.lerp(returnToLevelRectangle.y, stopLocButton, progress);
            }

            if (returnToLevelRectangle.y > stopLocButton - 10) {
                nextLevelRectangle.y = MathUtils.lerp(nextLevelRectangle.y, stopLocButton, progress);
            }


            backToLevelSelect.setAlpha(timePassedButtons1);
            returnToLevel.setAlpha(timePassedButtons1);
            nextLevel.setAlpha(timePassedButtons1);

        }
    }

    private void moveTile() {

        for (Tile tile : tiles) {
            if (tile.isMoving()) {
                tileMoving = true;
                tile.move(direction);
                break;
            } else {
                tileMoving = false;
            }
        }
    }

    private void checkTilePusherCollision() {

        for (Tile tile : tiles) {
            for (Pusher pusher : pushers) {
                if (pusher.getPushRect().overlaps(tile.getRectangle())) {
                    tile.setMoving(true);
                }
            }
        }
    }

    private void movePusher() {
        for (Pusher pusher : pushers) {
            if (pusher.isPushing()) {
                pusher.move(pusher.getDirection());
            }
        }
    }

    private void activateShooter() {

        for (Shooter shooter : shooters) {
            if (shooter.isShooting()) {
                shooterActive = true;
                shooter.shoot(player, tiles, deflectors, gameState);
                break;
            } else {
                shooterActive = false;
            }
        }
    }

    private void switchProjectileDirection() {

        for (Shooter shooter : shooters) {
            for (Deflector deflector : deflectors) {
                if (shooter.getProjectile().getCenterRect().overlaps(deflector.getCenterRect())) {
                    shooter.getProjectile().switchDirection(deflector);
                }
            }
        }
    }

    private void changeDeflectorTexture() {

        for (Shooter shooter : shooters) {
            for (Deflector deflector : deflectors) {

                if (shooter.getProjectile().getCenterRect().overlaps(deflector.getRectangle())) {
                    if (!deflector.isHitByProjectile()) {
                        deflector.setHitByProjectile(true);
                        audioHandler.playPingSound();
                    }
                }
            }
        }
    }

    private void checkShooterOverlap() {

        if (gameState == GameState.RUNNING) {

            for (Tile tile : tiles) {
                for (Shooter shooter : shooters) {

                    if (shooter.getProjectile().getCenterRect().overlaps(tile.getRectangle())) {

                        if (!tile.isTouched() && !tile.isHit()) {
                            tile.setHit(true);
                            tile.setTouched(true);
                            level.changeActiveTiles(+1);
                            audioHandler.playTickSound();
                        }

                        if (tile.isTouched() && !tile.isHit()) {
                            tile.setHit(true);
                            tile.setTouched(false);
                            level.changeActiveTiles(-1);
                            audioHandler.playTickSound();

                        }
                    }
                }
            }
        }
    }

    private void turnDeflectors() {
        for (Deflector deflector : deflectors) {
            if (tiles.size != level.getActiveCount()) {

                if (deflector.isTouched()) {
                    gameState = GameState.PAUSED;
                    deflector.setTappedByPlayer(true);// changes colour and size while turning
                    deflector.turn();
                    if (deflector.getAngle() == 45 || deflector.getAngle() == 135 || deflector.getAngle() == 225 || deflector.getAngle() == 315) {
                        deflector.setTappedByPlayer(false);
                        gameState = GameState.RUNNING;
                    }
                }
            }

            if (deflector.getAngle() == 360) {
                deflector.setAngle(0);
            }
        }
    }

    private void createArrowPointer() {

        for (ArrowShape arrow : arrows) {
            if (tiles.size != level.getActiveCount()) {

                if (arrow.isTouched()) {
                    gameState = GameState.PAUSED;
                    arrow.setTappedByPlayer(true); // changes colour and size while turning
                    arrow.turn();
                    if (arrow.getAngle() == 0 || arrow.getAngle() % 90 == 0) {
                        arrow.createPointers();
                        arrow.setTappedByPlayer(false);
                        gameState = GameState.RUNNING;
                    }
                }
            }

            if (arrow.getAngle() == 360) {
                arrow.setAngle(0);
            }

//            arrow.createPointers();

        }
    }

    private void checkIfLevelCompleted(float delta) {

        if (tiles.size == level.getActiveCount() && !touchingDown && !eventActive()) {

            gameState = GameState.PAUSED;
            timePassed += delta;

            for (Tile tile : tiles) {

                if (!tile.isDone()) {
                    if (timePassed > 0.1f / 2) {
                        audioHandler.playPlopSound();
                        tile.getRectangle().setWidth(tile.getRectangle().getWidth() + 2);
                        tile.getRectangle().setHeight(tile.getRectangle().getHeight() + 2);
                        tile.setDone(true);
                        timePassed = 0;
                    }
                }

                if (timePassed > 0.2f) {
                    gameState = GameState.LEVEL_COMPLETE;
                }
            }
        }
    }

    private void checkIfArrowOverlapsTile() {

        for (Tile tile : tiles) {
            for (ArrowShape arrow : arrows) {

                if (gameState == GameState.RUNNING) {

                    if (arrow.getPointerOne().overlaps(tile.getRectangle()) && !arrow.isPointerOneActive()) {

                        if (!tile.isTouched()) {
                            tile.setTouched(true);
                            level.changeActiveTiles(+1);
                            arrow.setPointerOneActive(true);

                        } else {
                            tile.setTouched(false);
                            level.changeActiveTiles(-1);
                            arrow.setPointerOneActive(true);
                        }
                    }

                    if (arrow.getPointerOneOpposite().overlaps(tile.getRectangle()) && !arrow.isPointerOneOppositeActive()) {

                        if (!tile.isTouched()) {
                            tile.setTouched(true);
                            level.changeActiveTiles(+1);
                            arrow.setPointerOneOppositeActive(true);

                        } else {
                            tile.setTouched(false);
                            level.changeActiveTiles(-1);
                            arrow.setPointerOneOppositeActive(true);
                        }
                    }

                    if (arrow.getPointerTwo().overlaps(tile.getRectangle()) && !arrow.isPointerTwoActive()) {

                        if (!tile.isTouched()) {
                            tile.setTouched(true);
                            level.changeActiveTiles(+1);
                            arrow.setPointerTwoActive(true);

                        } else {
                            tile.setTouched(false);
                            level.changeActiveTiles(-1);
                            arrow.setPointerTwoActive(true);
                        }
                    }

                    if (arrow.getPointerTwoOpposite().overlaps(tile.getRectangle()) && !arrow.isPointerTwoOppositeActive()) {

                        if (!tile.isTouched()) {
                            tile.setTouched(true);
                            level.changeActiveTiles(+1);
                            arrow.setPointerTwoOppositeActive(true);

                        } else {
                            tile.setTouched(false);
                            level.changeActiveTiles(-1);
                            arrow.setPointerTwoOppositeActive(true);
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0 || gameState == GameState.PAUSED || eventActive()) {
            return false;
        }

        Vector3 worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        touchingDown = true;

        if (gameState != GameState.PAUSED && !eventActive()) {
            player.move(worldCoordinates);
        }

        for (Pusher pusher : pushers) {
            pusher.setPushRect(new Rectangle());
        }


        switch (gameState) {

            case RUNNING:

                if (!eventActive()) {

                    for (Pusher pusher : pushers) {
                        if (player.taps(pusher)) {
                            pusher.setPushing(true);
                            player.addMove();
                            pusher.createRectangle();
                            direction = pusher.getDirection();
                        }
                    }

                    for (Shooter shooter : shooters) {
                        if (player.taps(shooter)) {
                            shooter.setTappedByPlayer(true);
                        }
                    }


                    for (Deflector deflector : deflectors) {
                        if (player.taps(deflector)) {
                            deflector.setTappedByPlayer(true);
                        }
                    }

                    for (ArrowShape arrow : arrows) {
                        if (player.taps(arrow)) {
                            arrow.setTappedByPlayer(true);
                        }
                    }

                    for (Tile tile : tiles) {
                        if (player.taps(tile)) {
                            audioHandler.playTickSound();
                            player.createRectangles(rectangles, tile);
                            player.addMove();
                        }
                    }

                    for (Tile tile : tiles) {
                        if (rectangles.size != 0 && rectangleOverlapsTile(tile)) {

                            tile.setPressedDown(true);

                            if (!tile.isTouched()) {
                                tile.setTouched(true);
                                level.changeActiveTiles(+1);

                            } else {
                                tile.setTouched(false);
                                level.changeActiveTiles(-1);
                            }
                        }
                    }
                }

                if (player.taps(toggleVolumeRectangle)) {

                    if (audioHandler.isSoundOn()) {
                        volumeTexture = assetLoader.volumeOff();
                        gMan.saveAudioSettings(false);
                    } else {
                        volumeTexture = assetLoader.volumeOn();
                        gMan.saveAudioSettings(true);
                    }
                    gMan.getPrefs().flush();
                }

                if (player.taps(refreshRectangle)) {
                    refreshTexture = assetLoader.refreshActive();

                }

                if (player.taps(backToMenuRectangle)) {
                    menuTexture = assetLoader.menuActive();
                }

                break;


            case LEVEL_COMPLETE:

                if (player.taps(backToLevelScreenRectangle)) {
                    backToLevelSelect.setTexture(assetLoader.menuActiveLevelComplete());
                }

                if (player.taps(returnToLevelRectangle)) {
                    returnToLevel.setTexture(assetLoader.refreshActiveLevelComplete());
                }

                if (player.taps(nextLevelRectangle)) {
                    nextLevel.setTexture(assetLoader.arrowRightActive());
                }
                break;
        }

        return true;
    }


    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0) {
            return false;
        }

        touchingDown = false;

        for (Tile tile : tiles) {
            tile.setPressedDown(false);
        }

        player.removeRectangles(rectangles);

        if (player.taps(toggleVolumeRectangle)) {
            audioHandler.toggleSound();
        }

        switch (gameState) {

            case RUNNING:

                if (!eventActive()) {

                    for (Deflector deflector : deflectors) {
                        if (player.taps(deflector)) {
                            player.addMove();

                            if (deflector.getAngle() == 45 || deflector.getAngle() == 135 || deflector.getAngle() == 225 || deflector.getAngle() == 315) {
                                deflector.setTouched(true);
                            }
                        }
                    }

                    for (ArrowShape arrow : arrows) {
                        if (player.taps(arrow)) {
                            player.addMove();
                            arrow.setTappedByPlayer(false);

                            if (arrow.getAngle() % 90 == 0) {
                                arrow.setTouched(true);
                            }
                        }
                    }

                    for (Shooter shooter : shooters) {
                        if (player.taps(shooter)) {
                            shooter.setShooting(true);
                            shooter.setTappedByPlayer(false);
                            player.addMove();
                        }
                    }
                }

                if (player.taps(refreshRectangle)) {
                    refreshTexture = assetLoader.refresh();
                    refreshLevel();

                } else if (player.taps(backToMenuRectangle)) {
                    menuTexture = assetLoader.menu();
                    ((Game) Gdx.app.getApplicationListener()).setScreen(new LevelSelectScreen(batch, camera, player, gMan, chapter, audioHandler, assetLoader));

                }
                break;

            case LEVEL_COMPLETE:

                if (player.taps(backToLevelScreenRectangle)) {
                    backToLevelSelect.setTexture(assetLoader.menuActive());
                    ((Game) Gdx.app.getApplicationListener()).setScreen(new LevelSelectScreen(batch, camera, player, gMan, chapter, audioHandler, assetLoader));

                } else if (player.taps(returnToLevelRectangle)) {
                    returnToLevel.setTexture(assetLoader.refresh());
                    refreshLevel();

                } else if (player.taps(nextLevelRectangle) && starsRendered) {
                    nextLevel.setTexture(assetLoader.arrowRight());
                    nextLevel();

                }

                break;
        }

        return true;
    }

    private boolean rectangleOverlapsTile(Tile shape) {

        for (Rectangle rect : rectangles) {
            if (rect.overlaps(shape.getRectangle())) {
                return true;
            }
        }
        return false;
    }

    private void saveProgress() {

        chapter.getLevelMap().get(level.getCurrentLevel()).setCompleted(true);

        if (level.getCurrentLevel() < chapter.getLevelMap().size()) {
            chapter.getLevelMap().get(level.getCurrentLevel() + 1).setUnlocked(true);
            gMan.saveLevelUnlocked(chapter.getLevelMap().get(level.getCurrentLevel() + 1).getLevelName());
        }

        // unlock first level of chapter when last level of previous chapter is completed

        if (gMan.getChapter1().getLevelMap().get(20).isCompleted()) {
            gMan.getChapter2().getLevelMap().get(1).setUnlocked(true);
            gMan.saveLevelUnlocked(gMan.getChapter2().getLevelMap().get(1).getLevelName());

        }

        if (gMan.getChapter2().getLevelMap().get(20).isCompleted()) {
            gMan.getChapter3().getLevelMap().get(1).setUnlocked(true);
            gMan.saveLevelUnlocked(gMan.getChapter3().getLevelMap().get(1).getLevelName());
        }

        gMan.saveLevelStars(chapter.getLevelMap().get(level.getCurrentLevel()).getLevelName(), level.getStars());
        gMan.getPrefs().flush();

    }

    @Override
    public boolean keyDown(int keycode) {

        if (keycode == Input.Keys.BACK) {
            if (gameState == GameState.RUNNING && !eventActive()) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new LevelSelectScreen(batch, camera, player, gMan, chapter, audioHandler, assetLoader));
            }

            if (gameState == GameState.LEVEL_COMPLETE) {
                refreshLevel();
            }
        }
        return false;
    }

    public boolean eventActive() {
        return shooterActive || tileMoving;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        batch.dispose();
        assetLoader.getAssetManager().dispose();
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}