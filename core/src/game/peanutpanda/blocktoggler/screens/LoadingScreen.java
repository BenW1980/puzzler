package game.peanutpanda.blocktoggler.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;

import game.peanutpanda.blocktoggler.gamedata.AssetLoader;
import game.peanutpanda.blocktoggler.gamedata.AudioHandler;
import game.peanutpanda.blocktoggler.gamedata.GameManager;
import game.peanutpanda.blocktoggler.gamedata.ScreenSize;
import game.peanutpanda.blocktoggler.gameobjects.Player;

public class LoadingScreen implements Screen {

    private AssetLoader assetLoader;
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private GameManager gMan;
    private Player player;
    private FitViewport viewport;
    private Sprite pandaSprite;
    private Sprite pandaTxtSprite;
    private Texture pandaTexture;
    private Texture pandaTxtTexture;
    private Texture backGroundWhite;

    private float pandaAlpha = 0;
    private float pandaTxtAlpha = 0;

    private boolean loaded = false;
    private AudioHandler audioHandler;



    public LoadingScreen() {

        assetLoader = new AssetLoader();
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        gMan = new GameManager();
        camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        player = gMan.getPlayer();
        viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);
        pandaTexture = new Texture(Gdx.files.internal("images/pepaWhite.png"));
        pandaTxtTexture = new Texture(Gdx.files.internal("images/pepatxt.png"));
        backGroundWhite = new Texture(Gdx.files.internal("images/backgroundWhite.png"));
        pandaSprite = new Sprite(pandaTexture);
        pandaTxtSprite = new Sprite(pandaTxtTexture);
        viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);
    }

    @Override
    public void show() {
        assetLoader.startLoading();
    }

    @Override
    public void render(float delta) {

        float alphaTime = 0.02f;

        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        batch.draw(backGroundWhite, 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());

        if (!loaded) {
            pandaTxtAlpha += alphaTime;
            if (pandaTxtAlpha > 1) {
                pandaTxtAlpha = 1;
                pandaAlpha += alphaTime;

            }
            if (pandaAlpha > 1) {
                pandaAlpha = 1;
            }

        } else {
            pandaTxtAlpha -= alphaTime;
            pandaAlpha -= alphaTime;
        }

        if (pandaAlpha < 0) {
            pandaAlpha = 0;
        }

        if (pandaTxtAlpha < 0) {
            pandaTxtAlpha = 0;
        }

        pandaTxtSprite.setAlpha(pandaTxtAlpha);
        pandaTxtSprite.setBounds(100, 365, 200, 113);
        pandaTxtSprite.draw(batch);

        pandaSprite.setAlpha(pandaAlpha);
        pandaSprite.setBounds(330, 365, 75, 113);
        pandaSprite.draw(batch);

        batch.end();

        if (assetLoader.getAssetManager().update()) {

            loaded = true;
            audioHandler = new AudioHandler(assetLoader);
            gMan.loadProgress();

            // for debugging
//            gMan.unlockGame();
//            gMan.unlockFirst19Levels();

            if (pandaAlpha == 0 && pandaTxtAlpha == 0) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen(batch, camera, player, gMan, assetLoader, audioHandler));
            }
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        batch.dispose();
        pandaTexture.dispose();
        pandaTxtTexture.dispose();
        backGroundWhite.dispose();
        assetLoader.getAssetManager().dispose();
    }
}
