package game.peanutpanda.blocktoggler.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.FitViewport;

import game.peanutpanda.blocktoggler.gamedata.AssetLoader;
import game.peanutpanda.blocktoggler.gamedata.AudioHandler;
import game.peanutpanda.blocktoggler.gamedata.GameManager;
import game.peanutpanda.blocktoggler.gamedata.ScreenSize;
import game.peanutpanda.blocktoggler.gameobjects.Player;
import game.peanutpanda.blocktoggler.renderers.MainScreenRenderer;

public class MainMenuScreen implements Screen, InputProcessor {

    private int buttonsWidth = 50;
    private int buttonsHeight = 43;
    private int buttonsDown = 40;
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private Player player;
    private Rectangle startRectangle;
    private Texture playUntapped;
    private GameManager gMan;
    private Vector3 worldCoordinates;
    private AudioHandler audioHandler;
    private AssetLoader assetLoader;
    private FitViewport viewport;
    private Rectangle twRect;
    private Rectangle creditsRect;
    private Rectangle volumeRect;
    private Texture twButtonTexture;
    private Texture creditsTexture;
    private Texture volumeTexture;
    private Sprite backgroundSprite;
    private float backgroundAlpha = 0;
    private boolean screenIsReady = false;
    private boolean backgroundReady = false;
    private boolean musicStarted = false;
    private MainScreenRenderer mainScreenRenderer;
    private boolean buttonsReady = false;
    private Sprite playSprite;

//    private BitmapFont font;

    private int timesPlayed;

    public MainMenuScreen(SpriteBatch batch, OrthographicCamera camera, Player player, GameManager gMan, AssetLoader assetLoader, AudioHandler audioHandler) {
        Gdx.input.setInputProcessor(this);
        Gdx.input.setCatchBackKey(false);
        this.batch = batch;
        this.camera = camera;
        camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.player = player;
        this.gMan = gMan;
        this.assetLoader = assetLoader;
        this.audioHandler = audioHandler;

        viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);

        startRectangle = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 75, ScreenSize.HEIGHT.getSize() / 2 - 75, 150, 150);

        twRect = new Rectangle(-50, buttonsDown, buttonsWidth, buttonsHeight);
        creditsRect = new Rectangle(-50, buttonsDown, buttonsWidth, buttonsHeight);
        volumeRect = new Rectangle(-50, buttonsDown, buttonsWidth, buttonsHeight);

        playUntapped = assetLoader.playUntapped();
        twButtonTexture = assetLoader.twButton();
        creditsTexture = assetLoader.credits();

        timesPlayed = gMan.getTimesPlayed();
        timesPlayed++;
        gMan.saveTimesPlayed(timesPlayed);
        gMan.getPrefs().flush();
        if (timesPlayed > 1) {
            audioHandler.setSoundOn(gMan.loadAudioSettings());
        } else {
            audioHandler.setSoundOn(true);
        }

        volumeTexture = audioHandler.isSoundOn() ? assetLoader.volumeMain() : assetLoader.volumeMainOnClick();

        playSprite = new Sprite(playUntapped);
        playSprite.setAlpha(0);
        playSprite.setBounds(ScreenSize.WIDTH.getSize() / 2 - 75, ScreenSize.HEIGHT.getSize() / 2 - 75, 150, 150);

        backgroundSprite = new Sprite(assetLoader.background());
        mainScreenRenderer = new MainScreenRenderer(audioHandler);
//        font = new BitmapFont();
    }

    @Override
    public void show() {

    }

    public void slideButtons() {

        float progress = 0.2f;
        boolean twitterDone = false;
        boolean creditsDone = false;

        twRect.x = MathUtils.lerp(twRect.x, ScreenSize.WIDTH.getSize() - buttonsWidth - 20, progress);
        if (twRect.x > ScreenSize.WIDTH.getSize() / 1.5f) {
            twitterDone = true;
            buttonsReady = true;
        }
        if (twRect.x > ScreenSize.WIDTH.getSize() - buttonsWidth - 20 - 2) {
            twRect.x = ScreenSize.WIDTH.getSize() - buttonsWidth - 20;
        }

        if (twitterDone) {
            creditsRect.x = MathUtils.lerp(creditsRect.x, ScreenSize.WIDTH.getSize() - buttonsWidth * 2 - 30, progress);
            if (creditsRect.x > ScreenSize.WIDTH.getSize() / 1.5f) {
                creditsDone = true;
            }
            if (creditsRect.x > ScreenSize.WIDTH.getSize() - buttonsWidth * 2 - 30 - 2) {
                creditsRect.x = ScreenSize.WIDTH.getSize() - buttonsWidth * 2 - 30;
            }
        }
        if (creditsDone) {
            volumeRect.x = MathUtils.lerp(volumeRect.x, ScreenSize.WIDTH.getSize() - buttonsWidth * 3 - 40, progress);
            if (volumeRect.x > ScreenSize.WIDTH.getSize() - buttonsWidth * 3 - 40 - 2) {
                volumeRect.x = ScreenSize.WIDTH.getSize() - buttonsWidth * 3 - 40;

            }
        }
    }

    public void fadeInPlay() {

        boolean done = false;


        if (playSprite.getColor().a > 0.9f) {
            playSprite.setAlpha(1);
            done = true;
        }

        if (!done) {
            playSprite.setAlpha(playSprite.getColor().a += 0.1f);
        }


    }


    @Override
    public void render(float delta) {

        float alphaTime = 0.05f;

        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();

        batch.draw(assetLoader.backgroundWhite(), 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());

        if (backgroundAlpha > 1) {
            backgroundAlpha = 1;
            backgroundReady = true;
        }

        if (!backgroundReady) {
            backgroundAlpha += alphaTime;
            backgroundSprite.setAlpha(backgroundAlpha);
            backgroundSprite.setBounds(0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
            backgroundSprite.draw(batch);

        } else {

            batch.draw(assetLoader.background(), 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
            screenIsReady = true;
            mainScreenRenderer.dropTitle();
            mainScreenRenderer.drawTitle(assetLoader, batch);

        }

        if (mainScreenRenderer.isTitleReady()) {
            batch.draw(twButtonTexture, twRect.x, twRect.y, twRect.width, twRect.height);
            batch.draw(creditsTexture, creditsRect.x, creditsRect.y, creditsRect.width, creditsRect.height);
            batch.draw(volumeTexture, volumeRect.x, volumeRect.y, volumeRect.width, volumeRect.height);
            slideButtons();
            audioHandler.playMusic();
        }

        if (buttonsReady) {
            fadeInPlay();
            playSprite.draw(batch);
            if (!musicStarted) {
                audioHandler.playMusic();
                musicStarted = true;
            }

            audioHandler.resetMusic();
        }

//        shapeRenderer();

//        font.getData().setScale(1.5f, 1.5f);
//        font.setColor(Color.WHITE);
//
//        font.draw(batch, "timesPlayed: " + timesPlayed, 20, ScreenSize.HEIGHT.getSize() - 230);

        batch.end();
    }


    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0 || !screenIsReady) {
            return false;
        }

        worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        player.move(worldCoordinates);

        if (player.taps(twRect)) {
            twButtonTexture = assetLoader.twButtonUP();
        }

        if (player.taps(startRectangle)) {
            playSprite.setTexture(assetLoader.playTapped());
        }

        if (player.taps(creditsRect)) {
            creditsTexture = assetLoader.creditsOnClick();
        }

        if (player.taps(volumeRect)) {
            audioHandler.toggleSound();
            if (volumeTexture == assetLoader.volumeMain()) {
                volumeTexture = assetLoader.volumeMainOnClick();
                gMan.saveAudioSettings(false);
            } else {
                volumeTexture = assetLoader.volumeMain();
                gMan.saveAudioSettings(true);
            }
            gMan.getPrefs().flush();
        }

        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0 || !screenIsReady) {
            return false;
        }

        if (player.taps(twRect)) {
            twButtonTexture = assetLoader.twButton();
            if (!Gdx.net.openURI("twitter://user?user_id=741271262006251520")) { // open app
                Gdx.net.openURI("https://twitter.com/PeanutPandaApps"); // open site
            }
        }

        if (player.taps(creditsRect)) {
            creditsTexture = assetLoader.credits();
            ((Game) Gdx.app.getApplicationListener()).setScreen(new CreditsScreen(camera, batch, player, gMan, audioHandler, assetLoader));
        }

        if (player.taps(startRectangle)) {
            playSprite.setTexture(assetLoader.playUntapped());
            ((Game) Gdx.app.getApplicationListener()).setScreen(new LevelSelectScreen(batch, camera, player, gMan, gMan.getChapter1(), audioHandler, assetLoader));
        }

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        if (pointer > 0 || !screenIsReady) {
            return false;
        }

        worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        player.move(worldCoordinates);

        if (player.taps(startRectangle)) {
            playSprite.setTexture(assetLoader.playTapped());
        } else {
            playSprite.setTexture(assetLoader.playUntapped());
        }

        if (player.taps(twRect)) {
            twButtonTexture = assetLoader.twButtonUP();
        } else {
            twButtonTexture = assetLoader.twButton();
        }

        if (player.taps(creditsRect)) {
            creditsTexture = assetLoader.creditsOnClick();
        } else {
            creditsTexture = assetLoader.credits();
        }

        return true;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }


    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }


    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        assetLoader.getAssetManager().dispose();
    }
}
